# React Router DOM :
**[React-Router](https://reacttraining.com/react-router/web/api/BrowserRouter)** est un package qui Peut etre utiliser pour du natif, du Web(DOM) et Autre.<br>
Pour l'installer :
`yarn add react-router-dom@4.2.2`
## Importer **BrowserRouter** et **Route** :
```javascript
import { BrowserRouter, Route } from 'react-router-dom'
```
## Utiliser **Route** et **BrowserRouter**:
Pour set-up de multiple **Route** BrowserRouter demande à ce qu'ils soient encapsuler dans une `<div>` :
```javascript
const routes = (
    <BrowserRouter>
        <div>
            <Route path='/' component={ExpenseDashboardPage} />
            <Route path='/create' component={AddExpensePage} />
        </div>
    </BrowserRouter>
)
```
En revanche il y a un probleme, si on set-up notre **`<BrowserRouter>`** comme cela on va avoir un problème
car pour les Route cela revient a dire affiche moi `ExpenseDashboardPage` si le path de l'url commence par `/`,
pareil pour `/create`. Cela peut etre utile mais peut causer des problème également. Pour éviter cela on ajoute
une propriété dans le `<Route>` :
* `<Route path='/' component={ExpenseDashboardPage} exact={true}/>`
## Utiliser `<Switch>` :
`<Switch>` est utiliser pour dire a `<BrowserRouter>` que dès qu'il trouve un match, il s'arrettent d'introduire de nouveau component, comme un switch normal :
```javascript
const routes = (
    <BrowserRouter>
        <switch>
            <Route path='/' component={ExpenseDashboardPage} exact={true} />
            <Route path='/create' component={AddExpensePage}  />
            <Route path='/edit' component={EditExpensePage}  />
            <Route path='/help' component={HelpPage}  />
            <Route component={NotFound}  />
        </switch>
        </BrowserRouter>
)
```
Il faut l'importer depuis react-router avant .
## Utiliser `<Link></Link>` :

Si on ajoute un simple lien sur nos page 404 par exemple, on effectuera une operation cotés server au lieu de le faire cotés client et la page se rafraichira a chaque fois que l'on clique, pour on veux le faire cotés client et donc ne pas recharger notre page on utilise `<Link href="">` :
```javascript

const NotFound = () => (
    <div>
        404 ! -- <Link to="/">Go Home</Link>
    </div>
)
```
## Utiliser `<NavLink></NavLink>` :
<NavLink>` Fonctionne comme <Link> mais possède des propriétés supplémentaire, par exemple il permet d'assigner une class CSS au lien actif :
