# **Astuces pour React**

## Aller chercher les éléments dans un `<form>`

```jsx
<form onSubmit={onFormSubmit}>
      <input type="text" name="option"/>
      <button >Add Option</button>
</form>

const onFormSubmit = (event) => {
  event.preventDefault()
  const option = event.target.elements.option.value
}
```

## Moyen de dynamiquement `disabled` un `button`

```jsx
<button disabled= {app.options.length == 0}> Choisis un random Option</button>
```

## Utiliser un array dans JSX

```jsx
const numbers = ['un', 'deux', 'trois']
{
 numbers.map((word, index) => {
          return <p key={index}>  {word}</p>
        })
}
```

## Utiliser un Array

```javascript
const options  = ['haha', 'hoho', 'huhu']

 class MedicalApp extends React.Component {
render() {
  return (
      <div>
        <Options options = {options}/>
      </div>
    );
  );
}


class Options extends React.Component {
  render() {
    return (
      <div>
        <p>Ici sont vos options</p>
        { this.props.options.map((option) => <Option key= {option} option= {option} />) }
      </div>
    )
  }
}

 class Option  extends React.Component {
  render() {
    return (
      <div>
        <p>{this.props.option}</p>
      </div>
    )
  }
}
 ```

## Render un élément en fonction d'une condition

Il existe un moyen simple avec peu de code pour générer un `<p>`par exemple, pour un
message d'erreur.

```javascript
{this.state.error && <p>this.handleError</p>}```
**Ainsi ce paragraphe ne sera présent que si l'erreur vaut true. **

---
## Desactiver et activer un boutton a l'aide d'une classe 
```javascript
 <button
          className={"button  button--warning button--big " + (!props.hasOptions ? 'button--disabled' : 'false')}
```
et 
```css
.button--disabled {
  cursor: default !important;
  background-color: rgb(39, 39, 39) !important;
  border: none;
  opacity: 0.2;
}
```