# __Documentation React__
---
## __Commandes Babel__ :
`babel src/app.js --out-file=public/scripts/app.js --presets=env,react --watch` : Choisir le fichier ou s'effectuera la transpilation.<br>
[Babel](https://babeljs.io/) est un compilateur qui permet de pouvoir créer du code ES6 et + malgrès que certains navigateurs ne le supportent pas, il en ai de même pour nodeJS par exemple, qui ne supporte pas les modules ES6, il est également utilisé pour d'autre languages.<br>
`yarn add babel-preset-react babel-preset-env`
---

## __JSX__ :
__JSX__ est propre a __React__, c'est la maniere qu'il utilise pour afficher des expressions sur une page HTML. __JSX__ rend le code __très lisible__ comparé a d'autres framework, on écrit du code __vanilla JS__, en revanche __il n'accepte pas les conditions__ comme le `if`, seulement les expressions, comme le ternaire par exemple. Il suffit de les déporter dans une fonction, comme on le fait toujours pour une meilleur lecture du code.
C'est pour cela qu'il est apprécié, pour ceux qui connaissent déjà __javascript__ il est aisé de s'y retrouver et de créer des applications basiques.
Il y a quelques règles à respecter cependant, comme par exemple que tous nos code `JSX` doivent etre encapsuler dans un élément parent, le plus souvent une `div`.

---
## __Variable Globales__ :
__[React](https://reactjs.org/docs/forms.html "Documentation API")__ nous donne accès à deux variables globales : __React__ et __ReactDOM__, chacune ayant leur spécificitées, par exemple la variable __React__ nous donne accès a la classe parente `React.Component`, __ReactDOM__ nous donne accès à la methode `.render()` qui permet de créer notre DOM.
---
## __Components__ :
Les components sont des éléments dans React qui permettent de __structurer notre code__.
En effet les components sont des éléments comme des classes qui permettent de rééutiliser des parties de notre code
qui se répètent, comme un header, une liste d'options etc ...Cela a plusieurs avantages :
* __Une meilleure lisibilité__.
* __Un gain de performance__, __React__ est __one-way-binding-data__ cela peut etre un désavantage comparé a __Angular__ et cela découle du fait que __React__ est dans sa forme basique seulement une __librairie pour l'affichage__, mais grace a cela on peut choisir quels éléments on veux raffraichir, c'est pour cela que __React__ est l'un des framework les plus rapide, si ce n'est __le__ plus rapide. en plus de cela __React__ possède un algorythme très puissant qui permet de ne raffraichir __que les parties qui on étaient modifiées__.
* __Une réutilisation__ poussée de certaines parties de notre code qui se répètent.

### Création d'une classe Component __React__ :
* Un component est en réalité une Classe qui hérite de la classe parente créer par le framework __React__ (`React.Component`), une des difference est que lors de la création de la classe component, elle ne peut pas être vide comme dans les classes ES6, il faut __obligatoirement__ crée la méthode `render()` :
```javascript
class Header extends React.Component {
    render() {
        return <h1> this is from header </h1>
    }
}
```
Ainsi on ne créer plus nos pages html avec toutes nos balises à l'intérieur mais on utilise nos components pour structurer notre code :

```javascript
class Header extends React.Component {
    render() {
        return <h1> this is from header </h1>
    }
}

let jsx = (
    <div>
        <h1>title</h1>
        //notre classe Header
        <Header />
    </div>
)

ReactDOM.render(jsx, document.getElementById("app"))
```
Il faut noter que tout cela est créer par la librairie __React__, donc les règles sont celle de __React__ et non plus celle d'ES6, par exemple les classes doivent __obligatoirement__ avoir une __lettre majuscule sur la premiere lettre__ du nom de la classe, car cela permet a React de savoir si on veux creer le html depuis un component ou si l'on veux creer un HTML element, comme avec `<header>`.<br>
Voici un exemple d'utilisation de plusieurs Components pour créer une page web :
```javascript
class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>Indecision</h1>
                <h2> Put Your Life In the Hand Of A Computer </h2>
            </div>
        )
    }
}

class Action extends React.Component {
    render() {
        return  (
            <div>
                <button> What Should I Do Computer ? </button>
            </div>
        )
    }
}

class Options extends React.Component {
    render() {
        return (
            <div>
                    <h1>Options Component Here !</h1>
            </div>
        )
    }
}


class AddOptions extends React.Component {
    render() {
        return (
            <div>
                <h1> Add Options Component Here !</h1>
            </div>
        )
    }
}

const jsx = (
    <div>
        <Header />
        <Action />
        <Options />
        <AddOptions />
    </div>
)

ReactDOM.render(jsx, document.getElementById("app"))
```
Comme déjà dit plus haut les components sont très utiles pour réutiliser du code répétitif, et pour  __structurer__ notre code, le rendant __plus lisible__, __React__ permet de nested nos components :

```javascript
class IndecisionApp extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <Action />
                <Options />
                <AddOptions />
            </div>
        )
    }
}

ReactDOM.render(<IndecisionApp />, document.getElementById("app"))

```


*Tip* : Il n'est pas obligatoire de créer un tag comme : `<ClassName />`, on peut créer un tag qui aura exactement le même comportement : <ClassName> </ClassName>, en revanche il est préférable d'utiliser la __première méthode__ qui fonctionnera dans toutes les situations et rend le code __beaucoup plus lisible__.

### Component Props :
Les props permettent de créer des rendu Dynamique à partir des Components, cela se créer de façon assez similaire avec la création d'attributs HTML et la création de propriétés dans une classe ES6 :

```javascript
class IndecisionApp extends React.Component {
    render() {
        return (
            <div>
                <Header title="test"/>
                <Action />
                <Options />
                <AddOptions />
            </div>
        )
    }
}

class Header extends React.Component {
    render() {
        return (
            <div>
                <h1>{ this.props.title }</h1>
                <h2> Put Your Life In the Hand Of A Computer </h2>
            </div>
        )
    }
}

/*result :
test
Put Your Life In the Hand Of A Computer */
```

Donc sur cet exemple on cromprend bien qu'utiliser la balise `<Header />` dans `IndecisionApp` revient a créer une nouvelle instance du component `Header`, et que lui donner un attribut `title="test"`revient à lui attribuer une propriété. <br>
Pour aller chercher cette propriété il faut utiliser le `this.props`qui correspond au propriétés de l'instance créer.

### Eviter de devoir set `bind()` à chaque fois :
Au lieu de devoir à chaque fois utiliser `bind()` lorsqu'on appelle une méthode du component, il est possible de le définir dans la déclaration du components directement :
```javascript
class Option extends React.Component {

    constructor (props) {
        //obligatoire pour pouvoir utiliser 'props' dans les méthode de l'instance du component
        super(props)
        this.handleRemoveAll = this.handle.handleRemoveAll.bind(this)
    }

    handleRemoveAll() {
        console.log(this.props.options)
    }
}
```
### React Component State :
Cela fait parti d'un des aspects les plus difficile à comprendre dans la librairie __React__, cependant c'est essentiel. <br>
React est **one-way-binding-data**, c'est à dire que **la vue n'est pas update en même temps que le model**, c'est pour cela que l'on doit a chaque fois re-render notre HTML. Cependant `React.Component` nous permet de déclarer des états de notre component : cela s'appelle le `state d'un component`. <br>
Dans un premier temps on doit déclarer les valeurs par défaut de notre application, lorsqu'il n'y a pas eut d'action par l'utilisateur. Pour comprendre le concept on utilise un compteur qui ajoute, diminue et reset un counter<br>
Pour définir le __state__ par défaut de l'instance créer on utilise la methode `this.state` définit dans le `constructor()` :
```javascript
class Counter extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            count: 0
        }
    }
}
```
Seulement voilà ici si on change la valeur de `this.state.count` cela n'affectera pas la vue de notre application et il faudra manuellement re-render la vue. Pour s'éviter ce travail supplémentaire, on peut utiliser une méthode possédée par chaque instances de `React.Component` : avec `setState()`. C'est ici que les choses deviennent un peu *'tricky'*, `setState()` demande en argument une **callback** dans laquelle on définie un objet avec les states que l'on souhaite modifier lors de l'evenement, il est important de comprendre que dans cette fonction on a accès en argument à l'état précedent `previousState` :
```javascript
    constructor(props) {
        super(props)
        this.handlePlusOne = this.handlePlusOne.bind(this)
        this.state = {
            count: 0
        }
    }

    handlePlusOne() {
        this.setState( (previousState) => {
            return {
                count : previousState.count += 1
            }
        })
    }
```
Ainsi sans avoir a coder, l'affichage sera automatiquement updater lors du click.<br>
__React__ ici fais tous le travail à notre place, et il n'update que les states que l'on a choisi de changer. Ainsi si il y avait d'autre propriétés dans `this.state` et que l'on avait pas choisi de les changer dans notre event, alors __React__ n'y toucherais pas et ne les changerais même pas, seulement à pour conséquence d'avoir un Framework très performant en terme de réactivité, c'est là que __React__ fait très fort.<br>

Il existe une ancienne méthode d'utiliser `this.state` qu'il faut éviter d'utiliser. D'après certaines rumeurs il est même dit que la prochaine version de __React__ ne permettra pas d'utiliser cette manière là. Au lieu de lui passer une fonction, on peut lui passer un objet directement :
```javascript
handleReset() {
    this.setState({
            count : this.state.count = 0
        }
    )
}
```
Le problème est que lorsque l'on a plusieurs `setState()` on peut avoir des problèmes de fonctionnement car il faut bien comprendre que `setState()` est une opération __asynchrone__. Donc :
```javascript
handleReset() {
    this.setState({
            count : this.state.count = 0
        }
    )
    this.setState({
            count : this.state.count + 1
        }
    )
}
// count ne sera jamais = 1 lorsqu'on clique sur le bouton reset
// count sera = previousState + 1
```
Et oui étant des opérations asynchrones le premier `setState()` n'aura pas le temps de modifier le `state`, javascript continuera son chemin et n'attendra pas, seulement le deuxième `setState()` sera  exécuté. C'est pour cett raison que `this.state.count` ne sera jamais égal à 0. <br> Cette méthode est donc a proscrire, voir à modifier si on la voit dans un code plus ancien. <br>
La bonne façon de le faire avec appel de **callbacks** :
```javascript
    handleReset() {
        //ici il n'est pas utile d'utiliser 'previousState'
        this.setState( () => {
            return {
                count: 0
            }
        })
        this.setState((previousState) => {
            return {
                //ici 'previousState.count' = 0
                count: previousState.count + 1
            }
        })
    }
```

---
### **Stateless Functional Component** :
Ce sont de simples fonctions, **où state n'est pas accessible**. Pour récuperer les props d'une __fonction Stateless Component__ il faut les récuperer dans le premier argument de la fonction :
```javascript
const User = (props) => {
    return (
        <div>
            <p>name : {props.name}</p>
            <p>age : {props.age}</p>
        </div>
    )
}

ReactDOM.render( <User name="axel" age={29}/>, appRoot )
```
#### Quel sont les avantages ?
* Plus performant en terme de rapiditité.
* Possède une meilleure lisibilité.
* Plus facile à tester.

Difference entre classe et fonction component :
```javascript
//Version classe component
class Options extends React.Component {
    render() {
        return (
            <div>
                    <h1>Options Component Here !</h1>
                    <button onClick= { this.props.handleDeleteOptions }>Remove All</button>
                    {
                    this.props.options.map((option) => <Option key={option} optionText={option} />)
                    }
            </div>
        )
    }
}
//==============================================================================
//version fonction component
const Options = (props) => {
        return (
            <div>
                    <h1>Options Component Here !</h1>
                    <button onClick= { props.handleDeleteOptions }>Remove All</button>
                    {
                    props.options.map((option) => <Option key={option} optionText={option} />)
                    }
            </div>
    )
}
```
Ainsi le `render()` disparait, l'appel se fait de la même manière en __JSX__ : `<Options />`.<br>
Donc dès que l'on a une classe component juste avec un render() à l'intérieur on la transformera a chaque fois en fonction component pour les avantages cités plus haut. On préfèrera en revanche la classe si on souhaite utiliser les __lifecycle methodes__, nous y reviendrons plus tard.

---

#### **Default Props** :
Il est possible d'établir des valeurs par défaut pour les props, grâce à `.defaultProps` contenu sur chaque components (classe ou fonction) :
```javascript
Header.defaultProps = {
    title: "Indecision App"
}
```

---
#### __React Dev Tools__ :
Il existe les dev tools de nos navigateurs mais aucun ne sont spécifiques à React et ne permettent pas de debugger très efficacement les fonctionnalités apportées par __React__. Le framework __React__ fourni sous forme  d'extension pour __Firefox__ et __Chrome__ :
* [React Dev Tool Firefox](https://addons.mozilla.org/fr/firefox/addon/react-devtools/ "Firefox Add-on")
* [React Dev Tool Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi "Chrome Add-on")
Cet Add-on nous donne accès à un onglet supplementaire dans le devTool du navigateur nommé `React`.

Ce qui est très interessant c'est qu'il est possible de **modifier beaucoup de code react**, comme nos props, nos state etc ... Ce n'est pas encore une fonctionnalités entierement finie car l'Add-on est encore en developpement : [Github Repo React Dev Tool](https://github.com/facebook/react-devtools). <br>
_A noter que comme sur nos navigateur avec `$0` il est possible de faire la meme chose dans react avec `$r`, c'est en effet une variable globale qui s'applique à l'élément selectionné et qui permet ensuite d'y avoir accès dans la console avec `$r`_

 ---
 #### LifeCycle Methodes :
 Sur les instances de nos classes components,__React__ nous donne la possibilité, à l'aide de méthodes, de connaitre l'état d'un component. Savoir si, dans l'instance d'un component, une propriété `state` à été changé, quand elle à été chargé et quand elle sera supprimé. __Ces méthodes ne sont accéssible que sur les classes__.
 ##### `componentDidMount()` :
 Cette méthode permet d'exécuter du code quand l'instance du component (classe)à été chargée

 `componentDidUpdate()` :
 Cette méthode permet d'exécuter du code quand le state de l'instance du component (classe) est changé.

`componentWillUnmount()` :
Cette méthode permet d'exécuter du code avant que l'instance du component (classe) ne soit supprimée.

Il en existe d'autres : [React Documentation Lifecycle Component](https://reactjs.org/docs/react-component.html)

---
### **[React Modal Components]('https://github.com/reactjs/react-modal')**
React Modal est un librairie permettant qui permet de créer facilement des components avec des options en plus comme l'accessibilité et un status (est ouvert ou non), cela permet de créer très facilement des pops up par exemple qui serait très difficile à créer voir impossible. <br>
pour l'utiliser il faut :<br>
Installer React Modal : `yarn add react-modal`<br>
Importer la librairie :
```javascript
import Modal from 'react-modal'
```
On peut ensuite créer une un `stateless function component` pour créer notre modal
```javascript
const OptionModal = (props) => (
    <Modal
        //Permet de définir si le modal est visible ou non
        isOpen={ !!props.selectedOption }
        //Pour les lecteur vocaux
        contentLabel='selected'
        //permet de choisir une action lorsque l'on press echap ou que l'on clique
        //du modal
        onRequestClose={ props.handleRemovePopup }
    >
        <div>
            { props.selectedOption && <p>{ props.selectedOption }</p> }
            <button onClick={ props.handleRemovePopup }>Okay !</button>
        </div>
    </Modal>
)
```
---
### **Customizer (SCSS) React Modal** :
React Modal nous est fourni avec des possibilités de customization. <br>
`closeTimeoutMS={200}` : Permet de donner en `ms` le temps que met le modal à se fermer.
Ensuite dans nos règles CSS :
```css
.ReactModalPortal > div {
    opacity: 0;
}
.ReactModalPortal .ReactModal__Overlay{
    transition: opacity 200ms ease-in-out;
}

.ReactModalPortal .ReactModal__Overlay--after-open {
    opacity: 1;
}

.ReactModalPortal .ReactModal__Overlay--before-close {
    opacity: 0;
}

``
React Modal nous donne la possibilité facilement de créer une classe pour appliquer des règles CSS, il suffit simplement d'ajouter la classe choisie dans le `<Modal >` avec `className="<class name>"`
