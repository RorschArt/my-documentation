# Redux :
## Installer Redux :
* `yarn add redux`
## Utiliser createStore :
```javascript
import { createStore } from 'redux'

const store = createStore((state = { count: 0 }) => {
    return state
})

console.log(store.getState())
```
## Utiliser Action :
`Action` est un methode utilisé dans `createStore` qui permet de changer le state du store :
Grace à la methode `dispatch({})` il est possible de set le state :

```javascript
store.dispatch({
    type: 'INCREMENT'
})
```
`type`est obligatoire.
Ainsi pour augmenter de 1 le `count` :
En revanche les `if()` sont difficile a lire on utilse a la place les `switch`.
```javascript
const store = createStore((state = { count: 0 }, action ) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + 1
            }
        case 'DECREMENT':
            return {
                count: state.count - 1
            }
        default:
            return state
    }
})

console.log(store.getState())

store.dispatch({
    type: 'INCREMENT'
})

console.log(store.getState())

store.dispatch({
    type: 'DECREMENT'
})

console.log(store.getState())

```
## Checker Les Changements dans Un Store :
`.subscribe()` permet de watch les changements, et a chaque fois qu'il y a changment elle permert de run une fonction.<br>
Pour stopper notre `.subscribe()` on utilise `unsubscribe()` :
## Les Actions Generators :
Ce sont simplement des fonctions qui retourne des objets Action.<br>
Elles permettent de factoriser notre code lorsqu'on utilise les Action. Ainsi au lieu d'écrire du code redondant on écrit. <br>
En plus de cela contrairement aux `.dispatch()` du dessus où on utilisait seulement des actions en ligne, ils permettent d'obtenir des erreurs, en effet avec juste une déclaration in line si il y a une erreur de syntaxe nous n'aurons pas d'erreur :
```javascript
// on met en valeur par defaut {} car si on ne le met pas et que l'on fait un
//store.dispatch(increment()), alors payload sera egale a undefined et chercher
//la methode de undefined donnera une erreur.
const increment = (payload = {}) => ({
    type: 'INCREMENT',
    incrementBy: typeof payload.incrementBy === "number" ? payload.incrementBy : 1
})

const store = createStore((state = { count: 0 }, action ) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            }
})
const unsubscribe = store.subscribe(() => {
    console.log('update !', store.getState())
})

store.dispatch(increment({ incrementBy: 5}))
```
## Shorthand c'est la Vie !
Pour donner plus de lisibilité a notre code utiliser du ES6 destructuring pour les valeur passer en argument nous donnera une bouffée d'air frais et un code plus facile à maintenir et a comprendre, car les noms des valeurs parlent d'elle même, plus besoin de devoir voir qu'elle valeur on été passer en argument lors de son appel :

```javascript
const add = (data, otherData) => {
    return data.a + data.b + otherData
}
console.log(add({ a:5, b: 10}, 1))
// NON !
```
Beaucoup mieux ! :
```javascript
const add = ({ a, b }, c) => {
    return a + b + c
}
console.log(add({ a:5, b: 10}, 1))
```
### Allons plus loin avec nos Action Generators !
L'utilité est évidente lorsque l'on arrive sur du code plus complexe, regardez moi la beauté du code en destructuring d'objets :
nous avons ceci :
```javascript
store.dispatch(increment({ incrementBy: 5}))
```
**Sans** Destructuring :
```javascript
const increment = (payload = {}) => ({
    type: 'INCREMENT',
    incrementBy: typeof payload.incrementBy === "number" ? payload.incrementBy : 1
})
```
**Avec** Destructuring :
```javascript
const increment = ({incrementBy} = 1 ={}) => ({
    type: 'INCREMENT',
    //incrementBy:incrementBy
    incrementBy
})
```
Ainsi plus besoin de ternaire, et la lisibilité est tellement supérieure !
## Reducer :
Les reducers sont de pure fonctions, elle dépendent seulement des variables située à l'interieur d'elle et n'interagissent pas avec celle qui sont à l'exterieure d'elle. <br>
En plus de cela elles ne changent pas les parametres qu'on leur fournis.
## Multiple Reducer :
On utilise de multiple Reducer car même s'il est possible de n'utiliser qu'un seul Reducer cela reviendrait à avoir un code très difficile à maintenir. Donc on découpe le Reducer en plusieurs.
Pour utiliser de multiple Reducers il nous faut d'abord importer :
```javascript
import { createStore, combineReducers } from 'redux'
```
Pour combiner des Reducers on utilise la fonction `combineReducers()` :
```javascript
const store = createStore(
    combineReducers ({
        expenses: expensesReducer,
        filters:  filtersReducer
    })
)
```
