# Local Storage :
**Toujours stocké en String**.<br>
Donc pour récupérer les données et les utiliser il faut __obligatoirement__ les parser : `JSON.parse()`.<br>
Pour les stocker il faudra les stringifier : `JSON.stringify()`
## Enregistrer Dans Local Storage :
* `localStorage.setItem( key : value)`
```javascript
localStorage.setItem( "livre": "Daytripper" )
```
## Charger Depuis Local Storage :
```javascript
localStorage.getItem("livre")
//"Daytripper"
```
## Supprimer Dans local Storage :
```javascript
//supprimer une clef de localstorage
localStorage.removeItem("livre")
//null
//supprimer la totalité
localStorage.length = 0
        //ou
localStorage.clear()
```
