# **Webpack** :
## **A quoi sert Webpack ?**
[Webpack](https://webpack.js.org/) permet d'utiliser des modules extérieurs (avec [NPM](https://www.npmjs.com/) et [Yarn](https://yarnpkg.com/en/)), et permet de découper notre application en de multiples fichiers pour une meilleure organisation, donc d'utiliser les modules ES6. <br> Beaucoup d'outils étaient disponible pour faire la même chose que __Webpack__ mais lui le fait d'une maniere unique : il utilise l'ES6 Modules import/export de manière intelligente, il ne charge que ce qu'il a besoin sans que l'on ne se soucis de rien. <br> Ainsi fini les successions de `<script>` ou l'on devait être attentif à l'ordre dans lequel on les chargés et les variables globales qui pollue notre application.<br>
Notre structure ressemblera à celle-ci :
* `public/ > index.html + bundle.js (webpack)`
* `src/ > app.js + utils.js`
* `node_modules/ > react.js + react-dom.js`

Au lieu d'avoir tous nos fichiers `.js` dans un dossier `public`

En plus de tout cela __Webpack__ permet de compresser tous nos fichiers `.js` et d'y intégrer Babel.<br>

---
## **Pourquoi il faut éviter d'installer les modules en Global ?**
Les modules ([Yarn](https://yarnpkg.com/en/) ou [NPM](https://www.npmjs.com/)) en global sont à éviter pour plusieurs raisons :
* Lorsque l'on partage un projet, les modules installés en global ne seront pas visibles dans le `package.json`de l'application, et ne seront donc pas installer lorsque nos collaborateurs feront un `npm install`, du coup comment savoir quel modules installer ? Aucun selon moi, a part demander au créateur...
* Il ne sera pas possible d'utiliser une version spécifique de nos modules, car en réalité chaque application fonctionne avec ses propres versions.
* Il sera obligatoire de taper la commande complète au lieu de n'avoir qu'a utiliser un alias comme `npm start` défini dans notre `package.json`
* Il ne sera pas possible de créer des scripts.

---
## **Configurer Webpack** :
Webpack est **Ultra customizable** et permet **de pouvoir s'adapter à tout type de projet** mais c'est également la partie la plus difficile à comprendre de webpack car il n'y a pas de bonne ou de mauvaise configuration, seulement **des bonnes et des mauvaises pratiques**. <br>
On arrive rapidement à devoir écrire des dizaine d'arguments dans la commande, c'est pourquoi webpack utilise un fichier a part pour pouvoir les spécifier.<br>
Le fichier de configuration __doit être dans le root de notre application__, c'est là que webpack le cherchera, sinon il ne le trouvera pas. Il doit s'appeler : `webpack.config.js`.<br>
Il s'agit en réalité d'un script nodeJS, on a donc accès à `module.exports`:
```javascript
    //Pour avoir accès à __dirname et toutes les méthodes se rapportant aux path
    // on a besoin du module 'path' contenu dans nodeJS avec un require
const path = require('path')

module.exports = {
    entry: src/app.js,
    output: {
    //le path Absolue de notre fichier depuis notre machine, pour éviter toute
    //erreur, on a accès à une variable dans nodeJS qui nous donne le path de
    //notre projet avec __dirname dans notre machine.
        path: path.join(__dirname, '/public'),
    //le nom du fichier final, en général bundle.js
        filename: 'bundle.js'
    }
}
```

---
## **Variables isolée** :
Les variables contenu dans un fichier séparé **sont totalement inconnus par `app.js`**, a moins qu'elles ne soient exportées.<br>

---
Ce comportement permet de ne **jamais polluer l'espace globale avec des variables**, a moins que l'on ne le fasse intentionnellement. Ainsi du code peut s'executer dans un fichier à part sans intéragir avec du code contenu dans un autre fichier. On obtient un code modulable, où l'on peut enlever, ajouter des modules si notre code est bien structuré.
## **Importer des modules** :
Importer des modules se déroule en plusieurs étape : `Installer -> Importer -> Utiliser`.
* On install le module via yarn `yarn add <moduleName>`
* On l'importe via la syntaxe ES6 modules : `import <name> from '<moduleName>'`.
* On utilise la variable avec ce que le module nous fourni.
La syntaxe est differente de celle de nodeJS (require) **qui utilise les module ES5** mais fonctionne exactement de la même manière.<br>
Malheureusement de temps à autre si on parcours le site du module via npm, il ne sera pas spécifié l'appel du module en ES6, il faudra alors soit le deviner, le googeler ou aller voir la Documentation s'il y en a une.

---
## **Loader** :
Le loader permet a webpack de savoir en fonction des fichiers les modules qu'il doit utiliser. <br>
Pour permettre a Webpack d'utiliser babel il faut dans un premier temps installer deux modules via yarn : `yarn add babel-core  babel-loader`, `babel-core est similaire à `babel-cli`, qui permet de run babel depuis une ligne de commande, mais lui depuis un tool comme webpack. `babel-loader` permet lui de pouvoir spécifier a __Webpack__ comment exécuter babel en fonction des fichiers qu'on lui fourni.

---
## **[Devtool]('https://webpack.js.org/configuration/devtool#devtool')** :
Devtool permet de définir la source map que l'on souhaite.
Ainsi au lieu d'avoir des erreurs dans la console qui se rapporte à notre `bundle.js` où tous notre code ES5 mignifié réside on peut avoir des erreurs qui se rapportent à nos fichiers d'origine, beaucoup plus utiles pour débugguer. Pour faire cela il faut écrire dans le `webpack.config.js`. Il faut bien comprendre qu'il n'existe pas qu'une seule source map mais beaucoup d'autres, certaines sont à utiliser en production d'autre en developpement, chacune ont leurs avantages, comme la performance par exemple et ces source map sont même customizable à volonter, webpack est un customizateur absolu, c'est pourquoi il est très utilisé et très difficile à maitriser entièrement.
 ```javascript
 module.exports = {
 devtool: 'cheap-module-eval-source-map'
}
 ```

---
## **[Dev-server]('https://webpack.js.org/configuration/dev-server')** :
 Webpack donne la possibilité à la manière de live-server de démarrer un server sur notre machine en localhost mais avec de meilleure performance quand on utilise webpack. Grace à lui il n'y aura plus besoin de run live-server, la commande webpack le fera tout seul.<br>
 Tout d'abord il faut l'installer avec `yarn add webpack-dev-server`. <br>
 Il faut donner le **path** de notre dossier public :
  ```javascript
  module.exports = {
  devServer: {
      contentBase: path.join(__dirname, '/public')
  }
 }
  ```
  Avec **devserver** webpack ne génère plus de fichier `bundle.js`, cela permet d'avoir de meilleure preformance et de générer plus rapidement notre page pour le développement.
---
## Définir Webpack Pour `React-Router` :
Pour que nos `<Route>` puissent fonctionner, c'est à dire que React-router pointe implicitement `index.html` il faut le lui spécifier dans webpack. Ainsi lorsque l'on utilisera le Route `/create`, webpack redirigera vers notre `index.html`, c'est provisoire, on changera le comportement lors du déploiement en prod.
```javascript
module.exports {
    devServer: {
        contentBase: path.join(__dirname, '/public'),
        //celui ci
        historyApiFallback: true
    }
}
```
Cela permet de dire à notre devServeur que l'on va faire du rooting depuis le client (REST) et qu'il faut renvoyer le `index.html` pour tous nos 404 route.
