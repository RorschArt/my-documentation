# **Babel**

## **[Plug-ins]('https://babeljs.io/docs/en/plugins')**

 ### class properties

 ```
 babel --plugins @babel/plugin-proposal-class-properties script.js
```

## Configurer Babel Pour pouvoir utiliser le très Utile **Spread Operator**

Ceci est un plug-in : [Babel-proposal-object-spread](https://babeljs.io/docs/en/babel-plugin-proposal-object-rest-spread)
* `yarn add babel-plugin-transform-object-rest-spread`

Puis dans `.babelrc`

```javascript
"plugins": [
    "transform-object-rest-spread"
]
```
