# NPM Modules :
## Commandes Utiles :
* `npm install <module_name> --save-dev` : Mettre un NPM Package en Dev dependency
* `npm uninstall -g babel-cli live-server` : Désinstaller un module installé en global

## Babel :
* `npm i babel-cli babel-preset-env` : Installer Babel.
* `npm babel-node index.js` : Demarrer __Node__ Avec Babel.
* `"nodemon src/index.js --exec babel-node" ` Demarrer __Nodemon__ avec Babel
