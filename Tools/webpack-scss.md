
# **Compiler SCSS avec Webpack** :
 `yarn add style-loader css-loader sass-loader node-sass` : installer les loader.
 Puis il suffit simplement de spécifier le type de fichiers et les loaders à utiliser dans `webpack.config.js` :
 ```javascript
 module : {
     //On défini les règles pour nos modules dans un tableau d'objets
     rules : [{
         loader: 'babel-loader',
     //on spécifie les fichiers sur lesquel executer babel
         test: /\.js$/,
         exclude: /node_modules/
     }, {
         test: /\.scss$/,
         //use permet d'utiliser plusieurs loaders, loader ne le permet pas
         use: [
             'style-loader',
             'css-loader',
             // 'node-sass', celui ci n'est pas a transmettre car il sert seulement a nodejs
             'sass-loader'
         ]

     }]
 },
 ```
 webpack sait maintenant quoi faire avec les fichiers css, il suffira simplement d'importer les fichiers dans notre component pour pouvoir appliquer le script chosi :
 ```javascript
import './style/style.scss'
```
---
## Structurer nos pages SCSS :
* Le dossier principal sera nommé `scripts`.
* La pages `style.scss`contiendra les `import` de nos page scss seulement.
* Un dossier nommé `base` contiendra tous les __partials__ de notre SCSS. Les partials dans SCSS sont en fait des fichiers pour dire à SASS qu'il ne faut pas les générer mais les utiliser pour générer le fichier final.
Pour créer des partials il faut :
* Placer dans le dossier `base` un fichier nommé `_<nom>.scss` sans oublier le **_**.
* Pour importer nos partials dans le fichier styles.scss il suffit de :
 ```scss
 @import './base/base';
```
On ne spécifie ni le **`_`** ni l'**extension**
---
## Utiliser les __rem__ en base de 10 :
Pour éviter de devoir faire une conversion à chaque fois, il existe un petit trick qui consiste à baisser en pourcentage la taille générale de notre <html>. 1 rem = 16px donc :
```css
html {
    font-size: 62.5%
}
```
Du coup avec cette conversion on peut facilement retrouver nos valeur en pixel, par exemple : <br>
`22 pixels = 2.2 rem`
---
## Appliquer du CSS au Components __React__ :
Utiliser le CSS dans une application React se fait relativement naturellement. Il suffit d'utiliser les **classe** et les **id** sur notre **JSX** :
```html
<div className="header"></div>
```
---
## [BEM](http://getbem.com/) Convention :
Il existe une convention pour plus de lisibilité, le **Block Element Modifier**, qui permet de déterminer un selecteur principal et des __modifiers__ qui utiliseront ce selecteur mais rajoutera des règles css pour au besoin. Cela permet de créer une sorte d'héritage et surtout de pouvoir lire beaucoup plus facilement à quoi correspondent les sélecteurs. Un peu à la manière de **Bootstrap** :

---
## Installer le reset (`css-normalize`) :
* `yarn add normalize.css` : commande Yarn pour l'installer.
* `import 'normalize.css/normalize.css'` <br>
Seulement il y a un problème avec notre configuration webpack, si cela n'a pas été changé on a configurer webpack pour ne charger que les fichiers scss et non pas les css, donc webpack ne pourra pas trouver le __reset__. Il faut donc changer le fichier de configuration `webpack.config.js` :
```javascript
module : {
    //On défini les règles pour nos modules dans un tableau d'objets
    rules : [{
        test: /\.s?css$/,
        //use permet d'utiliser plusieurs loaders, loader ne le permet pas
        use: [
            'style-loader',
            'css-loader',
            'sass-loader'
        ]
    }]
```
En ajoutant un `?`, cela équivaut en expression régulière à rendre un caractere optionnel, en d'autre therme webpack va charger des fichiers soit **CSS** soit **SCSS**.
---
## Séparer Nos variables :
On créer un fichier nommé `setting.scss` où toutes nos variables seront stockées, ainsi on aura tous au même endroit, si il y a quelquechose à modifier à notre application il suffira d'aller dans ce fichier.<br>
**Puis on l'importe dans `styles.scss`**.

 ---
 ## Responsive Web Application :

 * `<meta name="viewport" content="width=device-width, initial-scale=1">` à définir dans `index.html`. Cela permet de dire au device d'utiliser le **viewport width** du mobile ou autre.
