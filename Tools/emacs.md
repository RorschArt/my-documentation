# Emacs

## Simple Shortcuts :

```
C-f	Avance d'un caractère
C-b	Recule d'un caractère

M-f	Avance d'un mot
M-b	Recule d'un mot

C-n	Avance d'une ligne
C-p	Recule d'une ligne

C-a	Va au début de la ligne
C-e	Va à la fin de la ligne

M-a	Va au début de la phrase
M-e	Va à la fin de la phras

```
```
Si Emacs cesse de répondre à vos commandes, vous pouvez le débloquer
en toute sécurité avec C-g. Cette commande fait stopper une commande
qui met trop de temps à s'exécuter.
```

1 fenetre et la supprimer

```
 C-h k C-f.
  C-x 1
```

 ```
 <DEL>    Efface le caractère situé avant le curseur
 	C-d   	 Efface le caractère situé après le curseur

 	M-<DEL>  Supprime le mot situé avant le curseur
 	M-d	 Supprime le mot situé après le curseur

 	C-k	 Supprime du curseur à la fin de la ligne
 	M-k	 Supprime jusqu'à la fin de la phrase courante
 ```

 ```
 M-W Copier

 C-x C-s   Sauvegarde le fichier
 C-X C-F trouver un fichier

 C-X C-B liste des tampons
 C-X B rechercher un tampon et y entrer
 C-X 1 revenir aune fenetre
 ```

```
C-X S Sauvegarder (demande Confirmation)
C-X C-S Sauvegarder
C-X C-C Quitter
```

```
C-x C-f		Trouve un fichier.
C-x C-s		Sauvegarde un fichier.
C-x C-b		Liste des tampons.
C-x C-c		Quitte Emacs.
C-x 1		Détruit toutes les fenêtres, sauf une.
C-x u		Annulation.
```
```
M-X 'text-mode' changer le language d'edition

C-S rechercher
C-S pour aller sur l'occurance
```

```
C-x 2 Diviser en deux fenetre
C-M-v Defiler dans la deuxieme fenetre
C-X-o pour mettre le cursueur sur la deuxieme fenetre
```

```
M-x make-frame creer un cadre
M-x delete-frame Supprimer un cadre
