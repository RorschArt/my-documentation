package fr.ippon.exercise;
import java.math.*;

public class Solution {
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
	public BigDecimal RESULT = BigDecimal.ZERO;

	private static final int DECIMAL_PRECISION = 5;
	private BigDecimal ONE_DICE_PROBA_TO_BE_SIX = BigDecimal.ZERO;
	private BigDecimal DICES_COUNT = new BigDecimal("1");
	private BigDecimal NUMBER_OF_FACES_PER_DICE = new BigDecimal("6");

    public BigDecimal throwDices(int dicesCount, int facesCount) {

		System.out.println(allTestsValidate());
		return BigDecimal.ZERO;

		// return calculateAtLeastOneOfDicesBeSixProba(dicesCount, facesCount);
    }

	public BigDecimal calculateAtLeastOneOfDicesBeSixProba (int dicesCount, int facesCount) {
		setFacesCountGlobal(new BigDecimal(String.valueOf(facesCount)));
		setDicesCountGlobal(new BigDecimal(String.valueOf(dicesCount)));

		double result = applyBinomialDistributionRecursive(DICES_COUNT.intValue());
		System.out.println("result: " + result);
		RESULT = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(result)));

		return RESULT;
	}

	public double applyBinomialDistributionRecursive(int counter) {
		int totalDicesCount 		 = DICES_COUNT.intValue();
		int nFactor 				 = (totalDicesCount+1) - counter;

		double successFactor 		 = calculateSingleDiceProbabilitySetToGlobal().doubleValue();
		double successExponent 		 = counter;
		double successExponentResult = Math.pow(successFactor, successExponent);

		double failureFactor 		 = 1 - ONE_DICE_PROBA_TO_BE_SIX.doubleValue();
		double failureExponent 	     = totalDicesCount - counter;
		double failureExponentResult = Math.pow(failureFactor, failureExponent);

		if (counter <= 1) {
			return successFactor * failureExponentResult * nFactor;
		}

		return successExponentResult + applyBinomialDistributionRecursive(counter - 1);
	}

	public BigDecimal calculateSingleDiceProbabilitySetToGlobal() {
		double oneDiceProbaToBeSix = 1 / NUMBER_OF_FACES_PER_DICE.doubleValue();
		BigDecimal oneDiceProbaToBeSixDB = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(oneDiceProbaToBeSix)));
		setGlobalOneDiceProbaToBeSix(oneDiceProbaToBeSixDB);
		return oneDiceProbaToBeSixDB;
	}

	public double roundDouble(BigDecimal numberToRound) {
		BigDecimal numberToRoundDB = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(numberToRound)));
		double numberToRoundDouble = numberToRound.doubleValue();
		return numberToRoundDouble;
	}

// Setters
	public void setGlobalOneDiceProbaToBeSix(BigDecimal oneDiceProba) {
	 	ONE_DICE_PROBA_TO_BE_SIX = setScaleBigDecimalToFive(oneDiceProba);
	}

	public BigDecimal setScaleBigDecimalToFive(BigDecimal bigDecimalNumber) {
		return bigDecimalNumber.setScale(5, ROUNDING_MODE);
	}

	public void setFacesCountGlobal(BigDecimal facesCount) {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal(String.valueOf(facesCount));
	}

	public BigDecimal setDicesCountGlobal(BigDecimal dicesCount) {
		return  DICES_COUNT = dicesCount;
	}

	public void resetGlobalFacesCountVariable() {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal("6");
	}

	public void resetGlobalOneDiceProbaToBeSixToZero() {
		ONE_DICE_PROBA_TO_BE_SIX = BigDecimal.ZERO;
	}

	public void resetResultGlobalVariable() {
		RESULT = BigDecimal.ZERO;
	}

//=============================================================================
							/* PARTIE TEST */

	public boolean allTestsValidate () {

		return checksetScaleBigDecimalToFive()
				&& checkResetGlobalOneDiceProbaToBeSixToZero()
				&& checkCalculateAtLeastOneOfDicesBeSixProba()
				&& checkResetResultGlobalVariable()
				&& checkResetGlobalFacesCount()
				&& checkSetGlobalOneDiceProbaToBeSix()
				&& checkSetFacesCountToGlobal();
	}

	public boolean checkCalculateAtLeastOneOfDicesBeSixProba() {
		BigDecimal expectedResult = new BigDecimal("0.30556");
		int dicesCount1 = 2;
		int facesCount1 = 6;

		BigDecimal probabilityCalculate = calculateAtLeastOneOfDicesBeSixProba(dicesCount1, facesCount1);

		if (!probabilityCalculate.equals(expectedResult))
		{
			throw new IllegalArgumentException("calculateAtLeastOneOfDicesBeSixProba est invalide. Voulu: "
			+ expectedResult + " reçut: " + probabilityCalculate);
		}

		resetResultGlobalVariable();
		resetGlobalOneDiceProbaToBeSixToZero();
		return true;
	}

	public boolean checksetScaleBigDecimalToFive() {
		BigDecimal expectedResult1 = new BigDecimal("1.12346");
		BigDecimal expectedResult2 = new BigDecimal("1.12000");
		BigDecimal bigDecimal1 = setScaleBigDecimalToFive(new BigDecimal("1.123456789"));
		BigDecimal bigDecimal2 = setScaleBigDecimalToFive(new BigDecimal("1.12"));

		if (!bigDecimal1.equals(expectedResult1) || !bigDecimal2.equals(expectedResult2)) {
			throw new IllegalArgumentException("setScaleBigDecimalToFive est invalide.");
		}

		return true;
	}

	public boolean checkResetResultGlobalVariable() {
		RESULT = new BigDecimal("1234");
		resetResultGlobalVariable();

		if (RESULT.equals(new BigDecimal("1234"))) {
			throw new IllegalArgumentException("resetResultGlobalVariable est invalide.");
		}

		return true;
	}

	public boolean checkSetFacesCountToGlobal() {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal("100");
		BigDecimal expectedResult = new BigDecimal("100");
		setFacesCountGlobal(new BigDecimal("100"));

		if (!NUMBER_OF_FACES_PER_DICE.equals(expectedResult)) {
			throw new IllegalArgumentException("setFacesCountToGlobal est invalide.");
		}
		resetGlobalFacesCountVariable();
		return true;
	}

	public boolean checkResetGlobalFacesCount() {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal("1234");

		resetGlobalFacesCountVariable();

		if (!NUMBER_OF_FACES_PER_DICE.equals(new BigDecimal("6"))) {
			throw new IllegalArgumentException("resetGlobalFacesCount est invalide.");
		}

		checkResetGlobalFacesCount();
		return true;
	}

	public boolean checkSetGlobalOneDiceProbaToBeSix() {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal("6");
		setGlobalOneDiceProbaToBeSix(BigDecimal.ONE.divide(NUMBER_OF_FACES_PER_DICE));

		if (ONE_DICE_PROBA_TO_BE_SIX.equals(BigDecimal.ZERO) || !ONE_DICE_PROBA_TO_BE_SIX.equals(new BigDecimal("0.16667")))
		{
			throw new IllegalArgumentException("setGlobalOneDiceProbaToBeSix est invalide.");
		}

		resetGlobalOneDiceProbaToBeSixToZero();
		return true;
	}

	public boolean checkResetGlobalOneDiceProbaToBeSixToZero() {
		BigDecimal oneDiceProbaChecker = new BigDecimal("12345");
		ONE_DICE_PROBA_TO_BE_SIX = oneDiceProbaChecker;
		resetGlobalOneDiceProbaToBeSixToZero();

		if (ONE_DICE_PROBA_TO_BE_SIX.equals(oneDiceProbaChecker) || !ONE_DICE_PROBA_TO_BE_SIX.equals(BigDecimal.ZERO))
		{
			throw new IllegalArgumentException("resetGlobalOneDiceProbaToBeSixToZero est invalide.");
		}

		return true;
	}
}
