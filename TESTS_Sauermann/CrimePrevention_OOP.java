package fr.ippon.exercise;
import java.util.*;

public class Solution {

	public enum CountryCode {
		FRANCE("33"), AMERICA("1");

		private String countryCode;

		private CountryCode(String code) {
			this.countryCode = code;
		}

		public String getCountryCode() {
			return this.countryCode;
		}
	}

	private static final Random RANDOM = new Random();
	private static final String[] ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA = {"R", "-", "5"};
	private String SCREEN = "";
	private String COUNTRY_CODE_SELECTED;
	private HashSet<Integer> UNIQUE_NUMBERS = new HashSet<Integer>();

	public String predictCrimes() {
		COUNTRY_CODE_SELECTED = CountryCode.AMERICA.getCountryCode();

		printCountryDaysPredictionOnScreen(30);

		return SCREEN;
	}

	public void printCountryDaysPredictionOnScreen(int totalDaysOfPrediction) {
		for (int i=0; i < totalDaysOfPrediction; i++)
		{
			displayDayPredictionOnScreen();
		}
	}

	public void displayDayPredictionOnScreen() {
		SCREEN += ("Crime predicted: " + generateCriminalSocialSecurity() + "\n");
		SCREEN += ("Calling: " + generateCriminalPhoneNumber() + "\n");
	}

	public String generateCriminalSocialSecurity() {
		// R pour Random Digit
		return generateDigitFormated("RRR-RR-RRRR");
	}

	public String generateCriminalPhoneNumber() {
		// R pour Random Digit
		return new String().concat(COUNTRY_CODE_SELECTED + generateDigitFormated("-RRR-5555-RRRR"));
	}

	public String generateDigitFormated(String formatSchema) {
		String[] characterExctracted = formatSchema.split("");
		String result = "";
		int lengthOfDigitToGenerate = 0;
		String currentString;
		boolean isLastString;
		boolean isValidFormatSchema
		= areStrictlyContainedInArray(ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA, characterExctracted);

		for(int i=0; i < characterExctracted.length; i++)
		{
			currentString = characterExctracted[i];
			isLastString = i == characterExctracted.length - 1;

			if (!isValidFormatSchema) {
				throw new IllegalArgumentException("formatSchema must be only 5, '-' or R !");
			}

			if (currentString.equals("R") && !isLastString) {
				lengthOfDigitToGenerate += 1;
				continue;
			}

			if (currentString.equals("-") && lengthOfDigitToGenerate > 0) {
				result += generateRandomUniqueNumber(lengthOfDigitToGenerate);
				lengthOfDigitToGenerate = 0;
			}

			if (isLastString && currentString.equals("R")) {
				lengthOfDigitToGenerate += 1;
				result += generateRandomUniqueNumber(lengthOfDigitToGenerate);
				lengthOfDigitToGenerate = 0;
				continue;
			}
			result += currentString;
    	}
		return result;
	}

	public String generateRandomUniqueNumber(int digitLength) {
		int minValue =  (int) (1 * Math.pow(10f, (float)digitLength - 1));
		int maxValue =  (int) (10 * Math.pow(10f, (float)digitLength - 1));
		int randomNumber;

		do {
			randomNumber = minValue + RANDOM.nextInt(maxValue - minValue);
		} while(!UNIQUE_NUMBERS.add(randomNumber));

		return String.valueOf(randomNumber);
	}

	public boolean areStrictlyContainedInArray(String[] stringsToCheck, String[] array) {
		List<String> stringsAsList = Arrays.asList(stringsToCheck);

		for (String stringAllowed : array)
		{
			if (!stringsAsList.contains(stringAllowed))
				return false;
		}
		return true;
	}
}
