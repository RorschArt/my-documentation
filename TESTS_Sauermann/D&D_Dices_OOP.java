package fr.ippon.exercise;
import java.math.*;

public class Solution {
	public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
	public BigDecimal RESULT = BigDecimal.ZERO;

	private static final int DECIMAL_PRECISION = 5;
	private BigDecimal ONE_DICE_PROBA_TO_BE_SIX = BigDecimal.ZERO;
	private BigDecimal DICES_COUNT = new BigDecimal("1");
	private BigDecimal NUMBER_OF_FACES_PER_DICE = new BigDecimal("6");

    public BigDecimal throwDices(int dicesCount, int facesCount) {

		System.out.println(allTestsValidate());
		return BigDecimal.ZERO;

		// return calculateAtLeastOneOfDicesBeSixProba(dicesCount, facesCount);
    }

	public BigDecimal calculateAtLeastOneOfDicesBeSixProba (int dicesCount, int facesCount) {
		setFacesCountGlobal(new BigDecimal(String.valueOf(facesCount)));
		setDicesCountGlobal(new BigDecimal(String.valueOf(dicesCount)));

		double result = applyBinomialDistributionRecursive(DICES_COUNT.intValue());
		System.out.println("result: " + result);
		RESULT = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(result)));

		return RESULT;
	}

	public double applyBinomialDistributionRecursive(int counter) {
		int totalDicesCount 		 = DICES_COUNT.intValue();
		int nFactor 				 = (totalDicesCount+1) - counter;

		double successFactor 		 = calculateSingleDiceProbabilitySetToGlobal().doubleValue();
		double successExponent 		 = counter;
		double successExponentResult = Math.pow(successFactor, successExponent);

		double failureFactor 		 = 1 - ONE_DICE_PROBA_TO_BE_SIX.doubleValue();
		double failureExponent 	     = totalDicesCount - counter;
		double failureExponentResult = Math.pow(failureFactor, failureExponent);

		if (counter <= 1) {
			return successFactor * failureExponentResult * nFactor;
		}

		return successExponentResult + applyBinomialDistributionRecursive(counter - 1);
	}

	public BigDecimal calculateSingleDiceProbabilitySetToGlobal() {
		double oneDiceProbaToBeSix = 1 / NUMBER_OF_FACES_PER_DICE.doubleValue();
		BigDecimal oneDiceProbaToBeSixDB = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(oneDiceProbaToBeSix)));
		setGlobalOneDiceProbaToBeSix(oneDiceProbaToBeSixDB);
		return oneDiceProbaToBeSixDB;
	}

	public double roundDouble(BigDecimal numberToRound) {
		BigDecimal numberToRoundDB = setScaleBigDecimalToFive(new BigDecimal(String.valueOf(numberToRound)));
		double numberToRoundDouble = numberToRound.doubleValue();
		return numberToRoundDouble;
	}

// Setters
	public void setGlobalOneDiceProbaToBeSix(BigDecimal oneDiceProba) {
	 	ONE_DICE_PROBA_TO_BE_SIX = setScaleBigDecimalToFive(oneDiceProba);
	}

	public BigDecimal setScaleBigDecimalToFive(BigDecimal bigDecimalNumber) {
		return bigDecimalNumber.setScale(5, ROUNDING_MODE);
	}

	public void setFacesCountGlobal(BigDecimal facesCount) {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal(String.valueOf(facesCount));
	}

	public BigDecimal setDicesCountGlobal(BigDecimal dicesCount) {
		return  DICES_COUNT = dicesCount;
	}

	public void resetGlobalFacesCountVariable() {
		NUMBER_OF_FACES_PER_DICE = new BigDecimal("6");
	}

	public void resetGlobalOneDiceProbaToBeSixToZero() {
		ONE_DICE_PROBA_TO_BE_SIX = BigDecimal.ZERO;
	}

	public void resetResultGlobalVariable() {
		RESULT = BigDecimal.ZERO;
	}
}
