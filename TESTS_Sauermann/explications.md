<h1>Tests <b>Axel Daguerre</b> ✍️</h1>
<p>
Bonjour,<br>
Pour que vous comprenez un peu comment j'ai procédé durant mes test je vais vour
décrire ma philosophie rapidement.<br>
Chaque exercices ont deux versions, une version sans les tests et avec les tests sur la page. <br>
 <br>
 </p>
 <p>
D'après ce que j'ai compris de mes lectures dans la philosophie de l'OOP programming
il faut "cacher" au maximum les algorithmes complexes, il faut séparer au maximum
l'interface (partie abstraite) de la manipulation des données. Donc j'ai tenté de
penser comme si un collègue allait devoir toucher à mon code, donc lui fournir des
méthode facile à comprendre avec des noms qui ont du sens et avec un soucis de flexibilité
si on devrait faire évoluer notre classe (même si je sais que ce n'est pas parfaitement fait).<br>
</p>
<h2>Exercice <b>Crime Predicitions</b> 🕵️‍♂️</h2
<p>
Sur cet exercice c'est je pense l'endroit où j'ai le plus réussi à implémenter
des methodes réutilisables. J'ai fait des choses qui n'étais pas demandé j'aurais pu
faire un code plus court, plus lisible du coup mais étant parti sur la réutilisabilité
j'ai continuer sur cette lancée.
</p>
<h2>Exercices <b>Dés</b> 🎲</h2
<p>
Sur cet exercice j'ai eut beaucoup de mal à trouvé une formule qui puisse m'aider.
Du coup j'ai passé beaucoup de temps sur l'élaboration de la fonction traitant
la formule, je trouves qu'une fonction récursive se prété à l'exercice du coup
j'ai fait ma première fonction récursive 😀. Malheureusement je n'ai pas eut le
temps de regarder pourquoi elle nemarche pas avec plus de deux dés vu que je dois vous
rendre les tests aujourd'hui. Il doit y avoir une erreur sur l'écriture j'imagine.
J'ai utilisé les <code>BigDecimal</code> car ils étaient fournis dans l'exercice mais
je pense que j'ai fait une erreur,j'aurias pu faire les arrondi à la main je pense car
ça rend le code peu lisible jetrouve.
</p>
<h2>Exercices <b>Salade de fruit</b> 🥝</h2
<p>
Je n'ai pas eut le temps de le faire, je suis désolé.
</p>
<p>
Pour conclure je suis assez déçus de pas avoir pu réussir vos tests. Je vais continuer
ce genre de tests et peut etre qu'un jour ça sera facile pour moi !<br>
Je ne suis pas certains que c'était ce qui était demandé et que ce soit une bonne
pratique n'est pas du tout bonne je serais ravi que vous m'expliquiez pourquoi. <br>
Et si vous avez tout autres conseils ça m'interesse vraiment.
Bonne lecture.
</p>
