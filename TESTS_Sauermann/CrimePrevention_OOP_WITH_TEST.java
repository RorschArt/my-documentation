package fr.ippon.exercise;
import java.util.*;

public class Solution {

	public enum CountryCode {
		FRANCE("33"), AMERICA("1");

		private String countryCode;

		private CountryCode(String code) {
			this.countryCode = code;
		}

		public String getCountryCode() {
			return this.countryCode;
		}
	}

	private static final Random RANDOM = new Random();
	private static final String[] ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA = {"R", "-", "5"};
	private String SCREEN = "";
	private String COUNTRY_CODE_SELECTED;
	private HashSet<Integer> UNIQUE_NUMBERS = new HashSet<Integer>();

	public String predictCrimes() {
		COUNTRY_CODE_SELECTED = CountryCode.AMERICA.getCountryCode();

		if (!allTestsValidate()) {
			throw new IllegalArgumentException("Les tests ne sont pas satisfaits.");
		}

		printCountryDaysPredictionOnScreen(30);

		return SCREEN;
	}

	public void printCountryDaysPredictionOnScreen(int totalDaysOfPrediction) {
		for (int i=0; i < totalDaysOfPrediction; i++)
		{
			displayDayPredictionOnScreen();
		}
	}

	public void displayDayPredictionOnScreen() {
		SCREEN += ("Crime predicted: " + generateCriminalSocialSecurity() + "\n");
		SCREEN += ("Calling: " + generateCriminalPhoneNumber() + "\n");
	}

	public String generateCriminalSocialSecurity() {
		// R pour Random Digit
		return generateDigitFormated("RRR-RR-RRRR");
	}

	public String generateCriminalPhoneNumber() {
		// R pour Random Digit
		return new String().concat(COUNTRY_CODE_SELECTED + generateDigitFormated("-RRR-5555-RRRR"));
	}

	public String generateDigitFormated(String formatSchema) {
		String[] characterExctracted = formatSchema.split("");
		String result = "";
		int lengthOfDigitToGenerate = 0;
		String currentString;
		boolean isLastString;
		boolean isValidFormatSchema
		= areStrictlyContainedInArray(ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA, characterExctracted);

		for(int i=0; i < characterExctracted.length; i++)
		{
			currentString = characterExctracted[i];
			isLastString = i == characterExctracted.length - 1;

			if (!isValidFormatSchema) {
				throw new IllegalArgumentException("formatSchema must be only 5, '-' or R !");
			}

			if (currentString.equals("R") && !isLastString) {
				lengthOfDigitToGenerate += 1;
				continue;
			}

			if (currentString.equals("-") && lengthOfDigitToGenerate > 0) {
				result += generateRandomUniqueNumber(lengthOfDigitToGenerate);
				lengthOfDigitToGenerate = 0;
			}

			if (isLastString && currentString.equals("R")) {
				lengthOfDigitToGenerate += 1;
				result += generateRandomUniqueNumber(lengthOfDigitToGenerate);
				lengthOfDigitToGenerate = 0;
				continue;
			}
			result += currentString;
    	}
		return result;
	}

	public String generateRandomUniqueNumber(int digitLength) {
		int minValue =  (int) (1 * Math.pow(10f, (float)digitLength - 1));
		int maxValue =  (int) (10 * Math.pow(10f, (float)digitLength - 1));
		int randomNumber;

		do {
			randomNumber = minValue + RANDOM.nextInt(maxValue - minValue);
		} while(!UNIQUE_NUMBERS.add(randomNumber));

		return String.valueOf(randomNumber);
	}

	public boolean areStrictlyContainedInArray(String[] stringsToCheck, String[] array) {
		List<String> stringsAsList = Arrays.asList(stringsToCheck);

		for (String stringAllowed : array)
		{
			if (!stringsAsList.contains(stringAllowed))
				return false;
		}
		return true;
	}

//==============================================================================
							/* PARTIE TEST */

	public boolean allTestsValidate() {
		return checkStringsAreStrictlyContainedInArray()
				&& checkGenerateRandomUniqueNumber()
				&& checkGenerateDigitFormated()
				&& checkGenerateCriminalSocialSecurity()
				&& checkGenerateCriminalPhoneNumber()
				&& checkdisplayDayPredictionOnScreen();
	}

	public boolean checkdisplayDayPredictionOnScreen() {
		displayDayPredictionOnScreen();
		String expectedScreenForPredictionDay = "";
		String digitsNumbersInLogScreen = SCREEN.replaceAll("[a-zA-Z]|[\\s]", "");
		String[] digitArray = digitsNumbersInLogScreen.split(":");
		String SocialSecurityNumber = digitArray[1];
		String phoneNumber = digitArray[2];

		expectedScreenForPredictionDay += ("Crime predicted: " + SocialSecurityNumber + "\n");
		expectedScreenForPredictionDay += ("Calling: " + phoneNumber + "\n");

		if (!expectedScreenForPredictionDay.equals(SCREEN)) {
			throw new IllegalArgumentException("checkdisplayDayPredictionOnScreen invalide: Ne display pas le résultat expecté");
		}

		SCREEN = "";
		return true;
	}

	public boolean checkGenerateCriminalSocialSecurity() {
		String socialSecurityNumber = generateCriminalSocialSecurity();
		String[] socialSecurityDigitsOnly = socialSecurityNumber.split("-");

		boolean firstDigitIsValid  = socialSecurityDigitsOnly[0].length() == 3;
		boolean secondDigitIsValid = socialSecurityDigitsOnly[1].length() == 2;
		boolean thirdDigitIsValid  = socialSecurityDigitsOnly[2].length() == 4;

		if (!firstDigitIsValid || !secondDigitIsValid || !thirdDigitIsValid)
		{
			throw new IllegalArgumentException("checkGenerateCriminalSocialSecurity invalide: mauvais format");
		}

		try {
			Long.parseLong(socialSecurityNumber.replace("-", ""));
		}
		catch(NumberFormatException e) {
			throw new IllegalArgumentException("checkGenerateCriminalSocialSecurity invalide: ne retourne pas de numéros de sécurité sociale de type nombre seulement.");
		}

		return true;
	}

	public boolean checkGenerateCriminalPhoneNumber() {
		String phoneNumberFormated = generateCriminalPhoneNumber();
		String[] phoneNumberDigitsOnly = phoneNumberFormated.split("-");

		boolean firstDigitIsValid  = phoneNumberDigitsOnly[0].equals(COUNTRY_CODE_SELECTED);
		boolean secondDigitIsValid = phoneNumberDigitsOnly[1].length() == 3;
		boolean thirdDigitIsValid  = phoneNumberDigitsOnly[2].equals("5555");
		boolean fourthDigitIsValid = phoneNumberDigitsOnly[3].length() == 4;

		if (!firstDigitIsValid || !secondDigitIsValid || !thirdDigitIsValid || !fourthDigitIsValid)
		{
			throw new IllegalArgumentException("generateCriminalPhoneNumber invalide: mauvais format");
		}

		try {
			Long.parseLong(phoneNumberFormated.replace("-", ""));
		}
		catch(NumberFormatException e) {
			throw new IllegalArgumentException("generateDigitFormated invalide: ne retourne pas de numéros de téléphone de type nombre seulement.");
		}
		return true;
	}

	public boolean checkGenerateDigitFormated() {
		String badFormatSchema   = "A-2-555-RRRR";
		String goodFormatSchema1 = "RR-555-RRRR";
		String goodFormatSchema2 = "5-RRR-555";

		String goodDigitFormated1 = generateDigitFormated(goodFormatSchema1);
		String goodDigitFormated2 = generateDigitFormated(goodFormatSchema2);

		String wholeNumbergoodFormatSchema1 = goodDigitFormated1.replace("-", "");
		String wholeNumbergoodFormatSchema2 = goodDigitFormated2.replace("-", "");

		try {
			Integer.parseInt(wholeNumbergoodFormatSchema1);
			Integer.parseInt(wholeNumbergoodFormatSchema2);
			generateDigitFormated(badFormatSchema);
		}
		catch(NumberFormatException e) {
			// catch l'erreur du Integer.parseInt()
			throw new IllegalArgumentException("generateDigitFormated invalide: ne retourne pas de digit de type nombre seulement.");
		}
		catch (IllegalArgumentException e) {
			// catch l'erreur du badFormatSchema
			return true;
		}
		throw new IllegalArgumentException("generateDigitFormated invalide: devrait ne pas permettre un mauvais format.");
	}

	public boolean checkGenerateRandomUniqueNumber() {
			int lengthNumberWanted = 5;
			String randomNumber = generateRandomUniqueNumber(lengthNumberWanted);

			if (randomNumber.length() != lengthNumberWanted)
			{
				throw new IllegalArgumentException("generateRandomUniqueNumber invalide: longueur du nombre.");
			}

			try {
				Integer.parseInt(randomNumber);
			}
			catch(IllegalArgumentException e) {
					throw new IllegalArgumentException("areStrictlyContainedInArray invalide: pas un nombre.");
			}
			return true;
	}

	public boolean checkStringsAreStrictlyContainedInArray () {
		String[] goodStringsTocheck = {"5","-","R"};
		String[] badStringsTocheck = {"A","5","-","R"};

		if (areStrictlyContainedInArray(ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA, goodStringsTocheck)
		&& !areStrictlyContainedInArray(ALLOWED_CHARACTERES_IN_FORMAT_SCHEMA, badStringsTocheck))
		{
			return true;
		}

		throw new IllegalArgumentException("areStrictlyContainedInArray invalide");
	}
}
