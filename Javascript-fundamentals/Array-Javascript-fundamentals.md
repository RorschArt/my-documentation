# **Tips Utiles Dans Array** :
## `Array.map()` :

## `Array.concat()` :

## `Array.indexOf(value)` :
Retourne l'index de la valeur s'il est contenue dans le tableau. __Retourne `-1` si l'élément n'a pas été trouvé dans le tableau.__<br>
Cette méthode est utile pour :

* **Vérifier si une valeur est déjà présente dans le tableau.** <br>

Exemple d'utilisation :

* On souhaite retourner une erreur s'il existe déjà :

```javascript
handleAddOption(option) {
if (this.state.options.indexOf(option) > -1) {
    return 'l\'option existe déjà'
}
```
