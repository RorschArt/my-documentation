# __Javascript's fundamentals__

## __Callbacks__ :
Pour Comprendre les Callbacks il faut tout d'abord Comprendre que l'on ne peux pas stocker Dans une variable une un fonction qui doit retourner un resultat en __Asynchrone__ par exemple :
```javascript
function spoilGameOfThrones(){
    setTimeout(() => {
        const spoil = "**** a tuer la reine"
        return spoil
    }, 2000)
}

const spoil = spoilGameOfThrones()
```
On ne pourra pas faire cela car le resultat met du temps a arriver, et __javascript lui n'attends pas !__

Donc lorsque le code s'execute, javascript va executer la fonction `spoilGameOfThrones` lorsqu'il voudra donner une valeur a la variable `spoil`.<br>

Seulement la fin de l'execution, le `return`, est situe dans une operation __Asynchrone__ donc qui va mettre du temps, `2000ms` ici car `setTimeout` nous permet de Simuler cette operation. Le resultat de `spoil` sera __`undefined`__<br>
Le probleme est que __Javascript__ n'est pas un __multithread__ il ne peux donc pas realiser plusieurs operations en meme temps, il doit donc ruser !.<br>

En effet Il existe une possibilite, en realite plusieurs, mais toutes viennent du meme patern : les __callbacks__.<br>
Le But etant de ne pas retourner de valeur lorsque notre fonction se termine mais plutot de lui passer une fonction, une __callback__.<br>
Ainsi :
```javascript
function spoilGameOfThrones(callback){
    setTimeout(() => {
        const spoil = "JeNeSpoilPasMoi"
        callback(spoil)
    }, 2000)
}

//spoil ici pourrais porter n'importe quel nom, cela reste une fonction normale
spoilGameOfThrones(function (spoil) {
    console.log(spoil, "a tue la Reine !")
})
```

Ce qui est important ici c'est que l'on ne __return aucun resultat__ on utilise les fonctions pour respecter l'ordre d'execution.<br>
Ce qu'il faut bien voir egalement c'est que la definition de notre callback se fait lors de l'execution de la fonction `spoilGameOfThrones()`, et son execution se fait dans la definition de la fonction `spoilGameOfThrones`, il y a une sorte de va et vient que notre cerveau doit assimile, ce n'est pas facile.

Pour faire Plus simple et pour bien se representer le topo parlons de notre exemple : <br>
Ici lorsque l'on appelle notre fonction et qu'on lui assigne notre fonction en argument, il faut bien comprendre que __`function (spoil)` n'est pas appeler avant que `spoilGameOfThrones()` ne se soit fini__, et c'est la toute la magie.<br>

Et cela se verifie dans lorsque
```javascript
function spoilGameOfThrones(callback){
    setTimeout(() => {
        const spoil = "**** a tuer la reine"
        //n'est appele qu'a la fin
        callback(spoil)
    }, 2000)
}
```
Le principal probleme avec les callbacks c'est que lorsque l'on a beaucoup d'operations asynchrone a realiser, il devient tres __difficiles de relire le code__, car il faut sans cesse se referer a la fonction qui detient la callback dans sa definition, comme ici `spoilGameOfThrones()`.
On appelle cela le phenomene de __callbackHell__ .

---
## __Promises__ :
On le sait, __ES6__ a pris du temps a arriver, et il a apporter beaucoup de nouvelles 'fonctionnalites' avec lui __pour repondre a certaines difficultes__ pour nous developpeur. <br>
Avant nous avions les __callbacks patern__ pour repondre a nos besoins __asynchrones__, et c'est tres pratique il est vrai, cependant plus on aura d'operations asynchrones et plus la __lisibilite du code sera difficile__ (`callbacks hell`) car toutes nos callbacks seront __chaine__ et __nested__ les unes dans les autres et comme nous l'avons vu il faut une certaine gymnastique pour comprendre ce que fait vraiment notre code. <br>
ES6 a apporte une solution a cela, cela s'appelle __Promises__. <br>

Voyons a quoi elles ressemble :
```javascript
Const spoilGameOfThrones = new Promise()
```
Avant toute chose il faut bien comprendre qu'en general ce n'est pas nous qui creeont les promises, __ce sont les librairies que nous avons a disposition qui les creeer__.<br> Les librairies nous disent quand un methode retournerons une promise dans leur documentation, ce sera a nous de coder en fonction de cela. Cependant il est important de savoir comment elles sont creer pour pouvoir les utiliser. Revoyons le meme code donner dans l'exemple des callbacks mais cette fois avec une promise.
```javascript
const spoilGameOfThrones = new Promise(function (resolve, reject) {
     setTimeout(() => {
         const spoil = "**** a tuer Joeffrey"
         //tous s'est bien deroule
         resolve(spoil)
         //oops une erreur
         reject('une erreur lors du chargement de donne')
    }, 2000)
})
```
On peut y voir une grosse difference avec nos callbacks ici, en effet on voit que l'on a cette fois __une fonction differente pour chaque status__, on a une fonction `resolve` lorsque tous s'est bien passe et une fonction `reject` lorsque ce n'est pas le cas. __la lisibilite est deja meilleur__, mais ce n'est pas tout ...<br>

```javascript
const spoilGameOfThrones = new Promise(function (resolve, reject){
     setTimeout(() => {
         const spoil = "JeNeSpoilPasMoi"
         //tous s'est bien deroule
         resolve(spoil)
         //oops une erreur
         reject('une erreur lors du chargement de donne')
    }, 2000)
})
    //.then() ne s'execute que si tous s'est bien passe
    //spoil pourrait avoir n'importe quel nom ici
spoilGameOfThrones.then( function(spoil) {
    //on a pu avoir le spoil !
    console.log(spoil, "a tuer la reine")

}).catch( function(error) {
    //.catch ne s'execute que si il y a eut une erreur
    console.log(error)
})
```
Ainsi la principale difference ici est qu'un promise possede deux methode, __une qui execute une fonction lorsque tout s'est bien passe__ `.then()` et une autre __lorsqu'il y a eut une erreur__ `.catch`.<br>

Ainsi plus besoin de `if()` qui rendent le code difficile a lire, on __comprends beaucoup mieux les intentions du code__ et ca fait du bien ! <br>
De plus dans une callback __l'ordre des arguments__ determinent l'execution du code, il faut sans cesse se referer a son appel pour savoir si c'est une erreur ou pas.<br>
Ce n'est pas tout, une promise possede encore deux avantages, le premier et qu'il est plus difficile de faire des erreurs lors de son utilisation, je m'explique : lorsque l'on appelle `reject()` ou `resolve` __on stop totalement l'execution du code__ sans extra code. <br>
En comparaison avec une callback ou __l'on est oblige d'appeler un `return` pour stopper l'execution de notre code__. Impossible d'appeler deux `reject()` ou deux `reject()`. Sur notre code du dessus ce sera `resolve(spoil)` qui stoppera l'execution `reject('une erreur lors du chargement de donne')` ne sera jamais executer !<br>
Et la derniere est que `.catch()`, comme son nom l'indique, peut __attraper n'importe quel erreur__, comme un `throw error` ou un `trhow new Error()`, c'est pratique et rend le code encore plus lisible.

### D'autres Choses ?
Oui ! Il y a en effet un enorme avantage a utiliser des __Promises__, cela s'appelle le __Promises Chainning__.<br>
Lorsque l'on a une succession d'operation asynchrone on serait tenter de faire comme ici :

```javascript
findSpoilGameOfThrones().then((spoil) => {

    tellEveryoneSpoil(spoil).then((tellSpoil) => {
            console.log(tellSpoil)

    }).catch((e) => {
        console.log('personne n\'a voulu ecouter')

    })
}).catch((e) => {
    console.log('Pas pu trouver le spoil')
})

```
Du coup ici on n'arrive pas a trouver le reel avantage avec les callbacks, c'est pour cela qu'il existe le __Promises Chainning__, un patern tres utilise.<br>
Ainsi :
```javascript
findSpoilGameOfThrones().then((spoil) => {

    return tellEveryoneSpoil(spoil)

}).then((tellspoil) => {

}).catch((e) => {
    console.log('Pas pu trouver le spoil')
})

```
Ici tout se passe dans le `return`, c'est lui qui permet de creer ce chainage et d'eviter d'avoir des Promises __Nested__. Car on arrive a retourner une promise depuis notre callback `.then()`, et ainsi rajouter une callback `then()` par la suite, et ainsi de suite. <br>
Et de ce fait nous n'avons qu'un seul `catch()` a la fin de notre chainage.

---
## __Async / Await__ :
ES6 a apporte un bouleversement sur la facon dont les developpeurs Javascript ecrivent leur code Asynchrone : ce sont les fonction Async / Await <br>
les functions Await / Async sont calque sur le modele des Promises.<br>
Pour definir la fonction comme Async / Await il faut tout d'abord ecrire `async` lors de sa definition :

```javascript
const doWorkasync = async () => {}
console.log(doWorkasync())
//Promise { undefined }
```
Comme on peut le voir une `async` function retourne une promise !
De ce fait :
```javascript
const doWorkasync = async () => {
    return "hello i'm not just a string"
}

console.log(doWorkasync())
//Promise { 'hello i'm not just a string' }
```

Le return de la fonction retourne bien une promise avec la valeur qu'on lui a affectee.<br>
Du coup on peux faire comme dans une promise normale :

```javascript
const doWorkasync = async () => {
    return "hello i'm not just a string"
}

doWorkasync().then((result) => {
    console.log(result)

}).catch((error) => {
    console.log(error)

})
//Promise { 'hello i'm not just a string' }
```
En plus de cela il Faut savoir que lorsque l'on `throw new Error()` le catch va directement 'atrapper' cette erreur.<br>
Comme `async / await` fonctionne avec les promises patern, il ne change pas la facon dont on ecrit notre code, il change la facon dont on s'en sert seulement. Pour les librairies cela change tout car elles n'ont pas ete obliger de reecrire leur code pour accepter ce nouveau concept. <br>

### Qu'elle utilitee ?

#### __Await__ entre en jeu

La magie d'`Await` est qu'il permet d'ecrire un code qui parait synchrone alors qu'il ne l'est pas, je m'explique :

 ```javascript
const asyncData = await doWorkasync()
const asyncData2 = await doWorkasync(asyncData)
 ```
Pour Handle les individuelles erreurs on peut utiliser un `try catch`

```javascript
router.get('/users', async (req, res) => {
    try {
        const users = await User.find({})
        res.send(users)
    } catch (e) {
        res.status(500).send(e)
    }
})
```