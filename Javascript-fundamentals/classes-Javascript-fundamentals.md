# __Javascript's fundamentals__

---

## Les Classes ES6 :
Les classes fonctionnent comme les fonctions constructrice en Javascript, sauf qu'elle sont __beacoup plus directives__ et que les propritétés sont __plus ou moins figées__.
Ainsi pour construire une classe on fait :
```javascript
// on met en place la structure de notre classe(ici vide)
class Personne {

}

const person = new Personne()

```
Pour définir la __structure des instances__ qui découleront de notre classe il faut utiliser le __keyword__ :

```javascript
class Personne {
    constructor(name = 'anonymous') {
        this.name = name
    }
}

const CreatorC = new Personne('Bjarne Stroustrup')

console.log(CreatorC)
//Personne { name: "Bjarne Stroustrup" }
const creatorGit = new Personne('Linus Torvald')

console.log(creatorGit)
//Personne { name: "Linus Torvald" }
```

On peux assigner a la classe des méthodes qui seront propres a chaque instances créer a partir de cette classe :
```javascript
class Personne {
    constructor(name) {
        this.name = name
    }
    getGreeting() {
        console.log(`hello i'm  ${ this.name }`)
    }
}

const CreatorC = new Personne('Bjarne Stroustrup')

CreatorC.getGreeting()
//hello i'm  Bjarne Stroustrup
const creatorGit = new Personne('Linus Torvald')

creatorGit.getGreeting()
//hello i'm  Linus Torvald
```
## Sub-Classes :
Il est possible de créer des __Sub-Classes__, on en revient au __chainage de prototypes__ pour l'héritage dans les fonctions constructrices mais avec un peu moins de facilités selon moi. <br>
Pour cela il suffit d'utiliser le keyword `extends` :
```javascript
class Student extends Person {
}
const me = new Etudiant('Axel')
console.log(me)
//Etudiant { name: "Axel" }
```
Ce qu'il faut bien comprendre c'est que la classe Etudiant __hérite de toutes les propriétés__ de la classe Personne ici. Car Toutes les classes qui sont établies avec le mot `extends` hérite de toutes les propriétés contenu dans `constructor()` de la classe parente.

Ainsi :
```javascript
class Personne {
    constructor(name) {
        this.name = name
    }
    getGreeting() {
        console.log(`hello i'm  ${this.name}`)
    }
}

class Student extends Personne {
    constructor(name, diplome) {
        super(name)
        this.diplome = diplome
    }
}

console.log(new Student('Axel', 'Developpeur Web'))
//Student { name: "Axel", diplome: "Developpeur web" }
```
`super()` est un mot réservé qui permet de récuperer les propriétés propre de la classe parente, ici Personne. S'il n'est pas défini dans le `constructor()` javascript donnera une erreur demandant de la faire.<br>
Il est aisé __d'ecraser une methode venant de la classe parente__. Il suffit de creer une methode ayant exactement le meme nom que celle situe dans la classe parente. <br>
Mais il y a également possibilité de ne pas dénaturer la methode venant de la classe parente et d'y ajouter du code propre à la classe enfant. Ceci se fait grace au mot reservé `super()` qui permet de la recuperer dans la classe enfant, et de pouvoir y rajouter du code :
```javascript
class Personne {
    constructor(name) {
        this.name = name
    }
    getDescription() {
        return `i'm ${this.name} `
    }
}

class Student extends Personne {
    constructor(name, diplome) {
    //Permet d'appeler les propriétés de la classe parente
        super(name )
        this.diplome = diplome
    }
    possedeDiplome() {
        return !!this.diplome
    }
    getDescription() {
        let description = super.getDescription()

        if (this.possedeDiplome()) {
            description += `j'ai un diplome en tant que ${ this.diplome }`
        }
        return description
    }
}
const me = new Student('Axel')
console.log(me.getDescription())
//i'm Axel
const meLater = new Student('Axel', 'Developpeur Web')
console.log(meLater.getDescription())
//i'm Axel j'ai un diplome en tant que Developpeur Web
```
### Le Problème avec `this` :
Il se peut que le `this` binding soit cassé : lorsque l'on place dans une variable la méthode d'un objet ou l'instance d'une classe, on n'a plus accès au `this` car le contexte dans lequel est executer notre code est différent, plus exactement __le contexte n'est pas transferer dans la variable__:
```javascript
class Zombie {

    actionManger() {
        return `${ this.name } mange ${ this.proie }`
    }
    constructor (name, proie) {
        this.name = name
        this.proie = proie
    }
}

const zombieChien = new Zombie('chien','tout')
const actionManger = zombieChien.actionManger

console.log(actionManger())
//this is undefined
```
Pour remédier a ce probleme on utilisera `.bind()` qui permettra de binder le `this` sur une fonction, __chaque fonction__ possède cette méthode qui vient du prototype de `Function`. Donc :
```javascript
const zombieChien = new Zombie('chien','tout')
zombieChien.actionManger.bind(zombieChien)

console.log(actionManger())
//  chien mange tout
```
Pour éviter de devoir créer notre `constructor()` à chaque fois, et de devoir bind le `this`de nos méthodes, depuis ES6 on a la possibilité d'utiliser __les propriétés__ de classe ( **! attention **: ceci est une fonctionnaltié en **stage 2**, il faut utiliser babel pour pouvoir l'utiliser : [class-properties]('https://babeljs.io/docs/en/babel-plugin-proposal-class-properties').<br>
Ancienne maniere de faire :
```javascript
class AddOption extends React.Component {

    constructor(props) {
        super(props)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.state = { error : undefined}

    }

    handleAddOption(e) {
        e.preventDefault()

        const option = e.target.elements.option.value.trim()
        //retournera une erreur s'il y en a une
        const error = this.props.handleAddOption(option)

        this.setState( () => ({ error }))

        if (!error) {
            e.target.elements.option.value = ""
        }
    }
```
```javascript

```
