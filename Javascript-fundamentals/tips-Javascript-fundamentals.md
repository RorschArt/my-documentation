# Tips In Javascript :
## **Liens vers des Astuces Javascript** :
* [Lien 1 Medium](https://medium.com/@bretcameron/12-javascript-tricks-you-wont-find-in-most-tutorials-a9c9331f169d)
## **L'opérateur `!`**  :
L'opérateur peut etre très pratique :
* Convertir des valeur en `true` ou `false` : <br>

```javascriptQ
!''
//true
!!''
//false

!!'axel'
//true
!!undefined
//false
```
Grace à cette  technique on peut convertir des valeurs string en booleen
---
## Convertir en Int :
```javascript
~~'-1' = -1
~~true = 1
~~false = 0
~~5.6 = 5
```
## **DOM** :

Recuperer la valeur d'un input lors d'un click dans un `form` :
```javascript
<form onSubmit= { this.handleAddOption }>
    <input type="text" name="option" />
    <button>Ajouter Une Option</button>
</form>

event.target.elements.option.value
```

---
## Arrow fonctions : Retourner un Objet en ShortHand :
Comment faire pour que les `{}`ne soient pas considérés comme le corps de la fonction ? :
```javascript
const object () => ({key: value})
```
---
## **Conditions** :
### Shorthand pour retourner true ou false depuis une condition :
```javascript
const isAdult = (age) => age >= 18
// retourne true si age est superieur ou egal a 18 sinon false
```
---
## [Astuce pour enlever les valeur dupliquer dans un array](https://medium.com/dailyjs/how-to-remove-array-duplicates-in-es6-5daa8789641c)
```javascript
let text = [ 'A', 'B', 'C', 'D', 'E', 'D', 'B' ]
function duplicateCount(text) {
     lettersArray = text.toUpperCase().split('')
     comparasion = new Set(lettersArray)
     console.log([...comparasion])
     //[ 'A', 'B', 'C', 'D', 'E' ]
}
```
 [Astuce pour enlever les valeur dans un array](https://love2dev.com/blog/javascript-remove-from-array)
---
**[Use Of `.map()`](https://scotch.io/tutorials/4-uses-of-javascripts-arraymap-you-should-know)**
---
**[Shorthands](https://www.sitepoint.com/shorthand-javascript-techniques/)**
`for (let index of allImgs)` : Shorthand pour `for()`
---
 [`.slice()` Arrondi les index en paramètres](https://www.codewars.com/kata/reviews/567486aaed8cf6cf5600000c/groups/589b24a99f9136c56300041d)
---
## Utiliser les methodes d'un autre constructeur :
```javascript
Array.prototype.join.call(all, ' ');
```
---
## Utilisation de `.reduce()`   :
* [Liens  1](https://www.freecodecamp.org/news/a-guide-to-the-reduce-method-in-javascript-f47a7da511a9/)
---
## N'avoir qu'une condition dans un ternaire (ne retourner une valeur que si une condition est vrai) :
```javascript
count < n ? result.push(currentNumber) : false
```
## Avoir un tableau seulement avec des occurances répétées n x :
```javascript

function deleteNth(arr,x) {
  var cache = {};
  return arr.filter(function(n) {
    cache[n] = (cache[n]||0) + 1;
    return cache[n] <= x;
  });
}
console.log(deleteNth([20,37,20,21], 1))
```
---
[Convertir value En String](https://medium.com/dailyjs/5-ways-to-convert-a-value-to-string-in-javascript-6b334b2fc778)
---
## Pusher des valeur dans un array sans modifier le tableau original :
* Utiliser le **spread operator**.
* Utiliser `.concat()` :
```javascript
const names = ['julie', 'marie', 'jorgie']
const names2 = [... names, 'joe', 'pilou'] // names = ['julie', 'marie', 'jorgie']
```
## Spread Operator dans un objet (**plugin Babel**) :
```javascript
const person = {
    name: 'axel',
    age: 28
}
const person2 = {
    name: 'julie'
    ...person,
    age: 100
}
console.log(person2) //Object { name: "axel", age: 100 }
```
 ++ Permet d'override certaine propriétés.
---
## Avoir un Array de nombre jusqu'a 9 (pour comparaison par exemple) :
```javascript
let numbers = [...Array(10).keys()]; //[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
```
## Retourner les valeur répétés n fois dans le premier tableau et n fois dans le deuxieme **dem meme valeur** :
```javascript
let numbers = [...Array(10).keys()]; //[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
number1 = [1,5,6,6,6]
number2 = [1,6,6,4]
+numbers.some(number =>
number1.toString().repeat(3) &&
number2.toString().repeat(2))
```
---
## Ne retourner que les valeurs qui retourne True à une condition :
```javascript
array.filter((element, {a, b, c}) => {
    return a & b
})
```

Ne récuperer que les champs (keys) d'un Object :
```javascript
const updates = Object.keys(req.body)
```

## Supprimer une propriété d'un objet 💥

`deleteuser.password`
