# Atom's Shortcuts
---
## Commandes Terminal :
* Ouvrir Un fichier : `git [nomDuFichier]`
* Ouvrir Un fichier a Une Ligne Specifique : `git [nomDuFichier]:ligne:colonne`
* Ouvrir plusieurs Dossier En Meme Temps : `atom [nomDuFichier1] [nomDuFichier2]`
---
## Tree View :
* Aller dans Le Tree View : `ALT + \`
* Deplacer Fichier : `M`
* `Ctrl + Shift + C` : Copier Le chemin du Fichier

## Rechercher :
Rechercher des fichiers dans Les Fichiers Ouvert : `CTRL + B`
