<p>
📝 <b>Informations sur la rédaction du projet</b> 📝 ......................................................................<code>PAGE    </code><br>
</p>
<br>
---
<p>
🌍 <b>Overview</b> 🌍 ...................................................................................................................... <code>PAGE     </code>
<br>
<br>
1.⛰️ Environnement de travail ⛰️ ............................................................................................ <code>PAGE   </code><br>
2.👨‍⚕️ Le client 👨‍⚕️ ............................................................................................................ <code>PAGE   </code><br>
3.‍🎯 Objectif général 🎯 .......................................................................................................... <code>PAGE   </code><br>
4.‍‍🎯Objectifs techniques 🎯 .................................................................................................... <code>PAGE   </code><br>
5.‍‍💡Solutions  💡 ....................................................................................................................... <code>PAGE   </code><br>
6.‍‍🗼 Architecture 🗼 ................................................................................................................ <code>PAGE   </code><br>
</p>
<br>
---
<p>
 🔧 <b>Aspects Techniques</b> 🔧 ............................................................................................... <code>PAGE   </code>
 <br>
 <br>
1. 🛰️ Comment s'effectue les requêtes ? 🛰️............................................................................ <code>PAGE   </code><br>
2. 🧠 Philosophie 🧠 ............................................................................................................... <code>PAGE   </code><br>
3. 👥 Authentification 👥 ......................................................................................................... <code>PAGE   </code><br>
4. 👨‍🔬 Point de vue technique 👨‍🔬 ...................................................................................... <code>PAGE  </code><br>
5. 🤴 L'admin a t'il tous les droits ? 🤴 ..................................................................................... <code>PAGE </code><br>
</p>
<br>
---
<p>
👹 <b>Partie Back-end</b> 👹 ......................................................................................................... <code>PAGE   </code>
<br>
<br>
1. 🏗️ Structure de la base de donnée 🏗️ ............................................................................... <code>PAGE   </code><br>
2. 🗺️ Schémas 🗺️ ................................................................................................................. <code>PAGE   </code><br>
3. 🌎 Entité centrale 🌎 .......................................................................................................... <code>PAGE   </code><br>
4. 📚 Librairie mongoose 📚 ................................................................................................... <code>PAGE   </code><br>
5. 📦 Gestionnaire de package utilisé 📦 ............................................................................... <code>PAGE   </code><br>
6. 📧 Service de messagerie 📧 ............................................................................................ <code>PAGE   </code><br>
7. 🔑 Stockage des mots de passe 🔑 ................................................................................... <code>PAGE   </code><br>
8. 🔒 Protection des données 🔒 ............................................................................................. <code>PAGE   </code><br>
9. 🚀 Maximiser la performance 🚀 ......................................................................................... <code>PAGE   </code><br>
10.🖼️ Upload de fichiers : utilisation des NPM <code>multer</code>  et <code>sharp</code> 🖼️....................................... <code>PAGE   </code><br>
11.🛫 Déploiement en Production 🛫 ...................................................................................... <code>PAGE   </code><br>
</p>
<br>
<br>
<br>
<br>
---
<p>
🎨 <b>Partie Front-end </b> 🎨 .......................................................................................................... <code>PAGE   </code>
<br>
<br>
1. 📋 Résumé 📋 ..................................................................................................................... <code>PAGE   </code>
<br>
2. 🤔Alors pourquoi le faire avec React ? 🤔 .......................................................................... <code>PAGE   </code><br>
3. ⚙️ Configuration du projet front-end ⚙️ ............................................................................... <code>PAGE   </code><br>
4. 📦 Webpack et Babel 📦 ..................................................................................................... <code>PAGE   </code><br>
5. 🔬 Comment créer du contenu SPA ? 🔬 .............................................................................. <code>PAGE   </code><br>
6. 🗽 Comment créer des opérations asynchrones ? (<b>Traduction de l'Anglais</b>) 🗽 ................. <code>PAGE   </code><br>
7. 🎨 Styling 🎨 ...................................................................................................................... <code>PAGE   </code><br>
</p>
<br>
---
<p>
<b>🤔 Où en est l'application ? 🤔</b> ........................................................................................... <code>PAGE   </code>
<br>
<br>
1. 🤴 Le retour de mon client 🤴 ............................................................................................. <code>PAGE   </code><br>
2. 🏛️ Ce qui est déjà implémenté 🏛️ ....................................................................................... <code>PAGE   </code><br>
3. 👷 Ce que le repreneur devra implémenter 👷 ...................................................................... <code>PAGE   </code><br>
4. 🤝 Ce que le repreneur devra comprendre 🤝 ..................................................................... <code>PAGE   </code><br>
5. ✈️ Aller plus loin ✈️ ................................................................................................................ <code>PAGE   </code><br>
</p>
<br>
---
<p>
 <b>📚 Annexes et informations supplémentaires 📚</b> .............................................................. <code>PAGE   </code><br>
 <br>
 1.💾 Enregistrement des <code>states</code> dans local storage et récupération des données. 💾 ........... <code>PAGE   </code><br>
 2.🎨 Exemple d'un prototypage réalisé durant ma formation 🎨 .............................................. <code>PAGE   </code><br>
 3.🛠️ Liste des outils utilisés 🛠️ .............................................................................................. <code>PAGE   </code><br>
</p>
---
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<h1> 📝 Informations sur la rédaction du projet 📝</h1>
<br>
<p>
Il vous a peut-être paru déroutant de voir l'aspect de ce projet personnel et il est vrai qu'il n'est pas des plus attirant même pour moi qui ai pour habitude de création demandant beaucoup plus de visuel que celui-ci, néammoins j'aimerais attirer votre attention sur les raisons pour lesquelles j'ai choisi de l'écrire ainsi. <br>
Il s'agit en réalité d'un fichier <b>Markdown</b> (<code>Mardown Github</code>) qui a été convertis en format <b>PDF</b> pour l'impression. J'utilise régulièrement ce format pour plusieurs raisons : <br>
Il fait partie intégrante de la vie d'un <b>développeur web</b> qui parcourt les forums, les sites de versionning, les <b>documentations sur internet</b>, ainsi que beaucoup d'autres endroit où il s'avère trés utile et il est facilement convertible en des formats <b>PDF</b> et <b>HTML</b>, de ce fait il existe même des sites web écrit en langage hybride <b>HTML/Markdown</b>.<br>
Selon moi un développeur doit connaître ce 'langage' qui est trés simple à apprendre, en un jour on peut avoir la syntaxe basique en tête et surtout il permet <b>très rapidement</b> de créer un fichier agréable à lire, <b>structuré</b>.<br>
Alors certes il est difficile de créer une mise en page efficace pour de l'impression comme vous aller le voir en parcourant ce dossier mais je trouves que c'est quelque chose que je devais faire, l'outil se prettait entièrement à l'exercice qui m'est proposé ici.<br>
L'avantage est que toute les parties montrant du code sont parfaitement retranscrites, ce n'est pas une photos de mon éditeur qui donnerais je ne sais quel rendu "baveu" lors de l'impression. Bien plus plaisant à mon gout.<br>
En plus de cela il me permet d'insérer des émoticones <b>UNICODE</b> pour donner un peu de gaité à mon écriture, même si sur mon éditeur c'est beaucoup plus badass, mais bon. <br>
</p>
<h2>
Infos Supplémentaires : <br>
</h2>

Si  vous en voyez la necessité, vous pouvez allez voir les repos sur <b>Gitlab</b> à l'adresse suivante : <br>

<h3>URL des Repos</h3> 📡

<b>Back-End<b> : [https://gitlab.com/AxelDaguerre/medical-back-end]
<b>Front-End<b> : [https://gitlab.com/AxelDaguerre/medical-front-end]

<br>
<br>
<br>
<br>
<br>
---

<h2> Overview 🌍</h2>

<h3>Environnement de travail ⛰️</h3>

<p>
Mon projet découle de ce que j'ai produit durant mon stage, je vais donc récapituler dans qu'elles conditions j'étais : <br>
<br>
1.Dans la peau d'un <b>freelance</b>, un client 'm'embauche' pour réaliser son site web avec les <b>spécificités</b> établis avec lui lors d'un dialogue.
<br>
<br>

2.J'étais <b>seul</b>, personne n'étais là pour superviser mon travail.
<br>
<br>

3.<b>Parti de zéro</b>. Pas de serveur back-end, pas de base.
</p>

<br>

<h3> Le client 👨‍⚕️</h3>

<p>
Mon client est un professionnel de la santé et exerce le métier de <b>podologue</b>. <br>
Il appartient à un <b>groupe médical</b> regroupant plusieurs autres professionnels de la santé. <br>
</p>

<br>

<h3> Objectif général 🎯</h3>

<p>
L'objectif du site web est de créer un <b>espace commun</b> ou se regroupent <b>professionnels</b> de la santé du groupe médical et <b>patients</b>.<br>
Le but étant de faciliter les rendez-vous et les échanges avec les patients afin de leur apporter un soin plus efficace. Le but étant également de créer un environnement plus chaleureux et fidéliser les patients au groupe médical.
</p>

<br>

<h3> Objectifs techniques 🎯</h3>
<br>
<p>
1. <b>Faciliter l'appropriation future par un professionnel</b> : Utiliser des outils connus par les professionnels, essayer de maintenir un <b>wiki à jour</b> et utiliser des outils améliorant la lisibilité et la compréhension du code. <br>

2. <b>Protection des données</b> : Créer du code qui prend compte de la sécurité des données dans un niveau acceptable. <br>

3. <b>Un système performant.</b> Eviter de charger des librairies inutiles et coûteuse. le but étant aussi d'avoir un système le plus neutre possible<br>

4. <b>Un site web évolutif</b> : Créer une structure capable d'évoluer, enlever des fonctionnalités, en rajouter sans casser le code.
</p>
<br>
<h3> Solutions  💡</h3>
<br>
<p>
1.  <b>React ⚛️</b> : Un framework répondant à  => {<br>
    &emsp; &emsp; Facilité l'appropriation future par un professionnel<br>
    &emsp; &emsp; Protection des données<br>
    &emsp; &emsp; Un système performant<br>
}
<br>
<br>

2. <b>MongoDB</b> 📚 : Une base de donnée répondant à => {<br>
   &emsp; &emsp; Un site web évolutif<br>
   &emsp; &emsp; Protection des donnée <br>
}
<br>
<br>

3. <b>Express</b> 🚆 : Un framework Back-end répondant à => {<br>
  &emsp; &emsp; Facilité l'appropriation future par un professionnel <br>
}
<br>
<br>

4. <b>Mongoose</b> 📖 : Une librairie ORM pour base de donnée répondant à => {<br>
  &emsp; &emsp; Facilité l'appropriation future par un professionnel <br>
}
</p>
<br>

<h3> Architecture 🗼</h3>

<h4> Structure des fichiers back-end</h4>

<p>
Pour comprendre plus aisément comment est structuré mon application dans le <b>back-end</b> je fournis ici une image correspondant à la structure complète de mes fichiers.
<br>
</p>
<kbd>
<p align="center">
<a href="url"><img src="images/Structure1.png" height="450" width="600p" ></a>
</p>
</kbd>

<p>
Voici le fichier <code>package.json</code> pour voir l'ensemble des modules utilisés, comme on peut le voir la liste est quand même très courtes.
</p>

```json
{
  "name": "medical-back-end",
  "version": "1.0.0",
  "main": "index.js",
  "repository": "git@gitlab.com:AxelDaguerre/medical-back-end.git",
  "author": "Daguerre <litooo@hotmail.fr>",
  "license": "MIT",
  "dependencies": {
    "@sendgrid/mail": "^6.4.0",
    "bcrypt": "^3.0.6",
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "jsonwebtoken": "^8.5.1",
    "mongodb": "^3.2.7",
    "mongoose": "^5.6.3",
    "mongoose-autopopulate": "^0.9.1",
    "multer": "^1.4.1",
    "sharp": "^0.22.1",
    "validator": "^11.0.0"
  },
  "scripts": {
    "start": "node src/index.js",
    "dev": "env-cmd -f config/dev.env nodemon src/index.js"
  },
  "devDependencies": {
    "env-cmd": "^9.0.3",
    "nodemon": "^1.19.1"
  }
}
```
Une attention particulière a été apportée pour créer une structure claire et lisible.

---
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<h2> Aspects Techniques 🔧</h2>
<br>
<h3> Comment s'effectue les requêtes ? 🛰️</h3>
<p>
Les demandes vers la base de donnée se feront à l'aide d'une architechture <b>REST</b>. <br>
Ce type d'architecture est très utilisée dans le web 'moderne' car elle offre de nombreux avantages que j'expliquerais lors de mon passage à l'oral. <br>
Notamment une grande souplesse, très bonnes performance, plus besoin de stocker en back-end la session, tout se passe au niveau du client.<br>
</p>

<br>
<h3> Philosophie 🧠</h3>
<p>
La philosophie du <b>CRUD</b> était qu'il fallait faciliter le travail côtés front-end, au maximum les opérations devaient se faire côtés serveur.<br>
Cela se traduit par des <code>URL</code> détaillés avec beaucoup de possibilités différentes.<br>
Par exemple une <code>URL</code> pour <code>fetch</code> les utilisateurs <b>non-identifiés</b>, une autre pour les utilisateurs <b>identifiés</b> mais <b>non-admin</b> et une dernière pour les utilisateurs <b>admin</b>. <br>
Ainsi chaque routes renvoient des données comportant plus ou moins d'informations en fonction des utilisateurs qui le demandent.<br>
Il y a également possibilité de trier nos données avec les paramètres spécifiés dans l'URL (<code>limit</code>, <code>skip</code>,<code>sort</code>).<br>
Tout le codage à été réalisé dans cette perspective, pour des soucis de performance bien sûr mais également pour une facilité côtés front-end qui était la partie la plus difficile pour moi (apprendre <b>React</b> en <b>2</b> mois).
</p>
<br>

<h3> Authentification 👥</h3>
<p>
Le site web possède la fonctionnalité de pouvoir <b>créer des comptes personnels</b> par l'utilisateur, ainsi l'utilisateur aura accès aux fonctionnalités plus avancés du site web. Cela permet de protéger les données sensibles des utilisateurs. <br>
<br>
</p>

<h4> Point de vue technique 👨‍🔬</h4>
<p>
Certaines opérations du <b>CRUD</b> demande un <b>JSON Web Token</b> qui doivent être envoyées par le client. Ce JSON Web Token sera créer lors de la <b>création</b> ou de la <b>connexion</b> de l'utilisateur et pusher dans un <b>array</b> appelé <code>tokens</code> appartenant à l'utilisateur en question. <br>
Le <b>JWT</b> sera renvoyé au client lors de ces opérations. Ainsi il sera permis à un utilisateur de se connecter depuis différents appareils en le comparant depuis notre back-end à ceux stockés dans notre base de donnée. <br>

Le processus d'identification sera expliqué dans les grandes lignes lors du passage à l'oral en essayant de <b>synthétiser</b> du mieux possible le déroulement de l'opération accompagné du code.
</p>

<h4> L'admin a t'il tous les droits ? 🤴</h4>

<p>
La possibilité de pouvoir tout 'fetcher' par l'utilisateur <b>Admin</b> a été réfléchi : il est clair que l'admin <b>ne devrait pas</b> pouvoir voir la totalité des données d'un utilisateur. Il doit pouvoir accéder : <br>
<ul>
<li> Au <b>nombre d'utilisateurs</b> qui sont connecté à un moment T. </li><br>

<li>Au <b>nombre d'utilisateurs</b> se sont connectés dans la journée. </li><br>

<li>A l'<b>adresse e-mail</b> de n'importe quel utilisateurs. </li><br>
</ul>
<p>
Mais il ne devrait pas pouvoir faire des opérations qui manipulent les données utilisateurs.<br>
Il doit pouvoir exécuter <b>n'importe qu'elles actions</b> sur la base de donnée <b>qui ne provient pas</b> d'un utilisateur avec l'exception de pouvoir <b>supprimer un utilisateur</b> et de <b>modifier son status ('admin' -> 'guest')</b>. <br>
</p>
</p>
</p>
<p>
Voici quelques middlewares pour l'<b>authentification</b> de l'utilisateur et la <b>protection des données</b> :<br>
<p/>

```Javascript
const jwt  = require('jsonwebtoken')
const User = require('../models/user')

// Authentification User
const auth = async (req, res, next) => {

    try {
        const json = req.header('Authorization').replace('Bearer ', '')
        //Contient l'_id
        const jsonDecoded = jwt.verify(json, process.env.JWT_SECRET)
        const user = await User.findOne({ _id: jsonDecoded._id, 'tokens.token': json })

        !user ? res.status(401).send() : false

        // On stocke nos valeur dans la req pour l'utiliser dans le route (performance)
        req.token = json
        req.user = user

        user.role.roleName === 'admin' ? req.user.isAdmin = true : req.user.isAdmin = false

        next()
    } catch (e) {
        res.status(401).send({ erreur: 'SVP authentifiez-vous !' })
    }
}

module.exports = auth

```

---

<br>
<br>
<br>
<br>

<h2> Partie Back-end 👹</h2>
<h3> Structure de la base de donnée 🏗️</h3>

<p>
Voici les entités dont j'avais besoin pour créer mon application web :<br>

1. <b>User</b> une entité pour stocker toutes les infos de l'utilisateur.<br>
2. <b>Role</b> une entité pour stocker les droits d'un utilisateur.<br>
3. <b>Mail</b> Trois entité pour stocker les e-mails ( email <b>envoyés</b>, <b>reçus</b> et <b>enregistrés</b>).<br>
4. <b>Article</b> Une entité pour stocker les articles créer par un utilisateur <code>Admin</code>.<br>
6. <b>Comment</b> Une entité pour stocker les commentaires d'un article.<br>
5. <b>Activity</b> Une entité pour stocker les jours (timestamp) qui ne sont pas disponible pour rendez vous.<br>
6. <b>Avatar</b> Une entité de type <b>Buffer</b> stockant l'image de profil d'un <b>User</b>.<br>
7. <b>ImageArticle</b> Une entité de type <b>Buffer</b> stockant l'image d'un <b>Article</b>.<br>
</p>
<br>
<h3> Schémas 🗺️</h3>

*Légende: S (String), T (Timestamp), B (Buffer), Entité (Entité liée)*
<p>
1. <b>User</b> 😄 { <em>age</em>: S, <em>email</em>: S, <em>tokens</em>: [S], <em>password</em>: S, pseudoName: S, <em>createdAt</em>: T, <em>deletedAt</em>: T, <em>avatarId</em>: B, <em>role</em>: <em>lien virtuel</em> (Role) }<br>

3. <b>Article</b> 📄 { <em>title</em>: S, <em>subtitle</em>: S, <em>textBody</em>: S, <em>image</em>: B, <em>author</em>: Entité(User), <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

4. <b>Mail-sended</b> 💌 { <em>textBody</em>: S, <em>header</em>: S, <em>from</em>: Entité(Role), <em>to</em>: Entité(Role), <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

5. <b>Mail-received</b> 💌 { <em>textBody</em>: S, <em>header</em>: S, <em>from</em>: Entité(Role), <em>to</em>: Entité(Role), <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

6. <b>Mail-saved</b> 💌 { <em>textBody</em>: S, <em>header</em>: S, <em>from</em>: Entité(Role), <em>to</em>: Entité(Role), <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

7. <b>Role</b> 💌 { <em>roleName</em>: S, <em>User</em>: Entité(User)  <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

8. <b>Avatar</b> 🖼️ { <em>image</em>: B <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>

9. <b>ImageArticle</b> 🖼️ { <em>image</em>: B <em>createdAt</em>: T, <em>deletedAt</em>: T }<br>
</p>
<br>
<h3> Entité centrale 🌎</h3>

<p>
Contrairement à ce que l'on pourrait croire <b>User</b> n'est pas l'entité centrale de la base de donnée, c'est peut être un défaut de conception de ma part mais <b>cela m'a facilité la tâche d'utiliser l'entité <b>Role</b> pour représenter un utilisateur</b>.<br> Cette entité possède toutes les informations de l'utilisateur en plus du type de compte.
En effet grâce à <b>MongoDB</b> et plus précisemment <b>Mongoose</b> on a la possibilité de créer des liens <b>virtuels</b> entre nos entités, ce qui diffère des languages <b>SQL</b>.<br>
J'ai choisi d'utiliser l'entité <b>Role</b> en tant qu'entité parente car elle me permet d'avoir plus d'informations sur l'utilisateur, maintenant elle n'ajoute que le type de role mais plus tard elle possèdera surement plus que ça, des propriétés comme <code>canWriteArticle</code> ou encore <code>canModifytext</code> en fonction de comment évolue l'application à l'avenir. <br>
On peut penser qu'il y aura une personne qui ne sera pas <code>admin</code> mais aura la possibilité de créer un article.
</p>
<br>
<h3> Librairie mongoose 📚</h3>

<p>
<b>Mongoose</b> est une librairie pour <b>MongoDB</b> permettant de manipuler nos données de manière facilité et avec une écriture de code plus facile à lire et maintenir. C'est en fait un Object Relational mapping (<code>ORM</code>).
</p>
<br>
<h2> Gestionnaire de package utilisé 📦</h2>

<p>
<b>Yarn</b> est le gestionnaire de package utilisé dans notre <b>backend</b> et <b>front-end</b>, il est très similaire à <b>NPM</b> mais offre un <b>systeme de cache</b>, ainsi un package déjà installé auparavant pourra être réinstaller <b>très rapidement</b> grâce au cache stocké dans la mémoire temporaire de notre ordinateur, quelques fonctionnalités supplémentaires et une sécurité accrue.
</p>
<!--
---

<h2> Liste Des Modules et Autres Outils Utilisés 🛠️</h2>

1.  Express
2.  React-calendar
3.  Mongoose
4.  MongoDB
5.  Bcrypt
6.  JsonWebToken
7.  Multer
8.  Validator
9.  Sharp
10. @sendgrid/mail
11. Env-cmd
12. azurVSCode -->


<br>
<h2> Service de messagerie 📧</h2>

<p>
Le service de messagerie utilisé est <b>Sendgrid</b>, il en existe beaucoup mais c'est celui-ci que j'ai choisi car il permet gratuitement et <b>à vie</b> d'utiliser le service pour envoyer jusqu'à <b>100 emails/jour</b>, ce qui est largement suffisant pour mon client. <br>
Ainsi <b>Sendgrid</b> est utilisé seulement lors de la <b>création</b> d'un utilisateur et de sa </b>suppression</b>, le système de messagerie est directement coder dans le backend et stocké dans notre base de donnée.<br>
Peut être que dans l'avenir un utilisateur pourrait <b>recevoir un e-mail</b> lorsqu'il reçoit un message dans son espace personnel, une fonctionnalité facile à implémenter.
</p>
<br>
<h2> Stockage des mots de passe 🔑</h2>

<p>
On ne peut pas stocker les mots de passe des utilisateurs <b>en clair</b>, cela à été fait autrefois mais maintenant tous le monde <b>se doit</b> de les stocker sous une forme 'hashé'. <br>
Pour cela on utilise un algorithme très utilisé dans le web sur nos : <b>le hashage</b>.<br>
Pour utiliser cet algorithme j'ai utilisé un <b>NPM</b> très connus : <code>bcrypt</code>.<br>
Ainsi les mots de passe sont 'hasher' avec un algorithme qui ne permet pas de revenir à l'état initial du mot de passe. <br>
Contrairement à l'<b>encryption</b>, qui lui, permet de retrouver l'<b>état initial</b>. <br>
Ainsi même si une personne malintentionné a accés aux identifiants de notre base de donnée, ce qui constitue le pire des scénarios,  il ne lui sera, en théorie, <b>pas possible de les déchiffrer</b>.
</p>
<br>
<h3> Alors comment vérifier les identifiants d'un utilisateur ? 🤔</h3>

<p>
Cependant l'algorithme permet de déterminer s'il s'agit du bon mot de passe. Pour cela on <code>hash</code> le mot de passe que l'on nous donne et on le compare à celui de la base de donnée. <br>
Pour déterminer cela, l'algorithme est très complexe mais une chose est sûr, si le mot de passe est <b>exactement</b> le même que celui stocké en base de donnée, il le saura à coup sûr.<br>
<br>
<br>
Voici comment est utilisé ce NPM :
</p>
</b>
```javascript
// Hashage du password 8 tour(préconisé par le créateur).
user.password = await bcrypt.hash(user.password, 8)
```

```javascript
// Récupère l'utilisateur avec l'adresse email donné par le client.
const user = await User.findOne({ email })
// Comparaison mot de passe entre DB et celui donné par l'utilisateur.
// return true si le mot de passe hashé correspond
const isMatch = await bcrypt.compare(password, user.password)
```
<p>
<b>8</b> est le nombre de 'tour' qu'effectue l'algorithme, <b>8</b> est le nombre <b>recommandé par son créateur</b> car il constitue le nombre idéal entre <b>performance</b> et <b>sécurité</b>.
</p>
<br>
<h2> Protection des données 🔒</h2>

<p>
Pour protéger les données des utilisateurs en plus du <b>hashage</b> du mot de passe et de <b>JWT</b> une attention particulière a été réalisé lors du renvoi des données d'un utilisateur vers le client sous la forme de <b>JSON</b>.<br>
Ainsi dans le code existe un <b>middleware</b> qui s'exécute lors d'un <code>JSON.stringify()</code> qui est utilisé pour enlever le mot de passe et les tokens de l'utilisateur, ainsi même si quelqu'un parvient à pirater un compte ou à avoir accès au <b>JSON</b> reçus il ne pourra avoir accès à aucune donnée sensible de l'utilisateur.</b><br>
Egalement lors des opérations <b>CRUD</b> sensibles, une vérification du type de compte de l'utilisateur sera effectué en plus
de son authentification.</b>
</p>
<br>
<h2> Maximiser la performance 🚀</h2>

Dès que cela est possible on évite les opérations supplémentaires inutiles, un exemple très significatif est l'utilisation du middleware d'authentification : ce middleware va chercher dans la base de donnée un utilisateur à chaque opérations <b>CRUD</b> si l'utilisateur est déjà connecté.
Ainsi aller chercher l'utilisateur connecté lors d'une opération comme <code>get my  profile</code> n'est pas utile.Pour éviter de devoir refaire l'opération, lors de l'execution du middleware on stocke les données de l'utilisateur dans la requetes d'express <code>req</code>, ainsi on y a accés à tous moment.


<h3> Upload de fichiers : utilisation des NPM <code>multer</code>  et <code>sharp</code> 🖼️</h3>
<br>
<h3> <code>Multer</code> </h3>

<p>
De base, <b>Express</b> n'accepte pas le transfert de fichiers (upload), mais il existe un <b>NPM</b> maintenu par la même équipe qui maintient <b>express</b> : <code>multer</code>. <br>
C'est le <b>NPM</b> le plus efficace et utilisé pour l'upload de fichiers vers nun back-end <b>NodeJS</b>. <br>
La plupart du temps nous transférons un <b>JSON</b> vers le serveur qui le traite pour le stocker, mais pour <code>uploader</code> une image ou une vidéos nous ne le stockons pas en <b>JSON</b> mais sous la forme d'un <code>buffer</code> (binaire). <br>
Ainsi <code>multer</code> utilise <code>multipart/form-data</code> pour transférer un fichier en binaire vers le serveur (découpé en plusieurs parts). <br>
En fonction du type de fichier que l'on souhaite <b>uploader</b> on créer une instance de multer en la configurant selon nos besoins. Cette instance contient les règles d'upload que l'on souhaite, la taille maximale du fichier, le type etc...<br>
<code>multer</code> s'utilise en le passant en <b>midleware</b> dans une <code>URL</code> router. Il existe deux manières de 'servir' un fichier.
</p>

<br>

<h4>Deux solutions </h4>
<p>
1. La première consiste à renvoyer la propriété de l'utilisateur contenant le binaire sous la forme d'un <b>JSON</b> et du côtés client utiliser une balise <code>img src="data:image/jpg;base64,fichierBinaire"</code>
</p>

<p>
2. La deuxième consiste à servir un <b>URL</b> depuis notre back-end dans lequel on envoi le fichier binaire de l'image contenu dans notre base de donnée. <br>L'avantage est qu'il suffira d'utiliser l'adresse <b>URL</b> dans un tag image pour que le navigateur l'interprete et on gagne un petit peu en performance car certaines opérations se déroule cotés back-end et non cotés front-end.
</p>

<p>
 C'est la deuxième que j'ai choisis car elle me parait beaucoup plus pratique et me facilite le travail côté fron-end, une sorte de factorisation du code. <br>
 Lors de la réponse du serveur back-end il faut spécifier le <code>header</code> de la réponse au navigateur dans lequel elle sera interprétée.<br>
 Donc la réponse doit avoir pour header : <code>Content-Type</code> et en valeur <code>image/jpg</code>(car toutes nos images son convertis en <code>jpg</code> à l'aide de <code>sharp</code>). <br>
 Si l'on ne spécifie pas de<code>Content-Type</code> <b>Express</b> le détermine comme étant <code>application/json</code> ce qui ne nous arrange pas dans notre cas. <br>
 Une fois ceci spécifié il nous suffit d'envoyer les données binaires au client pour que le navigateur l'interprete et face un rendu de l'image. <br>
 Voici le type d'<b>URL</b> à spécifier coté client, bien entendu aucune authentification n'est demandée lors de la demande de lecture de l'image, mais est nécessaire lors de l'upload (voir <code>auth</code>):
 <br>
</p>
 <br>

```http
URL/users/5d2c1feae4f6ee188017dee4/avatar
```

```javascript
const avatar = multer({
    limits: {
        fileSize: 500000
    },
    fileFilter(req, file, cb){
        if (!file.originalname.match(/\.(jpg|png|jpeg)$/)) {
            return cb(new Error('Veuillez Uploader un fichier image SVP !'))
        }
        cb(undefined, true)
    }
})
// single veut dire un seul fichier dans la requete.

// UPLOAD Par l'utilisateur
router.post('/users/me/avatar', auth, avatar.single('avatar'), async (req, res) => {})
//Lecture par n'iimporte quel utilisateur (connecté/non-connecté)
router.get('/users/:idUser/avatar', async (req, res) => {})

```

<p>
A noter que cette solution permet d'isoler le fichier binaire des entités <b>User</b>/<b>Role</b> puisque que l'on a seulement l'<code>_id</code> de l'image stocké dans l'entité qui nous permettra ensuite d'aller chercher l'image en question avec l'<code>_id</code> même s'il existerait la possibilité d'extraire le binaire avant le renvoi des données de l'utilisateur, à la manière que l'on a de ne pas renvoyer le mot de passe et les <code>tokens</code> vers le client.
</p>

<h3> <code>sharp</code></h3>

<p>
<code>sharp</code> nous permet de transformer un fichier dans le format de notre choix en plus d'autres capacités de <b>compression</b>, <b>redimmensionnement</b> etc ...
Cela nous permet de réduire fortement la taille des données stockées comme les <b>avatar</b> des utilisateurs. <code>Sharp</code> nous donne la possibilité <b>de manière très simple</b> de pouvoir manipuler nos fichiers <b>image</b> juste avec quelques méthodes fournis par une instance.
</p>

```javascript
// on transforme le fichier entrant en png et dans une taille de 250x250, en une ligne !
const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
```
<p>
C'est un gain de temps immense pour une tâche aussi difficile qui demande des connaissances <b>Hardware</b>, on voit ici l'utilité des packages dans le developpement web !
</p>
<br>
<br>
<br>
<br>
<br>
<br>
<h2> Déploiement en Production 🛫</h2>

Pour déployer notre <b>API</b> il nous faut un hébergeur, donc une machine qui stocke notre code avec :

1.NodeJS.
2.Les NPM utilisé.
3.Des variables d'environnement stockant les données sensibles ou autres.
4.Express.
<br>

<h3> Qu'est ce qu'une variable d'environnement ? 🤔</h3>

<p>
Les variables d'environnement sont utilisées pour <b>cacher</b> des données sensibles comme des <b>clefs d'API</b> extérieures utilisées dans notre application, <b>la phrase de déchiffrage des JWT</b> ou encore les <b>identifiants de la connection de notre base de donnée</b>. <br>
En effet si on n'utilisais pas de variables d'environnements pour cela, lorsque l'on pusheais notre code avec <b>GIT</b>, toutes nos données sensibles seront visibles en clair à toutes les personnes ayant accès à celui-ci. <br>
Les variables d'environnements servent également à la machine stockant le code (server), car les hébergeurs utilisent leurs propres nom de variables comme <code>port</code> stocké dans <code>process.env.port</code>. <code>process.env.port</code> est en réalité fourni par <b>C++</b> avec lequel est écrit <b>nodeJS</b> qui peut accéder aux fichiers système de la machine. <br>
Pour résumer : l'avantage est la <b>sécurité</b> et la <b>compatibilité</b> pour les machines stockant notre code.
</p>

<br>
<h3> Quels Services utilisés ? 🤔</h3>

<h4> Serveur back-end</h4>

<p>
J'ai utilisé <code>Heroku</code> qui est une plateforme <b>cloud</b> et permet de mettre en place un serveur très <b>rapidement</b>, sans avoir à installer les logiciels necessaires à la mise en production de notre serveur. <br>
Il fonctionne à l'aide de <b>'packages'</b>, on choisit notre environnement (<code>nodeJS</code>, <code>java</code> ...) et le serveur est disponible de suite. <br>
<b>Heroku</b> facilite même encore plus cela car il fourni un <b>CLI</b> (Command Line Interface) permettant de <code>pusher</code> notre code via <b>GIT</b> et même de configurer les variables d'environnements du serveur. <br>
En plus de cela lorsque l' on <code>push</code> notre code pour la première fois, <b>Heroku</b> installe automatiquement les logiciels nécessaires sans avoir à choisir quoi que ce soit grâce au <code>package.json</code> que l'on fourni lorsque l'on <code>push</code> notre code. C'est pour ces raisons que je l'ai choisi (rapidité).<br>
</p>

<h4> Serveur front-end</h4>

<p>
Idem, <code>Heroku</code>
</p>

<h4> Cluster MongoDB</h4>

<p>
Pour mettre en place ma base de donnée j'ai utilisé <b>MongoDB Atlas</b> qui donne une utilisation gratuite pour une application web de notre taille et donne des outils performant.
</p>

---

<h2> Partie Front-end 🎨</h2>

<h3> Résumé 📋</h3>

<p>
La partie front-end est la partie la plus dense de notre application, il y a beaucoup de <b>dépendances</b>, de <b>lignes de code</b>, et c'est la partie la plus <b>complexe</b>. <br>
Elle a pris plus d'un mois à être réalisée, il y a des parties qui peuvent être <b>améliorées</b>, <b>factorisées</b> et mieux <b>structurées</b>, cela devrait prendre quelques jours. <br>
Comme déjà décrit auparavant, la partie front-end est une <b>SPA</b>, tous les états utiles de la <b>web app</b> sont stockés lorsque l'utilisateur effectue des actions spécifiques. <br>
Ainsi un utilisateur qui a commencé à créer un message doit pouvoir récupérer son message plus tard s'il a quitté son édition. Je ne parle pas ici des messages 'sauvegardés' en base de donné mais bien de l'application front-end qui enregistre le message dans le <code>local storage</code> et dans le <code>store</code> <b>Redux</b> qui contient tous les états de l'application. Le but étant bien sur de donner la meilleure expérience possible. <br>
Au même titre le changement de page ne doit se faire ressentir que lorsque l'utilisateur se connecte ou se déconnecte, le reste du temps, l'utilisateur ne verra que des loaders sur les composants qui composent l'application, ainsi il ne se retrouvera <b>JAMAIS</b> devant une page blanche (sauf bug), et il pourra même réaliser une action sur un composant déjà chargé même si les autres ne le sont pas. <br>
Il verra en <b>temps réel</b> lorsqu'il recevra un message à l'aide d'un compteur de message ainsi que d'autre informations qui donne une très bonne <b>fluidité</b> et lui permet de profiter d'une des <b>meilleures expérience</b> du web à l'heure actuelle. <br>
L'application est également un dérivé de <b>CMS</b>, il aura une partie <code>administration</code>, qui donnera la possibilité d'éditer des articles, de supprimer un utilisateur, voir la fréquentation du site (peut être pas necessaire, car les serveurs de prod l'indiquent déjà). <br>
Au départ un <b>framework</b> comme <b>React</b> n'est pas le plus recommandé pour faire un <b>CMS</b> car il oblige de créer la logique <b>CMS</b> à la main, même si des outils comme <code>react-admin</code> existent. <br>
<br>
<br>
<br>
<br>
<br>
<h3>Alors pourquoi le faire avec React ? 🤔 </h3><br>
<p>
A cause des besoins dont le site souhaite répondre : <br>
Il doit posséder des fonctionnalités impossibles à satisfaire avec un <b>CMS</b>. <br>
En effet si le projet avait été codé avec un <b>CMS</b> comme <b>Wordpress</b>, il n'aurais pas pu répondre à certaines spécificités : une expérience <b>optimale</b> (<code>SPA</code>), une <b>évolutivité</b> (je parle ici du fait qu'il va permettre à mon client de ne pas avoir de restriction future, comme par exemple de créer un chat en temps réel). (<code>Web socket</code>)). <br>
Donc oui la partie <code>Admin</code> prend plus de temps à être réalisé, mais elle n'est pas impossible, contrairement au fait que le client était très attiré par une <b>SPA</b> qui aurait été impossible à réalisé.<br>
Comme déjà décrit auparavant, la partie back-end est déjà bien <b>sécurisé</b>, mais la partie front-end <b>le sera également</b>, car cela ne sera que mieux. <br>
La sécurité se fera lorsque l'utilisateur recevra les données du back-end, lorsqu'il enverra des données également. Par exemple : les <b>adresses emails</b> des utilisateurs ne seront <b>pas vu</b> par le client, ne seront <b>jamais affichés</b>, même pour un utilisateur connecté, le choix du destinataire d'un message se fera à l'aide des <code>pseudoname</code> qui seront bien entendu uniques (configurés en base de donné). <br>
Il aurait été idéal de ne <b>jamais renvoyer</b> vers le client les <b>adresses email</b> et de n'utiliser que les <code>id</code> pour effectuer les opérations, malheureusement ce n'est pas le cas, mais toujours possible car les opérations se servent des adresses emails <b>seulement dans le back-end.
</p>
<br>
<h3> Configuration du projet front-end ⚙️</h3>

<h4> <code>Package.json</code></h4>

Voici la liste des package utilisés, bien plus étendue que dans le <b>back-end</b>.

```json{
  "name": "medical-front-end",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "heroku-postbuild": "yarn run build:prod",
    "build:dev": "webpack --env development",
    "build:prod": "webpack --env production",
    "dev-server": "webpack-dev-server",
    "start": "node server/server.js",
    "test": "jest"
  },
  "repository": "git@gitlab.com:AxelDaguerre/medical-front-end.git",
  "author": "daguerre axel <axeldaguerredev@gmail.com>",
  "license": "MIT",
  "dependencies": {
    "@babel/core": "^7.2.2",
    "@babel/plugin-proposal-class-properties": "^7.5.5",
    "@babel/plugin-proposal-object-rest-spread": "^7.5.5",
    "@babel/preset-env": "^7.5.5",
    "@babel/preset-react": "^7.0.0",
    "axios": "^0.19.0",
    "babel-cli": "^6.26.0",
    "babel-eslint": "^10.0.2",
    "babel-loader": "^8.0.6",
    "css-loader": "^3.1.0",
    "express": "^4.17.1",
    "formik": "^1.5.8",
    "history": "^4.9.0",
    "lodash": "4.17.11",
    "mini-css-extract-plugin": "^0.8.0",
    "moment": "^2.24.0",
    "node-sass": "^4.12.0",
    "normalize.css": "^8.0.1",
    "polyfill": "^0.1.0",
    "react": "^16.8.6",
    "react-addons-shallow-compare": "^15.6.2",
    "react-dates": "^20.2.5",
    "react-dom": "^16.8.6",
    "react-dropzone": "^10.1.7",
    "react-loader": "^2.4.5",
    "react-modal": "^3.9.1",
    "react-redux": "^7.1.0",
    "react-router-dom": "5.0.1",
    "react-toastify": "^5.3.2",
    "redux": "^4.0.4",
    "redux-thunk": "^2.3.0",
    "sass-loader": "^7.1.0",
    "uuid": "^3.3.2",
    "webpack": "^4.39.1",
    "yup": "^0.27.0"
  },
  "devDependencies": {
    "dotenv": "^8.0.0",
    "eslint": "^6.1.0",
    "eslint-config-standard": "^13.0.1",
    "eslint-config-standard-react": "^8.0.0",
    "eslint-plugin-import": "^2.18.2",
    "eslint-plugin-node": "^9.1.0",
    "eslint-plugin-promise": "^4.2.1",
    "eslint-plugin-react": "^7.14.3",
    "eslint-plugin-react-redux": "^3.0.1",
    "eslint-plugin-standard": "^4.0.0",
    "jest": "^24.9.0",
    "prop-types": "^15.7.2",
    "scss-lint": "^0.0.0",
    "webpack-cli": "^3.2.1",
    "webpack-dev-server": "^3.7.2"
  }
}
```
<br>
<h4> Webpack et Babel 📦</h4>
<p>
Pour organiser nos fichiers et pour pouvoir utiliser les nouvelles fonctionnalités et possibilités de <b>Javascript (ES9)</b>, nous utilisons un <b>Bundle</b> qui s'appelle <code>Webpack</code>, ce bundle est <b>ultra-customizable</b> et permet de pouvoir "assembler" nos fichiers en  2 fichiers appelés <code>Bundle.js</code> et <code>style.css</code>.<br>
Ainsi la totalité de nos fichiers <b>Javascript</b> sont contenu dans <code>bundle.js</code> et les fichier <b>CSS</b> dans <code>style.css</code> <br>
En plus de cela il intègre des outils comme le transpileur <b>Babel</b>, ainsi que la possibilité de <b>mignifier</b> le code pour un maximum de performance et l'utilisation de <code>maps</code> correspondant entre autres à la possibilité de voir dans la console du navigateur le <b>nom du fichier</b> où on trouve l'erreur (<code>development</code>), ce qui serait normalement impossible puisque l'<b>on a qu'un seul fichier</b> <code>bundle.js</code> où tous nos fichiers ont été unifiés.<br>
Pour faire simple il faut créer un fichier <code>webpack.config.js</code> dans lequel toutes les options sont configurés (par exemple l'entrée et la sortie des fichiers), webpack possède une <b>documentation détaillée</b> pour nous y aider, quoique complexe.<br>
<b>Webpack</b> est installé en <code>dependecy</code> et en <code>devDependency</code> avec notamment un mode <code>production</code> et un mode <code>developpement</code>, exactement comme le fait le framework <b>Angular</b>, puisque lui aussi utilise <b>Webpack</b>.<br> Dans <b>React</b> nous n'avons pas tous c'est outils préinstallés, c'est son défaut mais également sa qualité qui est appréciée pour les projets qui demande une grosse préparation, très utilisé donc pour les startup.<br>
A noter qu'il y a la possibilité d'utiliser <code>React create</code> qui possède les mêmes fonctionnalités qu' <code>Angular create</code> pour pouvoir être rapidement en place, puisque <b>Webpack</b> sera déjà préconfigurer ainsi que d'autres modules.
</p>
<br>
<h4>Loader ⌛</h4>

<p>
Un loader est une fonction de <b>Babel</b> un peu avancée qui permet de configurer le comportement de <b>Webpack</b> en fonction du fichier qu'il utilise, par exemple lorsqu'il lit un fichier <code>SCSS</code>, il demande à <b>Babel</b> de le transpiler en <code>CSS</code>. <br>
Pour cela il faut créer un fichier <code>.babelrc</code>qui, comme pour <b>Webpack</b>, permet de configurer <b>Babel</b> (écrit en <code>JSON</code>). <br>
<!-- Pour le loader <code>CSS</code> il permet à <b>Webpack</b> de transcrire le <code>CSS</code> dans son fichier javascrit.<br> -->
Pour le <code>style-loader</code> il permet d'injecter la balise <code>style</code> dans le <code>HTML</code> quand il créera son fichier javascript.<br>
Il permet également d'ajouter notre <code>reset.css</code> afin d'avoir des règles <b>CSS</b> de départ identiques sur TOUS les navigateurs.
</p>
<br>

<h4> Encore un avantage 📦</h4>

<p>
Un autre <b>avantage</b> qu'offre le bundler <b>Webpack</b> parmis tant d'autres est sa <b>rapidité d'execution</b> en mode développement.<br>
<b>Webpack</b> vient avec son propre <code>live-server</code> mais avec une fonctionnalité en plus qui change tout.<br>
Le module <code>webpack-dev-server</code> à
 la faculté de ne pas réécrire le fichier <code>bundle.js</code> à chaque changement. <br>
Au lieu de ça il utilise le fichier <b>mis en cache</b> préalablement, pour <b>éviter de redevoir analyser certaines partie du code inchangés une seconde fois</b>.<br>
Pour finir <b>Babel</b> et <code>web-server</code> s'utilise en une seul commande, à la manière d'un framework comme <b>Angular</b> encore une fois.
</p>
<br>
<h4> Ajout stage 2 ES8 🔨</h4>

<p>
L'utilisation de fonctionnalités qui ne sont pas encore intégrées dans le langage <code>Javascript</code> peut servir à rendre un code plus <b>propre</b> et plus <b>maintenable</b>.  L'organisation responsable de ces intégrations ou non s'appelle TC39 (Technical Comitee 39).<br>
Les étapes de leur intégrations se déroulent en plusieurs stages : <br>
</p>
<p>
<b>Stage 0</b> : Juste une idée. Peut ne jamais voir le jour.<br>
<b>Stage 1</b> : qui vaut le coup de travailler dessus.<br>
<b>Stage 2</b> : Prévu, début de l'implementation.<br>
<b>Stage 3</b> : début integration dans navigateur.<br>
<b>Stage 4</b> : fini.<br>
</p>

<p>
Pour des raisons de lisibilité ce projet en possède 3 : <br>
Ce nouveau <code>plug-in</code> s'appelle <code>proposal-class-properties</code> et permet de s'absoudre du <code>constructor()</code> de nos classes, et du <code>this</code> du <code>state</code> de <code>React</code> :
</p>

```javascript
// Sans Plugin
   class AddOption extends React.Component {
    constructor(props) {
      super(props);
      this.handleAddOption = this.handleAddOption.bind(this);
      this.state = {
        error: undefined
      };
    }
	handleAddOption(e) {
      //code ici
	  }

	render() {
      return (
          <form onSubmit={this.handleAddOption}>
		  )}
	}
  // Avec Plugin
   class AddOption extends React.Component {

    handleAddOption = (e) => {
      e.preventDefault();
    }
  }
```

Cela évite donc d'utiliser la fonction <code>bind()</code> qui permet de récupérer le <b>contexte</b> de la classe, ce que l'on doit faire à chaque fois que l'on utilise une méthode de la classe dans le <code>JSX</code> si on a pas le <code>plug-in</code> en question. <br>
Il semblerait que depuis peu <b>React</b> ai remédié au problème, mais n'ayant pas trouvé d'informations précises à ce sujet, je ne veux pas l'affirmer.<br>
Il y en a un autres qui est très utile dans <b>Redux</b> par exemple, le <code>object spread operator {...} </code> qui permet à la manière de son cousin pour les <b>array</b> <code>array spread operator [...]</code> de pouvoir écraser des propriétés similaires par les nouvelles. <br>
Durant mon oral j'expliquerais un peu plus en détails ce qu'il fait, pour faire simple <b>Redux</b> demande qu'un de ses composants soit <b>immutable</b>, c'est à dire qu'il ne doit <b>pas être changé</b> mais <b>copié</b>, et c'est ce que fait cet opérateur, il créer une copie de l'objet, il y a la également possibilité d'utiliser la méthode native de <b>Javascript</b> <code>assign()</code> qui fait sensiblement la même chose mais que je trouves moins lisible comparé au <code>spread operator</code>.
<br>
<br>

<h3> Comment créer du contenu <b>SPA</b> ? 🔬</h3>

Pour ne pas recharger nos pages lorsque le client clique sur un lien il nous faut une librairie qui nous fournisse le <code>routing</code>. <br>
Le <code>routing</code> est le procédé de garder l'<b>URL</b> du navigateur synchronisé avec ce qui doit être rendu. <br>
Pour faire simple :  il nous suffit de représenter nos routes très simplement sur une page comme ceci :

```javascript
<Router history={history}>
<Switch>
  <PrivateRoute exact path='/dashboard' component={UserDashboardPage} />
  <PrivateRoute exact path='/user/articles' component={UserArticlesPage} />
  <PrivateRoute exact path='/dashboard/mailBox' component={MailBoxPage} />
  <AdminRoute exact path='/dashboard/admin/createArticle' component={AdminCreateArticlePage} />
  <AdminRoute exact path='/admin/editArticle/:id' component={AdminEditOneArticlePage} />
  <AdminRoute exact path='/dashboard/admin/mailBox' component={AdminMailBox} />
  <AdminRoute exact path='/dashboard/admin/CreateSucceed' component={AdminSuccefullCreationPage} />
  <PublicRoute exact path='/login' component={LoginPage} />
  <PublicRoute exact path='/createAccount' component={CreateAccountPage} />
  <PublicRoute path='/articles' component={ArticlesPage} />
  <PublicRoute path='/' component={MainPage} />
</Switch>
</div>
</Router>
```

On appelle cela du <b>routing déclaratif</b> car il suffit de l'écrire pour qu'il soit pris en charge par <code>react-router</code><br>

La librairie la plus utilisée se nomme <code>react-router</code>, utilisable sur <code>react-nativ</code> également.<br>
Le <b>routeur</b> est représenté par un <code>Component</code> et il est le parent de tous les autres <code>Components</code>. <br>
Ainsi <code>Router</code> est l'élément qui est <b>toujours présent</b>, c'est pour cela que la page ne se charge jamais(pour faire simple). <br>
Dans une <b>SPA</b> une seule page est fournie par le serveur, cette page nous sert à <code>render</code> les composants de notre application de manière dynamique.<br>
<br>
<br>
<br>
<br>
<br>
<br>
<h3> Comment créer des opérations asynchrones ? (Traduction de l'Anglais) 🗽</h3>

<p>
Pour expliquer comment on réalise ce genre d'opération je vais profiter d'un article <b>en anglais</b> que j'ai utilisé qui explique ce genre d'opérations dans <code>React-Redux</code> en utilisant la notation <b>RSAA</b> qui est implémentée directement dans <b>Redux</b> et qui est la manière 'recommandée' de faire, mais que je n'ai pas pu tester sur mon application, j'utilise de simples actions plus facile à comprendre mais <b>moins lisible</b> et surtout <b>moins factorisable</b>, mais tout de même <b>largement pratiqué</b> par les développeurs web, peut être à implémenter plus tard.<br>
J'ai décidé par soucis de <b>lisibilité</b> de ne traduire que des bouts de paragraphes pour que le correcteur puissent jeter un coup d'oeil facilement sur le texte anglais correspondant, mon opinion est que certains mots anglais même certaines expressions <b>ne sont pas vraiment traduisable</b>, à cause de nos <b>cultures différentes</b>, des documentations <b>exclusivement écrite en anglais</b> et aussi car <b>je peux parler anglais</b> mais j'ai toujours eut du mal à traduire <b>français/anglais</b>, si vous en voyez la necessité, n'hésitez pas à me demander de <b>parler en anglais</b> ou d'<b>expliquer</b> un aspect de mon application en <b>anglais</b>. <br>
De ce fait j'ai <b>volontairement laissé</b> des mots qui ne me paraissaient pas <b>naturels</b> lorsqu'ils sont traduits, donc je les ai <b>laissé en anglais</b>.<br>

Ainsi :<br>
</p>

> It is very likely that, at a given point during the development of a client-side web app, it will be necessary to communicate with the server side using a RESTful API. If your stack is based on React/Redux, you may have found yourself wondering about where the ideal spot is to call your API and how to handle your application state based on the outcome of the interaction.
> The problem Having decided that we’re using Redux, calling the API from a container component (in the componentWillMount method for example) is not really an option because we want our Redux store to be the only place where the application state is held. Also, this would make our component harder to test, because we would be creating a dependency on the API that would then need to be mocked.
> We also know that a reducer isn’t a good place, because it has to be a pure function with the sole responsibility of transforming the current state into the next state according to a given action. Hence side effects, like API calls, are not allowed. A solution: using thunks
> Apparently, by a process of elimination, a good spot to make our API calls is inside the action creators. Action creators are meant, by default, to synchronously return actions as plain objects. Although it is possible to implement an async flow with plain action creators, it is much more advisable to use a middleware like Redux Thunk or Redux Promise. This way, our components “aren’t aware of how action creators are implemented, and whether they care about Redux state, whether they are synchronous or asynchronous, and whether or not they call other action creators”.
> An example using Redux Thunk middleware :
<p>
<br>
<br>
<br>
Il est très probable, qu'au un moment donné durant le développement d'une <b>web app</b> côtés client, il soit nécessaire de communiquer avec le serveur <b>back-end</b> utilisant une <code>RESTful API</code>. Si votre stack est basé sur <b>React</b> et <b>Redux</b>, vous devriez vous être retrouvé dans un moment où vous vous êtes demandé où était l'endroit idéal pour appeler votre <b>API</b> et comment prendre en main les <code>state</code> de votre application basé sur le résultat de vos intéractions.<br>
J'ai décidé que nous utiliserons <b>Redux</b>, appeler une <b>API</b> depuis un composant <code>container</code> (dans la method <code>componentWillMount</code> par exemple)  n'est pas vraiment une option car nous voulons que le store de <b>Redux</b> soit le seul endroit où les <code>state</code> de l'application soit contenus. De plus, cela créerais un composant plus difficile à tester, car nous créerions une dépendance dans l'<b>API</b> qui aurait besoin d'être <code>mocked</code>.<br>
Nous savons également que le <code>reducer</code> n'est pas un bon endroit, car cela doit être une fonction pure avec la seule responsabilité de transformer le <code>state</code> actuel dans le nouveau par rapport à une action donnée. Ainsi tout autres effets n'est pas permis.
Une Solution: Utiliser les <code>thunks</code>.
Ainsi, par processus d'élimination, le bon endroit pour faire des appels d'<b>API</b> est à l'intérieur des créateurs d'actions. Les créateurs d'action sont destinés, à l'origine, à retourner des actions sous la forme d'objets et des <b>opération synchrones</b>. Bien que il est possible d'implémenter un flux <b>asynchrone</b> avec des créateurs d'actions purs, il est beaucoup plus conseillable d'utiliser un <code>middleware</code> comme <code>Redux Thunk</code> ou <code>Redux Promise</code>. De cette manière, nos <code>components</code> ne sont pas conscients, ni de la manière dont sont implémentés les créateurs d'actions, ni des <code>Redux States</code>, ni s'ils sont <b>synchrone</b> ou <b>asynchrone</b>, ou encore s'ils appellent d'autres créateurs d'actions.<br>
Un exemple utilisant <code>Redux Thunk middleware</code> :<br>
<br>
<code>EXEMPLE DE CODE</code>
<br>
</p>

> In this example the last action creator returns a function (thunk) that receives the dispatch method as a parameter and is therefore able to dispatch actions asynchronously. What we usually want when we call an API endpoint is to dispatch an action immediately to notify our app that a request is pending (and show a loader for example). Once the request is handled by the server, we want to dispatch either a success action (along with the returned data) or an error action (in order to notify the user that something went wrong).
> There’s nothing wrong with this approach, but as our application grows we may want to try a different solution that will save us from unnecessary boilerplating and can give us some nice extra features to control the way we interact with our APIs.
> A different approach: using Redux API middleware
> A bit of history: Even if the examples provided in the Redux documentation about async actions make use of Redux Thunk, in the “real world” examples of the same repository, API calls are implemented using a custom API middleware. The concepts behind this example eventually evolved into the Redux API middleware.
> This middleware relies on the core concept of RSAA. Let’s have a quick look at it.
> CODE EXAMPLE
> RSAA stands for Redux Standard API-calling Action and it’s a specification for structuring actions that lives alongside the more familiar Flux Standard Action (FSA).
> An RSAA is identified by the presence of a [CALL_API] property. This property is a Symbol defined in the middleware itself.
> In order to have a valid RSAA the [CALL_API] property must be a plain object and contain the following properties:
> * endpoint (a string or a function returning a string),
> * method (a string amongst: GET, HEAD, POST, PUT, PATCH, DELETE or OPTIONS),
> * types (an array of length 3 containing strings or Symbols representing FSAs).
> Let’s see a minimal example from the docs to clarify that:
<br>
<br>
<br>
<p>
Dans cet exemple le dernier créateur d'action retourne une fonction (<code>thunk</code>) qui reçoit une méthode <code>dispatch</code> en tant que paramètre et est donc capable d'expédier une action de manière <b>asynchrone</b>. Ce que nous voulons généralement quand nous appelons l'<code>endpoint</code> d'une API c'est expédier une action immédiatement pour notifier notre application que la requete est en suspend (en train de se dérouler) (et montrer un chargement au client par exemple). Une fois que la requete est prise en main par le serveu, nous voulant expédier également une action de succés (avec les données retourné) ou une action d'erreur (pour notifier l'utilisateur que quelque chose s'est mal passé). <br>
Il n'y a rien de mauvais avec cette approche, mais à mesure que notre application évolue et s'agrandit nous devrions essayer une option différente qui va nous éviter des <code>boilerplates</code> intutiles et qui peut nous donner quelques bonne nouvelles fonctionnalités pour controler la façon dont on interact avec nos API. <br>
Une différente approche: Utiliser <code>Redux API middleware</code>. <br>
Un peu d'histoire : même si les examples fournis dans la documentation de <code>Redux</code> décrivant les actions asynchrones utilisent <code>Redux Thunk</code>, dans les exemples appelés <code>realWorld</code> (aka de vrais applications), les appels vers l'API sont implémenté en utilisant un <code>middleware</code> API personnalisé. Les concepts derrière cet example évolue au finalement à l'intérieur <code>Redux API middleware</code>. <br>
Ce middleware se repose sur les concepts fondamentaux appelé RSAA. Regardons rapidement ce que c'est.
<br>
<code>EXEMPLE DE CODE</code>
<br>
<b>RSAA</b> signifie <code>Redux Standard API-calling Action</code> c'est une spécification pour structurer les actions qui se rapprochent du plus connus <code>Flux Standard Action</code> (FSA). Un <code>RSAA</code> est identifié par la présence d'une propriété <code>[CALL_API]</code>. Cette propriété est un symbole défini dans le middleware lui-même. Afin d'avoir un RSAA valide la propriété <code>[CALL_API]</code> doit être un objet entier et contenir les propriétés suivantes :

*Un <code>endpoint</code> (un <code>string</code> ou une fonction retournant un <code>string</code>). <br>
*Une méthode (un <code>string</code> parmis <code>GET</code>, <code>HEAD</code>, <code>POST</code>, <code>PUT</code>, <code>PATCH</code>, <code>DELETE</code> et <code>OPTIONS</code>).<br>
*Types (un <code>array</code> de <code>length</code> 3 contenant des <code>strings</code> ou symboles représentant des <code>FSA</code>).<br>

Voyons un exemple simple venant de la documentation pour clarifier ça :<br>
</p>
<br>

<code>EXEMPLE DE CODE</code>
<br>

> Lifecycle
> Every action dispatched within the application will be passed through every registered middleware. To use it in  conjunction with other middlewares, like the Redux Thunk for example, we just have to specify the Redux API Middleware when we configure the store:
> If the action is recognised as an RSAA (due to the presence of the [CALL_API] property), it will be processed by Redux API middleware, otherwise it
> will be transparently passed to the next middleware.
> If the action is recognised as an RSAA by the middleware, the first FSA (in the example ‘REQUEST’) will be dispatched before making the ‘GET’ to the specified endpoint.
> If the request is successful, the second FSA (‘SUCCESS’) is dispatched together with the data sent from the server in the payload. Otherwise, the third
> FSA is dispatched with an object describing the error as its payload.
> This means we can now rewrite the Redux Thunk example from above like this:

<p>
Cycle de vie<br>
Chaque actions expédiées à l'intérieur de l'application vont passer au travers de chaque <code>middlewares</code> enregistrés.<br> Pour l'utiliser avec d'autre middlewares, comme le <code>Redux Thunk</code> par exemple, nous devons juste spécifier le <code>Redux API middleware</code> quand nous configurons le <code>store</code> :<br>
<br>
<code>EXEMPLE DE CODE</code>
<br>
Si l'action est reconnu en tant que <code>RSAA</code> (dût à la présence de la propriété <code>[ CALL_API]</code>), elle va être exécuter par <code>Redux API middleware</code> , sinon cela va être passé au prochain <code>middleware</code> de manière transparente.<br>
Si l'action est reconnu en tant que <code>RSAA</code> par le <code>middleware</code>, le premier <code>FSA</code> (dansl'exemple <code>REQUEST</code>) va etre expédié avant de procéder au <code>GET</code> vers un <code>end point</code> visé.<br>
Si l'action est un succès, le second <code>FSA</code> (<code>SUCCESS</code>) est expédié ensemble avec la donnée envoyée par le serveur dans le <code>payload</code> (les données utiles, très spécifiques à <code>Redux</code> en réalité, difficile à traduire ici).  Sinon le troisième <code>FSA</code> est expédié avec un objet décrivant l'erreur et le <code>payload</code> (encore). <br>
Cela signifie que l'on peu réecrire l'exemple comportant le <code>Redux Thunk</code> d'avant comme ceci : <br>
<br>
<code>EXEMPLE DE CODE</code>
<br>
</p>
> As you can see, a lot of the boilerplating has gone. We can, of course, import our actions from other modules and have, for example, a generic handler for request failures, if we want. Also, notice that we removed the isomorphic-fetch import, as it is used internally by the middleware.
> Other goodies
> Error handling
> Besides the error handling for a response with a status code outside of the 200 range, the middleware will also handle errors for non-valid RSAAs and for other scenarios, such as a network failure. In every case, the dispatched FSA will contain an error in the payload.
> This error will be an object of a class that extends the native Error class and it will be amongst these: InvalidRSAA, InternalError, RequestError,
> ApiError.
> FSA customisation
> Another useful feature of the library is the ability to deeply customise the dispatched FSAs. The specified FSA can, in fact, be an object that can contain, besides the type, a payload and/or a meta property.
> If the payload is specified as a function, this function will be able to access the action itself and the application state. This allows us to pass all the information we want inside our action.
<p>
<br>

Comme vous pouver le voir, beaucoup de <code>boilerplating</code> est parti. Nous pouvons, bien entendu, importer nos actions depuis un autre module et avoir, par exemple, un <code>handler</code> générique pour les requetes ayant échouées, si nous le voulons.<br>
Egalement veuillez noter que nous avons enlever l'import isomorphique <code>fetch</code>, étant donné qu'il est utilisé à
l'interieur du middleware.<br>
Autre bonne choses <br>
<code>Gérer les erreurs</code><br>
Outre les <code>error handling</code> pour une réponse avec un status code en dehors d'un intervalle de <b>200</b> à <b>300</b>, le  va également récupérer les erreurs des RSAA non valides et pour d'autres scénarios, comme un défaut de connection. Dans tous les cas, les <code>FSA</code> émis vont contenir une erreur dans le <code>payload</code>.<br>
Cette erreur sera un objet avec une classe qui étend la classe <code>Error</code> (javascript natif) avec ceci : <code>InvalidRSAA</code>, <code>InternalError</code>, <code>RequestError</code>, <code>ApiError</code>.
Customisation des <code>FSA</code> <br>
Une autre fonctionnalité utile de la libraire est l'abilité de customiser à l'extrême les <code>FSA</code> émisent. La FSA spécifiée peut, en effet, être un objet qui peut contenir, avec le <code>type</code>, un <code>payload</code> et/ou une propriété <code>meta</code>.<br>
Si le <code>payload</code> est défini comme étant une fonction, cette fonction sera capable d'accéder à l'action elle-même et aux <code>state</code> de l'application. Cela nous permet de lui passer toutes les information que nous voulant à l'interieur de nos actions.<br>
<br>
<code>EXEMPLE DE CODE</code>
<br>
</p>


Je n'ai pas fini la traduction car il en restait encore un bon morceaux et que le quota est largement atteint.<br>

---
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<h2> Styling 🎨</h2>

<h3> SASS</h3>

<p>
Principales spécificités : <br>
1.Toutes les valeurs de dimensionnements sont exprimées en <code>em</code> lorsque c'est possible. <br>
2.Pour ne pas avoir une fonction <b>SASS</b> pour avoir une conversion de pixels vers <code>em</code>, le projet défini le <b>font-size</b> du <code>body</code> dans le fichier <code>_base</code> donc de toutes nos pages a une valeur de <b>62,5%</b> ce qui a pour effet de pouvoir definir nos <code>em</code> en base de <b>10</b>, il n'y a plus de conversion à faire. <code>16px = 1.6em</code>.<br>
3.<b>SASS</b> permet de <b>nested</b> les selecteurs entre-eux, cependant plus il y a de sélecteurs plus la <b>lisibilité</b> du code devient <b>difficile</b> et donc la maintenabilité du code également. <br>
Pour éviter cela, le projet utilise la convention <b>BEM</b>(<code>Block Element Modifier</code>) qui est une manière de <b>structurer</b> nos selecteurs <b>CSS</b> et de leur donner une logique <b>facilement compréhensible</b> d'un simple coup d'oeil. C'est ce qu'utilise un framework comme <b>Bootstrap</b>, largement utilisé dans le web et approuvé par la communauté. <br>
5.Création de <code>mixin</code> pour des opérations <code>CSS</code> répétitives, en essayant autant que possible d'y intégrer la compatibilité des navigateurs.<br>
6.Le projet suit autant que possible la convention <b>SASS</b>.<br>
7.Le projet a prévue de ne pas employer le nom des classes <b>Bootstrap</b> pour éviter les conflits avec une prochaine implémentation.<br>
</p>

Pour l'organisation des classes <b>CSS</b>, la convention <b>BEM</b> à été utilisée.

Voici la structure des fichiers <b>SASS</b> simplifiée :

<kbd>
<p align="center">
<a href="url"><img src="images/sassstructure.png" height="560" width="590" ></a>
</p>
</kbd>

---

<br>

<h2> Où en est l'application ? 🤔</h2>

L'application n'est pas terminée, notamment le styling n'est seulement qu'à un stade primitif visuellement, même si la logique est là, il manque à réaliser le <b>CSS</b> qui permettra un visuel agréable à l'utilisateur. <br>
La fonctionnalité <b>messagerie</b> est bien implémentée et fonctionne, chaque composants possèdent leur logiques propres :<br>
<code>UnreadMessagesList</code>, <code>SaveMessageList</code>, <code>SendMessageList</code>, cependant il reste quelques fonctionnalité à ajouter pour certains, surtout visuels, comme le fait d'avertir le client lorsqu'un message à eut une erreur lors de l'envoi, pour certains la fonctionnalité est là, pour d'autres pas, tout dépend du composant.<br>
Il reste encore beaucoup de travail à réaliser, et dès le départ je le savais, je l'avais bien spécifié à mon client et mon but était avant tout de créer une structure <b>claire</b> et <b>facilement reprenable</b> par un développeur qui connais les technologies associées. <br>
<br>

<h2> Le retour de mon client 🤴</h2>

<p>
Mon client n'étais malheureusement pas très disponible durant le long de mon stage, heureusement nous avions eut un bon contact où j'ai pu receuillir un maximum d'informations durant nos entretien. <br>
Ainsi mon client a été agréablement surpris par les fonctionnalités présentes lors de la fin de mon stage, car je ne lui avait pas promis de réaliser la partie messagerie. <br>
Le <b>point négatif</b> est le <code>styling</code> qui n'étais pas optimal lors du compte-rendu final, je lui ai bien expliqué que l'habillage n'étais qu'une composante de l'application et je lui ai bien fait comprendre également qu'une application 'belle' visuellement ne voulait pas dire que l'expérience utilisateurs serait <b>optimal</b> et que l'équilibre entre le visuel (<b>UI</b>) et l'expérience (<b>UX</b>) était très importante et qu'il ne fallait pas la négliger.<br>
Je lui ai également dit que le <code>styling</code> ne devrait pas prendre énormément de temps car l'application est composé de composants réutilisés de multitudes fois, donc il suffit de styliser un composant une fois pour qu'ils le soient à chaque fois, seul le composant parent qui acceuille tous les composants devra être personnalisé en fonction de l'utilisateur.<br>
</p>
<br>
<br>
<br>
<br>
<br>
<h3> Ce qui est déjà implémenté 🏛️</h3>

<p>
Je vais lister quelques-unes des fonctionnalités qui sont déjà implémentées : <br>
1. Voir visuellement à l'aide d'un widget le <b>nombre de messages non-lus</b>.<br>
2. <b>Trouver</b> un utilisateur à l'aide de son pseudoName.<br>
3. <b>Changer</b> son mot de passe.<br>
4. <b>Supprimer</b> son compte.<br>
5. <b>Enregistrer</b> un message, l'editer</b> et l'envoyer</b>.<br>
6. <b>Voir</b> les messages reçus, les messages envoyés et les supprimer</b> (un par un).<br>
7. <b>Créer</b> un compte.<br>
8. <b>Se connecter</b> à un compte.<br>
9. <b>Ajouter un article</b>, avec une image.<br>
10. <b>Ajouter</b> un avatar (image Profil).<br>
11. <b>Loader personnalisé</b> (roue qui tourne) pour chaque composants visibles lors de leur chargement.<br>
12. Espace <b>Admin</b> et espace <b>Utilisateur</b> normal.<br>
</p>
<br>
<h3> Ce que le repreneur devra implémenter 👷</h3>

<p>
Je vais lister quelques-unes des fontionnalités qui restent à implémenter : <br>
1. <b>Editer un article</b> (<b>Admin</b>).<br>
2. <b>Améliorer la précision des messages</b> des <b>pop-up</b>.<br>
3. Revoir les entités <b>Role</b> et <b>User</b> de la partie back-end. Selon moi <b>Role</b> doit être 'indépendante' à la manière de l'entité <b>Avatar</b>, et on doit stocker son <code>id</code> dans l'entité <b>User</b>.<br>
4. Acheter un domaine pour la messagerie <b>Sendgrid</b> pour éviter que les messages finissent en <code>spam</code> de la boite e-mail.<br>
5. <b>Ajouter des commentaires aux articles</b> (routes Back-end déjà implémentées).<br>
6. <b>Changer son pseudoName</b>.<br>
7. <b>Ajouter son emploi du temps</b> (<b>Admin</b>)(routes Back-end déjà implémentées).<br>
8. Changer le status d'un utilisateur (<b>Admin</b>) 'guest' vers 'admin' donc.<br>
9. Supprimer TOUS les messages</b> d'un seul click.<br>
10. Voir tous les <code>admin</code> et les sélectionner pour intéragir avec eux Un Component à part.<br>
11. Se déconnecter de tous les comptes associés (suppression de tous les <b>JWT</b>, routes Back-end déjà implémentées).<br>
12. <b>Ajouter un éditeur de texte intégré</b> qui permettra à l'admin voir aux utilisateurs d'enrichir leur texte (articles, commentaires et messages).<br>
</p>
<br>
<br>
<br>
<br>
<br>

<h2> Ce que le repreneur devra comprendre 🤝</h2>

<p>
<b>SASS</b> : <br>
1.Toutes les propriétés <b>CSS</b> sont triés par ordre alphabetique <code>ascendant</code>. [conseil plugin vscode](https://marketplace.visualstudio.com/items?itemName=Tyriar.sort-lines) <br>
2. Les propriétés <b>CSS</b> sont triés par ordre alphabétique pour la <b>rapidité</b>, pour chercher une propriété il suffit de connaitre sa <b>premiere lettre</b>.<br>
3. Pour rechercher un composant ou autre, il suffit la plupart du temps d'utiliser l'outil de <code>recherche</code> de l'editeur, les fichiers et les noms on été pensé pour utiliser cette fonctionnalité.<br>
4. Il existe un fichier nommé <code>variables</code> où sont déclarés les variables principales du thème. La structure à été créer pour que autant que possible on n'ai pa besoin d'étudier et modifier les fichier <b>SASS</b>, si on change la valeur par defaut d'une variable d'une propriété <b>CSS</b>, tout se répercute sur l'ensemble du thème avec l'utilisation des fonctions <b>SASS</b> et de la fonction <b>CSS</b> <code>calc()</code> sous-entendu dans les fonctions <b>SASS</b>.<br>
</p>


---
<br>
<h2> Aller plus loin ✈️</h2>

<p>
1. Factoriser les paramètres lors des quieries (<code>sort</code>, <code>limit</code>)<br>
2. Améliorer la compatibilité vieux navigateurs.<br>
3. Améliorer la responsivité (media queries etc).<br>
4. Améliorer les erreurs lors de l'utilisation des mixins et de functions (mauvais parametres) <code>@error</code>.<br>
5. Créer une documentation plus fournie qu'elle ne l'est, par manque de temps, la documentation cotés front end est faible.<br>
</p>

---
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<h2> Annexes et informations supplémentaires 📚</h2>

<h3> Sructure des fichiers de la partie front-end</h3>
<kbd>
<p align="center">
<a href="url"><img src="images/structurefe.png" height="500" width="800" ></a>
</p>
</kbd>
<h3> Code Sengrid (gestion email Back-end)</h3>

<p>Envoi d'un email personnalisé lors de la suppression/création d'un compte (message simplifié)</p>

```Javascript
const sgMail          = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomEmail = (email, name) => {
    const msg = {
        to: email,
        subject: 'Bienvenue au groupe médical ! 🧘 💌',
        from: 'axeldaguerredev@gmail.com',
        text: `Bonjour ${name}, ravie de vous compter parmis notre groupe médical !\n
                Le Groupe Médical. 👨‍⚕️👩‍⚕️🏥`
    }
    sgMail.send(msg)
}

const sendCancelationEmail = (email, name) => {
    const msg = {
        to: email,
        from: 'axeldaguerredev@gmail.com',
        subject: `C\'est si Triste de Te Voir Partir ${name} ! 🥺`,
        text: `C'est dommage de vous voir partir ${name} !
        Mais sache que vous pouvez recréer un compte quand vous le souhaitez.
        Le Groupe Médical. 👨‍⚕️👩‍⚕️🏥`
    }
    sgMail.send(msg)
}

module.exports = {
    sendWelcomEmail,
    sendCancelationEmail
}
```
---

<h3> Enregistrement des <code>states</code> dans local storage et récupération des données. 💾</h3>

<p>Fonction qui permet de sauvegarder et charger nos données depuis <code>localStorage</code></p>
<p><code>helpers/localStorage.js</code></p>

```javascript
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')

    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    // laisse le reducer initialiser le state
    return undefined
  }
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch (err) {
    console.log('saveStorage failed')
  }
}
```
<p>On charge nos données depuis localStorage</p>
<p><code>store/configureStore.js</code>.</p>

```Javascript
// on Load les données enregistré dans localStorage.
const persistedState = loadState()

export default () => {
  const store = createStore(
    combineReducers({
      articles: articlesReducer,
      user: authReducer,
      adminArticleCreation: articleAdminReducer,
      creationAccount: accountCreationReducer
    }),
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
  )
```

<p> On sauvegarde nos données voulu dans <code>localStorage</code>.</p>

<code>store/app.js</code>
```Javascript
// throttle(lodash) permet d'éviter d'utiliser lafonction trop souvent(JSON.Str)
// est couteuse.
store.subscribe(throttle(() => {
  saveState({
    // On choisi les states à save dans localStorage(tout sauf UI state)
    articles: store.getState().articles,
    user: store.getState().user,
    adminSpace: store.getState().adminSpace,
    creationAccount: store.getState().creationAccount,
    filters: store.getState().filters
  })
}, 1000))

```

---

<h3> Exemple d'un prototypage réalisé durant ma formation 🎨</h3>
<p>
Je n'ai pas eut le temps durant mon stage de réaliser un <b>prototypage</b> car le temps manquait, de ce fait il y a eut une longue discussion au départ pour éviter les fausses routes au maximum, le plan était clair et le client ne recherchait pas quelque chose de très spécifique, il s'en ai remis à mes connaissances et ma capacité à juger de la pertinance de l'<b>UX</b> et de l'<b>UI</b>. <br>
De ce fait voici le prototypage de ce qui aurait dût être mon projet. <br>
Résumé : Un site web pour apprendre de nouveaux domaines, de l'e-learning pour faire simple. L'application devait également posséder une animation javascript avec des <b>SVG</b>. <br>Cela à été abandonné au détriment de ce projet qui était beaucoup plus intéressant et remplissaient le référentiel beaucoup plus aisémant. Ce prototypage à été réalisé sur <b>Adobe XD</b>, dont les pages ont été crées sur <b>Photoshop</b>, tous les icones ont été crées avec <b>Illustrator</b> à l'aide de l'outil <code>pathfinder</code>.
</p>

<kbd>
<p align="center">
<a href="url"><img src="images/prototypage.png" height="700" width="850" ></a>
</p>
</kbd>
<br>
<br>
<br>
<br>
---
<h3> Documentation Back-end Postman 📰</h3>
<p>
<b>Postman</b> permet de générer un URL contenant toute la documentation des URL, voici un aperçu de ma documentation
 du backend, cette documentation permet de travailler en collaboration en générant simplement et très rapidement une documentation
 claire et bien présentée.
</p>

<kbd>
<p align="center">
<a href="url"><img src="images/postman.png" height="534" width="939" ></a>
</p>
</kbd>
<kbd>
<p align="center">
<a href="url"><img src="images/postman2.png" height="461" width="939" ></a>
</p>
</kbd>
---
<h3> Liste des outils utilisés 🛠️</h3>

<h3> Editeurs de code✏️</h3>

<p>
1.Atom <br>
2.VS code<br>
3.Emacs<br>
</p>

<h3> Langages 🎌</h3>

<p>
1.Javascript<br>
2.CSS (SASS)<br>
3.batch (scripts)<br>
4.HTML<br>
5.JSON<br>
</p>

<br>
<h3> Technologies 🛰️</h3>

<p>
1.MongoDB<br>
2.Yarn<br>
3.Heroku CLI<br>
4.Webpack<br>
<img src="images/outils.png" height="300" width="400" align="right">
5.Babel<br>
6.NodeJS<br>
7.Express<br>
8.React<br>
9.Redux<br>
10.Jest<br>
11.gitLab<br>
12.React-router<br>
13.Redux thunk<br>
14.Yup<br>
15.EsLint<br>
16.tern<br>
17.Git<br>
18.MongoDB Compass<br>
19.MongoDB Atlas<br>
</p>
<br>
<br>
<br>
<br>
<br>
<h3> Editeurs d'images 🖼️</h3>
<p>
1.Illustrator<br>
2.Photoshop
</p>

<h3> Références, documentation et autres 🔍</h3>

<p>
1.Repository React<br>
2.Repository Redux<br>
3.Udemy<br>
4.Site web Andrew Mead<br>
5.Site web SASS et sa Guidelines<br>
6.Codepen<br>
7.CSS-Tricks<br>
8.Stackoverflow<br>
9.BEM guidelines<br>
</p>

<h3> Provenance images de couverture 🖼️</h3>

<p>
1.https://svgsilh.com<br>
2.https://purepng.com<br>
</p>
