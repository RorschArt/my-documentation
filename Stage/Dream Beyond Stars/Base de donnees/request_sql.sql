
/* *************** SELECT ***********************************/

/*EST CE QUE LES REPONSES DONNEES PAR L'USER avec son id specifier SONT BONNES ? */

SELECT  users.lastname, questions.name, possible_answers.name 
FROM users 
JOIN user_answers 
ON users.id = user_answers.id_user 
JOIN possible_answers 
ON user_answers.answers = possible_answers.name 
JOIN questions
ON possible_answers.questions = questions.name
Where user_answers.answer_selected = 0 
AND possible_answers.good_answer = user_answers.answer_selected
AND users.id = 1; 

/*-----------------------------------------------------------*/

/* L'USER A REPONDU QUOI AVEC LES QUESTIONS ASSOCIEES(id en param)  ? */

SELECT questions.name, possible_answers.name 
FROM users 
JOIN user_answers 
ON users.id = user_answers.id_user 
JOIN possible_answers 
ON user_answers.answers = possible_answers.name 
JOIN questions
ON possible_answers.questions = questions.name
Where user_answers.answer_selected = 1
AND users.id = 1;

/*-----------------------------------------------------------*/

/* QUEL SONT LES BONNES REPONSES ET LEURS QUESTIONS ASSOCIEES ? */

SELECT questions.name, possible_answers.name
FROM users 
JOIN user_answers 
ON users.id = user_answers.id_user 
JOIN possible_answers 
ON user_answers.answers = possible_answers.name 
JOIN questions
ON possible_answers.questions = questions.name
Where user_answers.answer_selected = 1
AND users.id = 1;

/*-----------------------------------------------------------*/

/* QUEL EST LA PART TOTALE DES REPONSES DONNEES PAR LES UTILISATEURS 
EN FONCTION DES QUESTIONS ? (parametre a changer)*/

SELECT user_answers.answer_selected
FROM users 
JOIN user_answers 
ON users.id = user_answers.id_user 
JOIN possible_answers 
ON user_answers.answers = possible_answers.name 
JOIN questions
ON possible_answers.questions = questions.name
Where user_answers.answer_selected = 1
AND questions.name = "Combien de temps penses tu qu'il faut pour aller sur mars ?" ;


/*-----------------------------------------------------------*/

/* QUEL EST LE FEEDBACK DONNE PAR UN USER AINSI QUE SA NOTE A L'APPLICATION, LONGUEUR MINI 120 ?*/

SELECT users.feedback_stars, users.feedback_text, users.firstname 
FROM users_suggestion 
JOIN users 
ON users.id = user_suggestions.id_user;
WHERE LENGTH (user_suggestions.content) > 120

/*-----------------------------------------------------------*/

/* COMBIEN DE USERS ONT DONNER LEUR AVIS (minimum noté avec des étoiles ? */

SELECT COUNT(users.feedback_stars)
FROM users;

/*-----------------------------------------------------------*/

/* RECUPERER LA TOTALITE DES FEEDBACKS ET LEUR CONTENU (stars + content) */

SELECT users.feedback_text, users.feedback_stars, users.username
FROM users
GROUP BY users.username 
ORDER BY users.username


/* RECUPERER LES SUGGESTIONS DES UTILISATEURS */

SELECT user_suggestions.content 
FROM user_suggestions;

/*-----------------------------------------------------------*/

/* RECUPERER LES SUGGESTIONS,LE NOM, L'EMAIL, LA DATE CREER AVEC UN MINIMUM DE LONGUEUR (100) */

SELECT users.lastname, users.email, user_suggestions.content
FROM user_suggestions 
JOIN users 
ON users.id = user_suggestions.id_user
WHERE LENGTH (user_suggestions.content) > 100;

/*-----------------------------------------------------------*/

/* RECUPERER LE NBRE TOTAL DE PERSONNE AYANT TENTER L'EXPERIENCE*/





/*-----------------------------------------------------------*/

/* PRENDRE  LE MOT DE PASSE DE L'USER */

SELECT users.password 
FROM users
WHERE users.email = 'litooo@ho'

/******************************** STATS **********************/

/* CALCULER LE NBRE DES UTILISATEURS QUI ON CHOISI X REPONSE POUR LA QUESTION X */

SELECT COUNT( user_answers.answer_selected )
FROM user_answers
JOIN possible_answers 
ON possible_answers.name = user_answers.answers
WHERE user_answers.answer_selected = 1
AND possible_answers.name = '28 jours' ;

/*-----------------------------------------------------------*/

/* CALCULER LE NOMBRE DE PERSONNE DANS X TYPE DE COURS */

SELECT COUNT(user_courses.id_user)
FROM user_courses
JOIN fields_studies 
ON fields_studies.name = user_courses.field_study
WHERE fields_studies.name = 'Astronomy' ;













