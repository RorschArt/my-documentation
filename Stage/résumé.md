<h1> 📑 Projet Groupe Médical Axel Daguerre 📑</h1>
<!-- <kbd>
<p align="center">
<a href="url"><img src="images/resumé.png" height="400" width="750" ></a>
</p>
</kbd> -->
<p>💊 <b>Domaine</b>: Médical.<br></p>
<p>😀<b> Utilisateurs visés</b> : Les patients du groupe médical</p>
<p>
📰 <b>Résumé</b> :
<br>
<br>
Ce projet est constitué d'une web application <code>Single Web Page</code> qui doit permettre aux patients et aux Professionnels du groupe d'avoir la possibilité de pouvoir <b>échanger simplement</b>, d'avoir rapidement accès aux <b>disponibilités des professionnels</b> à l'aide d'un calendrier (une interface simple par 1/2 journée, peu précise), d'avoir accès à des <b>articles</b> donnant des informations sur les professionnels du groupe et de leur permettre de les rédiger simplement, le site web est une sorte de <b>prototype</b>. <br>
Il doit cependant être <b>reprenable</b> s'il plait aux professionnels du groupe médical et qu'il leur semble être un <b>outil positif</b> à leur pratique.
<br>
<br>
⚙️ <b>Fonctionnalités implémentées et fonctionnelles</b> :
<br>
<br>
<ul>
<li>Messagerie entre <code>Patients/Professionnels</code> & <code>Patients/Patients</code>. </li>
<li>Espace <code>Admin</code> / <code>Utilisateurs connéctés</code> / <code>Utilisateurs non-connectés</code>.</li>
<li>Lister les message reçus, sauvegardés et envoyés.</li>
<li>Supprimer les messages individuellement ou en une seule fois.</li>
<li>Créer un compte,le supprimer, changer le mot de passe et l'image de profil.</li>
<li>Envoi d'un e-mail à l'adresse e-mail donné lors de la création, e-mail lors de la création et lors de la suppression.</li>
<li>Site web <b>SPA</b>.</li>
<li>Possibilité d'avoir une image de profil.<br>
<li>Possibilité d'écrire un article par l'<code>Admin</code>.</li>
</ul>
<br>
</p>
<p>
🛰️ Technologies utilisées :
<ul>
<li>React + Redux.</li>
<li>MongoDB.</li>
<li>Express.</li>
<li>Babel.</li>
<li>Webpack.</li>
<li>React-router.</li>
<li>Full Javascript.</li>
</ul>
</p>
