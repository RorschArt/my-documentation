# __Mozilla Firefox Raccourcis__
## Navigation :
* `Alt + <-`  Back
* `Alt + ->`  Forward
* `Ctrl + R` Recharger Page
## Tab
* `Shift + Tab` Selection Precedente
## Recherche :
* `Ctrl + k` Rechercher onglet Google
* `f6` Rechercher (NavBar)
## Page :
* `Ctrl + Haut` Remonter En Haut de La Page
* `Ctrl + Bas` Aller En Bas De La Page
## Find :
* `'` Quick Find (Seulement Links)
* `/` Quick Find
## Onglets :
* `Ctrl + W` Fermer L'onglet
* `Ctrl + Shift + N` Undo La Fermeture D'un Onglet
* `Ctrl + Tab` Cycle Dans Les Onglets
* `Ctrl + T` Ouvrir Un Onglet
* `Ctrl + Shift + P` Ouvrir Un Onglet Prive
## Sidebar et Autres:
* `Ctrl + B` Bookmarks Sidebar
* `Ctrl + J` Downloads
