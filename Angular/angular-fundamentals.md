# Angular :
## Creer des projets d'ancienne version :
[Medium](https://medium.com/polyglots-blog/creating-angular-v5-project-when-your-global-cli-version-is-6-x-cb9888bb5a6f)
[Stack Overflow](https://stackoverflow.com/questions/48230917/how-to-install-angular-cli-with-angular-v4-x)
## Creation projet :
* `ng new nameProject`.
## Très bonne chose a apprendre :
* [ngrx](https://ngrx.io/)
## Installer les outils pour utiliser Angular :
* `npm install -g @angular/cli`.
## Start Server :
* `ng serve`.
## Localisation :
* [110n et 118n](https://www.w3.org/International/questions/qa-i18n)
* [110n et 118n Angular](https://docs.angularjs.org/guide/i18n)
## Les filtres Pipe :
* [Pipe Angular](https://angular.io/guide/pipes)
```javascript
<p>{{ date | date : 'mediumDate' }}</p>
```
## Generer Directive :
```javascript
`ng g directive coucou`
## Directives :
import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
    constructor(el: ElementRef) {
       el.nativeElement.style.backgroundColor = 'yellow';
    }
}
```
## Afficher un tableau d'objet :
`*ngFor= "let creator of creators"`
## Injection :
`@Injection` est juste utilisé pour dire à **Angular** qu'il doit ajouter au `constructor()` le service pour ne pas que l'on ai a binder notre service à notre component, ce qui voudrait dire les lier, c'est très mauvais.
## Ne pas oublier de mettre nos providers(injection) dans ` @NgModule / app.modules.ts`
Dans un service `@injection` n'est là que pour donner la possibilité au service d'avoir des dependencies dans le `constructor()`, s'il n'en a pas il n'est pas obligatoire d'en mettre.
## Ajouter Bootstrap à Angular :
* `npm i bootstrap --sav` save rajoute le module au `package.json`.
* Dans `style.css` : `@import '~bootstrap/dist/css/bootstrap.css'`
## Two ways-binding:
Dans angular pour faire du **Two-Ways-Binding** il nous faut utiliser la notation 'banana in the box', ainsi on utilise : `[(ngModule)]=propriétéDeLaClasse`, ainsi au lieu de devoir passer des arguments depuis notre `$event` dans notre fonction, il n'est plus necessaire de le faire, cela donne un code plus propre et surtout des methode dans nos classe sans paramètre donc réutilisable n'importe où puisqu'elles se suffisent à elle même. C'est là toute l'utilité de la programmation orienté objet.
## Utiliser forms Module :
`import { FormsModule } from '@angular/forms'`.
