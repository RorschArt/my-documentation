<h1>Gatsby JS</h1>

<h2>Script de Déploiement sur gh-pages branch</h2>

<p>
Doc Gatsby expliquant la démarche pour deployer nos modif avec un simple script.
(https://www.gatsbyjs.org/docs/how-gatsby-works-with-github-pages/).
</p>

<h2>Ajouter un layout component avec <code>typography</code></h2>

<p>
Doc Gatsby expliquant l'utilisation simple de typography
(https://www.gatsbyjs.org/tutorial/part-three/#your-first-layout-component)
</p>
