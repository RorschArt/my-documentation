# Object Oriented Programming

Gros Avantage par rapport au Procédural, au fur et à mesure que l'application grandit le Procedural Programming devient difficilement maintenacle dût a la complexité de retracer "à la main" le code.
Le procédural est principalement une logique créer à l'aide de fonctions, or si la structure des données qui passent dans la fonction change cela risque de casser notre code.
De nombreux autres désavantage sont à noter :

* Difficulté de faire évoluer notre code sur de gros projets.
* Difficulté à Débugguer.
* Difficulté à réutilisé des parties du code.
* Fragilité du code.

Cependant Le Procédural n'est pas à jeté et il est obligatoire pour certaines tâches.

---

## Que signifie OOP

OOP signifie que l'on structure notre code en créant des **Classe** Et Des **Objets**.

## Pointers et Accès aux Méthodes

Pour avoir accès au méthode De notre Classe ou Objet depuis un pointer, nous devons `(*className).methodName()` car le `*` opérateur n'a pas la priorité.
Cette Notation peut être difficile à lire donc **C++** permet d'utiliser une autre notation plus explicite : `className->methodName()`.

## Class member access modifier

### **public**  😄

Accessible partout.

### **private**  🕵

Accessible seulement par les membres ou enfants de la classe.

### **protected** 🔒

Utilisé avec l'héritage.

```c++
class Player{
    public:
        int health;
        int xp;

    private:
        int age;
        string name;
};
int main()
{
    Player user;
    user.age = 256; //ERROR
    user.health = 250;
}
```

⚛️