# Visual Studio pour C++

Visual Studio build un projet de manière étrange et il faut a chaque fois restructurer nos dossier, en effet il met les fichiers dans un dossier intermediaire, le `.exe` est dans un dossier à par entiere à la racine du projet dans `Debug` <br>
Il faut donc **cliquer droit** sur le deuxieme fichier et non pas le premier qui est la solution et choisir <br>
* Platforms : `AllPlatforms`
* `GeneralProperties `:
1. Dans `Output Directory` taper : `$(SolutionDir)bin\$(Platform)$(Configuration)\`
2. Dans `Intermediate Directory` `$(SolutionDir)bin\intermediates\$(Platform)$(Configuration)\`

[Youtube](https://www.youtube.com/watch?v=qeH9Xv_90KM&t=76s)

## Define

On peut definir des mots comme des valeurs compris par le compilateur  
`#define INTEGER in` -> on peut maintenant faire : `INTEGER variableName = 5;`

## if endif

On peut demander au compilateur d'inclure une partie du code en fonction d'une condition  

```c++
#if 1 // sera inclus (si 0 ne le sera pas)
int main() {
	std::cout << "hello World";
	std::cin.get();
}
#endif
```

## Deux étapes : compilation & link

Pour comprendre voici un fait : si on ne créer pas de fichier `main.cpp` et que l'on `compile` notre **solution** il n'y aura pas d'erreur.  
En revanche si on `build` notre solution on aura une erreur qui dira que "aucun point d'entrée pour la solution"

## static

Il est très important de comprendre comment le compilateur de C++ fonctionne, par exemple lorsque l'on declare une fonction situé dans un autre fichier
```c++
void log(const char* name);

int multiply(int a, int b) {
	return a * b;
}

int main() {
	std::cout << multiply(5, 10);
	std::cin.get();
}
// 50 
```
**En revanche** si on spécifie cette fonction dans une fonction **qui n'est pas appelé** on aura une erreur, car le linker regarde à l'intérieur de la fonction et détermine que cette fonction peut être appelé dans un autres fichier, donc il donne une erreur.  

```c++
void log(const char* name);

int multiply(int a, int b) {
	log("multiply");
	return a * b;
}

int main() {
	std::cin.get();

}
//ERROR LINKER 
```

Il faut bien comprendre que ceci est une erreur du linker et non pas du compilateur, si on compile ce fichier nous n'aura pas d'erreur et la compilation sera un succés.  
Pour faire comprendre au linker que cette fonction est utilisé **seulement** dans ce fichier, donc éviter l'erreur il suffit de définir la fonction comme `static`  

```c++
static int multiply (int a, int b) {
	return a * b;
}
// pas d'erreur
```

## Naif Compilateur 

On peut lui déclarer n'importe quoi, le compilateur nous croit c'est pourquoi nous n'avons pas d'erreur lorsque l'on compile mais pas lorsque l'on buil. Car c'est là que le linker entre en jeu.  
Le linker est là pour **lier** tous les fichiers qui ont été compiler par le compilateur.  
Pour faire simple le compilateur va **compiler** chaque fichiers **séparemment** dans un language compréhensible par la machine, le linker va lui aller chercher tous les fichiers et s'occuper de la logique du programme

## Inclure une seul fois un header file 

```c++
#ifndef _LOG_H
#define _LOG_H

#endif
```

## Debugger ou the way of learning 

Il est important de savoir se servir du debuggeur car il nous aide énormément à savoir comment le language fonctionne. 
tips: Il est possible d'ouvrir la fenetre de la mémoire pour voir les updates, et on peut acceder aux adresse mémoire en faisant un `&variableName`  
Le build lors d'un debugg est plus long car il fait des choses en plus : comme par exemple mettre un `cc` lorsqu'une variable à été déclaré mais pas initialisé.  
Ainsi on peut avoir des infos en plus lorsque l'on debug.

 