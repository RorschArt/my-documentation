# Bookmarks

## Explication SDL_Video Structure 

Page 59.

## Explication représentation pixels dans le jeu 
Page 67.

## Structure de SDL_surface et SDL_color

**STRUCT SDL_Surface**
```c++
typedef struct SDL_Surface {

Uint32 flags;/**< Read-only */
SDL_PixelFormat *format;/**< Read-only */
int w, h;/**< Read-only */
Uint16 pitch;/**< Read-only */
void *pixels;/**< Read-write */

/** clipping information */
SDL_Rect clip_rect;/**< Read-only */
/** Reference count --used when freeing surface */
int refcount;/**< Read-mostly */
/* --other members that are private --*/
} SDL_Surface;
```

**Struct SDL_Color**

```c++
typedef struct{
Uint8 r;
Uint8 g;
Uint8 b;
Uint8 unused;
} SDL_Color;
```

Struct SDL_PixelFormat : 

```c++
/** Everything in the pixel format structure is read-only */
typedef struct SDL_PixelFormat {
SDL_Palette *palette;
Uint8  BitsPerPixel;
Uint8  BytesPerPixel;
Uint8  Rloss;
Uint8  Gloss;
Uint8  Bloss;
Uint8  Aloss;
Uint8  Rshift;
Uint8  Gshift;
Uint8  Bshift;
Uint8  Ashift;
Uint32 Rmask;
Uint32 Gmask;
Uint32 Bmask;
Uint32 Amask;
/** RGB color key information */
Uint32 colorkey;
/** Alpha value information (per-surface alpha) */
Uint8  alpha;
} SDL_PixelFormat;
```

Page 68.

## Toutes les HEX color

Page 69.

## Function SDL_UpdateRect()

Page 77.

## Function SDL_WM_SetCaption()

Page 78.



