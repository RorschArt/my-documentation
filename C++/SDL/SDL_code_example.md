#include <iostream>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#undef main

using namespace std;
int main(int argc, char* argv[])
{
	SDL_Surface* pDisplaySurface = NULL;  //display surface
	SDL_Event event;               //event structure
//initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) == -1) {
		cerr << "Could not initialize SDL!" << endl;
		exit(1);
	}

	//initialize SDL_ttf
	if(TTF_Init() == -1) {
		cerr << "TTF_Init: " << TTF_GetError() << endl;
		exit(2);
	}
	//create the window
	pDisplaySurface = SDL_SetVideoMode(440, 440, 0, SDL_ANYFORMAT);
	//error check
	if (pDisplaySurface == NULL) {
		//report error on the creation of video display
		cerr << "Could not set up display surface!" << endl;
		exit(1);
	}

	// load font.ttf at size 16 into font
	TTF_Font *pfont;
	pfont=TTF_OpenFont("CONSOLAS.ttf", 24);
	if (!pfont) {
		cerr << "TTF_OpenFont: " << TTF_GetError() << endl;
		// handle error
	}
		// let's create white text
		SDL_Color color = { 255,255,255 };

		SDL_Surface* ptext_surface = NULL;
		ptext_surface = TTF_RenderText_Solid(pfont, "Pass the Mojito!", color);

		if (ptext_surface == NULL) {
			//handle error here, perhaps print TTF_GetError at least
			cerr << "Could not create text_surface error: " << TTF_GetError() << endl;
			exit(3);
		}
	
	// Read in the image
	SDL_Surface* pJpegimage = IMG_Load("test.png");
	if (pJpegimage == NULL) {
		// report error trying to read in image file
		cerr << "Could not read image file" << endl; 
		exit(1);
	}
	// Get image ready for display on the screen
	SDL_Surface* pDisplayFormat = SDL_DisplayFormat(pJpegimage);
	// show on display screen
	SDL_Rect DestR; 
	DestR.x = 0; 
	DestR.y = 0; 
	SDL_BlitSurface(pDisplayFormat, NULL, pDisplaySurface, &DestR);
	// print the message
	DestR.y = 100;
	DestR.x = 100;
	SDL_BlitSurface(ptext_surface,NULL,pDisplaySurface,&DestR);
	// process events until user closes the window
	for (;;)
	{
		//wait for an event
		if (SDL_WaitEvent(&event) == 0) {
			cerr << "Error while waiting for an event!" << endl;
			exit(1);
		}
		//check for a quit event
		if (event.type == SDL_QUIT) break;
		//update the screen
		SDL_UpdateRect(pDisplaySurface, 0, 0, 0, 0);
	}
	// unload the dynamically loaded image libraries
	SDL_FreeSurface(pJpegimage);
	IMG_Quit();
	// free text message
	SDL_FreeSurface(ptext_surface);
	// free the font
	TTF_CloseFont(pfont);
	pfont=NULL; 
	TTF_Quit();
	// close SDL
	SDL_Quit();
	// we are done
	cout << "Terminating normally." << endl;
	//return 0
	return(0);
}