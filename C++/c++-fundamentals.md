# C++ :
## Compiler un fichier :
`g++ -Wall -std=c++14 <nomDuFichier>.cpp -o <nomDufichier>.exe`
Cela créer un fichier `.exe` il suffit d'entrer `<nomDuFichier.exe` dans la commande pour l'exécuter.
## Namespace standart :
Pour utiliser la librairie standart : `using namespace std;`
Pour utiliser seulement certain namespace spécifique :
```c++
using std::endl;
using std::cout;
using std::cin;
```
## Le Buffer :
Pour comprendre les i/o opérateurs il est important de comprendre comment fonctionne le buffer, par exemple lorsque l'utilisateur entre un input avec une nombre décimal dans une variable qui devrait etre un `int` le buffers va s'arretter au `.`, donc le nombre avant la virgule sera stocké dans la variable et le nombre qui reste sera dans le second input car le buffer le gardera en mémoire. Par exemple pour un input de type `int` demandé, si l'utilisateur rentre `12.5` alors le buffer enregistrera `12` et enregistra `0.5` dans la prochaine variable stockant le prochain input. <br>
Le buffer **ignore les white space** pour lui un white space correspond à la fin d'un input et le prochain commence au premier caractère **non whitespace** rencontré. Il existe une subtilité entre `std::cout/cin` et celui de la librairie standart `cout` au niveau de la destruction des données que j'expliquerais plus tard.
## Note à savoir sur `cin` `cout` :
`cin` `cout` ne sont pas utilisé dans une application réelle, mais ils sont très facile à utilisé et seront utilisé pour la totalité de cette documentation couvrant les fondamentaux du language **C++**.
## Initialisation de Variables :
```c++
int age = 20 //C convention (old style)
int age (20) // Initialization constructor (object)
int age {20} // Initialization list (C++11)
```
L'Initialisation par list est recommandé car elle permet de catch les erreurs comme l'overflow d'une variable. <br>
Il est dangereux de ne pas intitialiser nos variable avec des nombres, car si on ne le fait pas et que la variable n'est jamais utilisée ou qu'aucune valeurs n'est entré dans celle-ci, le compileur donnera un nombre random à notre variable et le programme ne sera plus valable.<br>
Ceci est dut que lorsque l'on initialise notre variable à une adresse mémoire, la variable prendra l'ancienne valeur contenue dans celle-ci, si on ne spécifie aucune valeur.
## Types dans C++ :
* `char`représente un seul caractère et sa valeur doit etre entouré de simple quotes `''`.
* Pour rendre les grand nombre lisible le nouveau standart de C++ (C++14) permet d'utiliser les `'` pour les séparer, peu importe l'endroit où on les met, c'est seulement pour nous humains :`1'256'685'235'`.
* `bool` = boolean.
## Arrays :
```c++
int test [5] {5,8,4,88,7};
int test [2] {3,4}; // le reste à 0
int test [variable] {0}; // init tout à zero
int test [variable] {};// init tout à zero
int test [] {1,5,8,9}; // set automatiquement la longueur
```
## Vectors :
Pour utiliser les vector il faut d'abord : `#include <vector>`.
Le C++ moderne utilise très peu les arrays au profit des vecteur. Ils permettent par exemple de ne pas avoir une longueur fixe lors de leur déclaration contrairement aux arrays.<br>
Les vecteurs sont contenu dans la Standard Template Library qui est la librairie standart contenant fonctions et autre de C++. Les vecteurs sont en réalité des objets dont leur longueurs peuvent varier durant le runtime.
```c++
vector <char> vowels (5); //vecteur de charactere
vector <int> test (10); // tous initialisé a 0

vector <char> test2 { 'a', 'e', 'i', 'o', 'u' };
vector <double> days (365, 80.0) //vecteur de 365 de longueur toutes init a 80.0
```
Il est meme possible de copier un array dans un vector, les vecteurs sont très puissants en c++.
Les vecteurs contiennent des fonctions qui permettent contrairement aux arrays de vérifier leur contenu. De plus tous les éléments sont init à 0 lorsque leur valeurs ne sont pas déclaré, contrairement aux arrays qui eux contiendront des valeurs **RANDOM**.<br>
Ainsi pour acceder à un index et se donner la possibilité de renvoyer une erreur si l'index n'existe pas dans un vecteur on utilise la method `at()` :
```c++
std::cout << test.at(2);
```
### Liste des méthodes dans les vecteurs :
* `.push_back()`.
* `.size()`.
* `.at()`.
## Multi-dimensionnal vectors :
```c++
vector <vector<int>> movieRating
{
    {1, 2, 3, 4}
    {1, 3, 4, 4}
    {1, 2, 4, 5}

}
```
## Casting Types :
Il est possible de spécifier le type de la conversion lorsque de valeur ont des type différents, par exemple lorsque l'on additionne un int et un double, la variable qui stockera le résultat sera convertit dans le type le plus grand, c'est à dire le double  :
```c++
int total_amount {100};
int total_number {8};
double average {0.0};
//ici si on ne fais rien le compilateur va executer une division d'int et va donc
// donner un résultat rond (12)
average = total_amount / total number; //12
cout << average << endl;
// donc pour eviter cela on utilise
average = static_cast<double>(total_number) / total_number;
std::cout << average << "\n"; //12.5
// C-style average = (double)total/count; OLD STYLE, ne check pas si la conversion est possible
```
## Switch Statement :
Pratiquement toujours utilisé avec les type intégrals.. <br>
Switch Statement fonctionne comme en javascript :
```c++
switch (var)
{
case 0:
    /* code */
    break;
default:
    /* code */
    break;
}
```
## Enumérations :
```c++
enum Direction {
    left, right, up, down
};
Direction heading {up};
```
A noter la majuscule obligatoire du nom de l'énum. Ce qui est interessant avec les enum c'est leur association avec les `switch()`. Car si dans un `switch()` un des enum n'est pas utilisé, on aura un arning dans le compilateur **sauf** si on lui met un default.
## Range-based For Loop (C++11) :
Ils sont parfait pour acceder aux éléments d'un array ou d'un vector et sont très facile à lire :
```c++
int scores [] {100, 97, 92};
for (int score : scores)
std::cout << score << "\n";
```
il est possible de demander a **C++** de déterminer le type de nos valeurs **automatiquement** : `for (auto score : scores)`.
## Liste des Librairies :
* <iomanip> permet de manipuler les valeurs affichées , `fixed`, `setprecision(1)`.
## Strings & Characteres :
méthodes de manipulation :
`#include <cctype>` : Inclure la librairie pour manipulation de string et charactere.
### Boolean :
* `isalpha()` : is a letter.
* `isalnum()`: is a letter or digit.
* `isdigit() `: is a digit.
* `islower()` : is lowercase.
* `isprint()` : is a printable letter.
* `ispunct()` : is a punctuation.
* `isupper()` : is whitespace.
### Conversion :
* `tolower()`
* `toupper()`
## String C-style:
A retenir que les array de char sont toujours terminé par un `\0`.
* `cin.getline(variable, 50)` : Pour avoir la possibilité de récupérer un string avec des whitespaces.
* `strcmp(variable1, variable2)` compare deux variables, retourne 0 si c'est un match sinon un autre nombre.
* `strlen(variable)` : longueur de l'array string.
## String class C++-Style :
Sont plus safe car plus intelligente, C++ renvoie une erreur si la manipulation est mauvaise. Possède beaucoup de méthodes, qui peuvent être compliquées. Possède une taille dynamique comparé aux string C-style.
* `#include <string>` : inclure la librairie **string**.
```c++
string s1; //empty
string s2 {"Axel"}; //Axel
string s3 {s2}; //Axel
string s4 {"Axel", 3}; //Axel
string s5 {s3, 0, 2}; //Ax
string s6 {3, 'X'}; // XXX
```
### méthodes :
* `.substr(index1, numberOfChar)` : extracte un substring.
* `.find("word")` : donne en retour l'index ou se trouve la premiere lettre du mot recherché. On peut même recherché une seule lettre. Si rien trouvé retourne `std::string::npos`.
* `.erase(index1, index2)`.
* `.clear()` : efface un string "entierement".
* `.length()` : retourne la longueur du string.
lorsque l'on veux lire plusieurs mot séparés par des whitespace contenu dans une variable on ne peux pas afficher tous les mots avec juste un `cout`, car C++ s'arette au premiere whitespace trouvé. On utilise donc `getline()`, il existe 2 variantes :
* `getline(cin, variable)`
* `getline(cin, s1, 'x')` : stop au premier x rencontré.
## Type size_t :
`size-t` est le type qui est utilisé pour stocker les variables de comptage, comme la longueur d'un array, la taille d'un objet etc. Une bonne règle est de mettre ce type lorsque l'on veut dans une boucle comparer à un autre type de type size-t. Si on ne spécifie pas size-t quand il le faudrait, il peut avoir des bugs. **A utiliser seulement pour les nombres unsigned**.
## Pass-By-Value array dans une fonction :
Attention C++ 'Pass-By-Value' les array en argument d'une fonction seulement le début de l'array, car la variable ne représente que le début de l'emplacement mémoire de l'array, C++ ensuite fait la déduction en fonction de l'index et déplace le pointer en fonction. Pour pallier à cela il faut dans notre fonction y passer la taille de l'array également :
```c++
void zero_array(int numbers[], size_t size){
    for (size_t i {0}; i < size; ++i)
    {
        numbers[i] = 0;
    }
}
int main(){
    int my_numbers[] {1,2,3,4,5};
    zero_array(my_numbers, 5);
}
```
Mais Attention ! On passe dans la fonction L'adresse mémoire de l'emplacement de l'array, ce qui veut dire que contrairement au valeur passée en argument et qui ne sont que des copies dans notre fonction, ici il s'agit du coup de l'array original, ou plus précisemment sont emplacement mémoire exact. Donc si on le modifie à l'interieur de notre fonction, on va modifier également l'array original.<br>
Pour se protéger de cela on peut utiliser le keyword `const` (read-only) lorsque l'on passe l'array dans largument de la fonction, le compilateur nous donnera une erreur si on essaye de changer la valeur des éléments de notre array. <br>
Pour remédier à cela on **Pass-By-Reference**.
## Pass-By-Reference :
Pour passer nos argument par référence il suffit d'utiliser un `&`, cela marche pour n'importe quel type :
```c++
//prototype
void add_two (int &a);
//definition
void add_two (int &a) {
   a = a +2;
}
```
Ainsi les variables passé dans nos fonctions sont les références meme de l'emplacement mémoire de notre valeur.
## Static scope :
Si on veux qu'une variable contenu dans un block, soit initialisé à une certaine valeur et qu'elle garde en mémoire la valeur précédente, il faut utiliser le keyword `static` : `static int variable{5}`. Utile dans une fonction par exemple, contrairement à javascript ou l'on doit utiliser une callback pour faire le trick.
## Recursives Fonctions :
Une fonction récursive s'appelle elle-meme. <br>
Les fonctions récursives sont exactement comme les formules récursives en mathématiques, on peut d'ailleurs les écrire tels quel dans notre code. Par exemple pour représenter un nombre **Fibonacci** dont la formule mathématique est : `Fibonacci(n) = Fibonacci(n-1) + Fibonacci(n-2)` :
```c++
unsigned long long f²ibonacci(unsigned long long n) {
    //bases cases car Fibonacci(0)=0 et Fibonacci(1)=1
    if (n <= 1)
    return n;
    return fibonacci (n-1) + fibonacci (n-2); // recursion
}
```
Il faut faire très attention **à bien specifier les bases cases** car sans cela nos fonctions peuvent ne **jamais avoir de fin** et creer un **stack overflow**, de plus elles sont plutot **gourmandes en ressources**.<br> Elles sont gourmandes car chaque appels sont stockées dans le **call Stack** et <u>prenne de la mémoire</u>. <br>
[ Explication call Stack Recursive Fonction](https://www.udemy.com/beginning-c-plus-plus-programming/learn/lecture/9535518#overview) (à 10 minutes). <br>Il faut avoir en tete également que **chaque problème récursif** peuvent s'écrire de maniere **itérative** égalem, mais les fonction itératives seront **moins explicite** pour se genre de problème.<br>
