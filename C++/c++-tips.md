# Tips in C++ :
---
## Tableau 2D :
* Set tous les éléments à une valeur choisie :
```c++
int array [3][5] = {
    {1,5,8,9,5},
    {4,9,2,4,1},
    {7,6,8,9,7}
};

for (int row {0}; row < 3; ++row) {
    for (int col {0}; col < 5; ++col) {
        array[row][col] = 1000;
    }
}
```
## Loop sur un vector 2D :
```c++
vector<vector<int>> vector_2d  {
    {1, 2, 3},
    {2, 6, 8, 9},
    {4, 8, 9, 10, 89}
};

for (auto vector: vector_2d) {
    for (auto value: vector) {
        std::cout << value << " ";
    }
    std::cout << "\n";
}
```
---
## C++ String :
```c++
string s1; //empty
string s2 {"Axel"}; //Axel
string s3 {s2}; //Axel
string s4 {"Axel", 3}; //Axel
string s5 {s3, 0, 2}; //Ax
string s6 {3, 'X'}; // XXX
string s6 ( 20, 'W' ); //wwwwwwwwwwwwwwww
```
---
## Display `true` ou `false` :
`cout << boolalpha`.

---
## Pointers :

* <u>**Acceder aux éléments d'un tableau**</u> :

```c++
int array[] {100,220,58};
int *int_ptr {array};

std::cout << *(int_ptr + 0) << "\n"; //ou pointer_name[0]
std::cout << *(int_ptr + 1) << "\n"; //ou pointer_name[1]
std::cout << *(int_ptr + 2) << "\n"; //ou pointer_name[2]

```
* <u>**Incrementer l'adresse d'un pointer et le déréférencer en 1 ligne**</u> :<br>
`cout << *array_ptr++; ` : on déférence le pointer puis on l'incrémente.
---
## Arrays :
* **Un Array est un pointer donc on peux faire pareil** :<br>

```c++
    int array[] {100,220,58,2200};
    int *int_ptr {array};

std::cout << (array + 1) << "\n";
std::cout << (array + 2) << "\n";
std::cout << (array + 3) << "\n";
```
* **Creer un array et stocker son adresse dans un pointeur** :<br>

```c++
void display(const int *const array, size_t size){
    for(size_t i {0}; i < size; ++i){
        std::cout <<  array[i]<< "\n";
    }
}
int *create_array(size_t size, int init_value ) {
    int *new_storage = new int[size];

    for (size_t i {0}; i < size; ++i) {
        *(new_storage + i) = init_value;
    }
    return new_storage;
}

int main(){
    int *list_int;
    int init_value {5};
    size_t size {10};

    list_int = create_array(size, init_value);

    display(list_int, size);

    delete [] list_int;

    return 0;
}
```
## Utiliser référence dans un range-based for loop :
Il n'est pas possible de changer la valeur d'un élément juste en utilisant la référence dans un range-based for loop, car ce n'est en fait qu'une copie :
```c++
for(auto number : number_list) //15, 25, 69, 85
    &number = 0; // 0,0,0,0
```
