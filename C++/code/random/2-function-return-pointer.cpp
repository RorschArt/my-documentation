#include <iostream>
#include <vector>
#include <cstring>
#include <string>
#include <cmath>
#include <random>
#include <time.h>
#include <cstdlib>
#include <ctime>
using namespace std;

int array1[]{1, 2, 3, 4, 5};
int array2[]{10, 20, 30};
const size_t array1_size{5};
const size_t array2_size{3};

void print(const int *const array, size_t size);

void print(int array[], size_t size)
{
  std::cout << "[";
  for (size_t i{0}; i < size; i++)
  {
    std::cout << array[i];
    std::cout << ",";
  }
  std::cout << "]"
            << "\n";
}

int *apply_all(const int *const arr1, size_t size1, const int *const arr2, size_t size2)
{
  int *newArray{};
  newArray = new int[size1 * size2];
  int position{0};

  for (size_t i{0}; i < size2; ++i)
  {
    for (size_t j{0}; j < size1; ++j)
    {
      newArray[position] = arr2[i] * arr1[j];
      //int array1[]  {1, 2, 3, 4, 5};
      //int array2[] {10, 20, 30};
      position++;
    }
  }
  std::cout << newArray << "\n";
  return newArray;
}

int main()
{
  print(array1, 5);
  print(array2, 3);

  int *results = apply_all(array1, array1_size, array2, array2_size);
  std::cout << results << "\n";
  print(results, 15);

  return 0;
}
