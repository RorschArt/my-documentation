# Formules Mathématiques pour Game Developpement

## Vecteurs 📐

### Agrandir un vecteur

On peut agrandir un vecteur en le multipliant par un scalaire
`vector(5,2) * 2 = vector(10,4);`
Si on le multipliant par un négatif on inverse son sens.

### Raccourcir un vecteur

On peut raccourcir un vecteur en le divisant par un nombre scalaire.

## Additionner deux vecteur

`v = [10, 5];`
`a = [5, 10];`

`v + a = [15, 15]`
### Accélération d'un Objet dans un espace 3D 🏃

s: position,
vo: speed at start,
a: acceleration,
t: elapsed time (écoulé)

`Δv = a·t`

`Δs = v₀t + ½·a·t²`

### Accélération

imass: inverse of mass (1/m)

`force x imass`

### halft2 (?)

`halft2 = 0.5f * t * t`

### Vélocité

`Accélération x t(s`)

### Trouver la position suivante

`pos += vel * t(s) + acceleration * halft2`

### Longueur d'un vecteur 2D

Pythagore therom :

`length = sqrt(x²+y²)`

### Longueur d'un vecteur 3D (ou magnitude)

`length = sqrt(x²+y²+z²)`

### Longueur Entre Deux vecteur (ou magnitude)

Version Math :
`Accélération
Version C++ :

`float distance = (a-b).length();`
L'ordre n'a pas d'importance car sqrt(a2-b2) et sqrt(a2-b2) vaut toujours pareil (toujours +)

### Normaliser un vecteur

quota : " *By combining the two previous operations, we can “normalize” a vector.
 A vector is said to be normalized when its length is equal to 1. Since we can
 multiply any vector (except for (0,0,0)) by a scalar and therefore change its
 length to anything we want, we can normalize any vector by multiplying it by
 the inverse of its length.* "

 Version Math :
`n = v * (1/|v|)`

Si un vecteur est normalisé `length = 1;`

Application example: If you want a vector that represents a direction without a length.

### Dot Product

Donne un scalar tès utile en 2D.
Version Math:
`(a,b,c) . (d,e,f) = a*d + b*e + c*f`
                ou
`v1 . v2 = |v1| * |v2| * cos(α)`
                Donc l'angle =
`α = arc cos( (v1 . v2) / (|v1| * |v2|) )`

quota: " *The formula above can easily be derived by isolating α on the previous
equation. If the two vectors are normalized, then their lengths are one, and the
 dot product is simply the cosine of the angle between them.

One consequence of the cosine relationship is that the dot product can be used
to determine the relative facing of two vectors. Let d be the dot product between
two NORMALIZED vectors. Then:

```
v = 1: the two vectors face the exact same direction
0 < v < 1: the two vectors face roughly the same direction
v = 0: the two vectors are perpendicular
-1 < v < 0: the two vectors face roughly opposite directions
v = -1: the two vectors face exactly opposite directions
```

If you’re not interested in v=1 and v=-1 cases, then there is no need for the vectors
to be normalized.

### Créer un vecteur parallèle

`b // = (v.b)b` donne un nouveau vecteur parallèle grâce au dot product.
`b
### Cross product (principalement pour la 3D)

Similar to the dot product, but instead of taking two vectors and producing a scalar,
the vector product takes two vectors and produces a third vector:
`(a,b,c) x (d,e,f) = (b*f-c*e, c*d-a*f, a*e-b*d)`
            donc l'angle =
`|v1 x v2| = |v1| * |v2| * sin(α)`

quota: " *it’s important to note that while the dot product is also defined for 2D vectors,
the cross product is only defined for 3D vectors. However, since it has useful properties
even for 2D, a pseudo vector product can be defined for 2D vectors " :
2D pseudo Vecteur :
`a,b) x’ (c,d) = a*d-b*c`

[plus d'infos](http://higherorderfun.com/blog/2010/02/23/math-for-game-programmers-04-operations-on-vectors/)