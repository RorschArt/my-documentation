# Pointers :
On entend beaucoup d'histoire horrible sur les pointers de C++ mais en réalité se sont de simples variables qui représente un emplacement mémoire de la machine.

* **Pourquoi Les utiliser ?** <br>
Il est vrai qu'une variable représentent également un emplacement mémoire de la machine où la valeur est stockée et lorsque l'on peux on doit utiliser des variables plutot que des pointers, mais les pointers ont un gros avantage par rapport au variable, ils n'ont pas de scope. Ainsi le pointer sera toujours utilisable en dehors d'un block de code.
Les pointers doivent obligatoirement être initialisé car sinon ils contiennent une donnée 'garbage', et donner un n'importe quel emplacement mémoire de la machine :
```c++
//pointer init à 'nulle part'(0) très different de n'importe où(non initialisé)
// nulle pointer est amené par c++11
string *pointer_name {nullptr};
```
Il ne faut pas confondre **la taille d'un pointeur** et la taille de la donnée qu'il pointe. Un pointeur aura **toujours la taille de 4 bits ou 8bits**, selon la machine car 4 ou 8 bits suffisent à définir un emplacement mémoire.
Un pointeur doit **obligatoirement avoir un type** sinon le compilateur renverra une **erreur**:

```c++
int score {10};
double score_double {100.00};

int *score_ptr {nullptr};
*score_ptr = &score; //ok
*score_ptr = &score_double; // ERREUR !

```
<u>En Bref</u> :
* Pointeurs **peuvent être null**.
* Pointeurs **sont des variables donc peuvent changer**.
* Pointeurs **peuvent être non-intitialiser** (à éviter !).
* Pointeurs **ne stocke que l'adresse** de la valeur mémoire !
## Derefencing un Pointer :
La manière dont on déréfencie un pointer à **toujours été critiquer** en C++ et on peut le comprendre, pour déréférencier un pointeur C++ à choisi d'utiliser **le même opérateur** que lorsque l'on déclare un pointeur : `*` !  <br>
Mais il faut faire avec ... Donc pour retrouver la donnée de l'emplacement mémoire que référencie un pointeur il faut le déréférencier :

```c++
double hight_temp {107.5};
double low_temp {20.5};
double *temp_ptr {&hight_temp};

std::cout << &hight_temp << "\tadress hight\n";
std::cout <<  temp_ptr << "\tvalue pointeur 1\n";
std::cout <<  *temp_ptr << "\tadress pointeur 1\n";

 temp_ptr = &low_temp;
 std::cout << &low_temp << "\tadress low\n";
 std::cout <<  temp_ptr << "\tvalue pointeur 2\n";
 std::cout <<  *temp_ptr << "\tadress pointeur 2\n";

 *temp_ptr = 1500;
 // 0x61ff00        adress hight
 // 0x61ff00        value pointeur 1
 // 107.5           adress pointeur 1

 // 0x61fef8        adress low
 // 0x61fef8        value pointeur 2
 // 20.5            adress pointeur 2
 //1500             Nouvelle valeur de low_temp
```
Si on comprend la logique derrière un pointeur ce systeme de déréferencement n'est pas si compliqué. Pour faire simple :
* Pour avoir l'emplacement d'un pointeur : `pointer_name`.
* Pour avoir les données de l'emplacement mémoire que détient le pointeur : `*pointeur_name`(dereferencing).
* Un pointeur aura toujours l'adresse en memoire : `*pointer_name = &variable`.
* Un pointeur peux changer la valeur de l'emplacement mémoire qu'il détient : `*pointer_name = 15`.

Pour utiliser la methode `at()` dans un pointeur de type `<vector>` il faut utiliser les `()`, sinon le compileur renverra une erreur : `(*vector_ptr).at(0)`. Ceci est dut au compileur qui doit d'abord déréférencier le pointeur avant d'uiltiser la méthode, car les `()` ont toujours la priorité.<br>
On peut même looper avec le pointeur :
```c++
vector<string> stooges {"Axel", "Eugenie", "Joy"};
vector<string> *vector_ptr{nullptr};

vector_ptr = &stooges;
for(auto stooge: *vector_ptr)
    std::cout << stooge << "\n";
    return 0;
```
## Allouer type durant le runtime :
Le keyword `new` permet d'allouer un type et une valeur à un pointeur durant le runtime dans le `heap` qui est different du stack, c'est une mémoire libre, inutilisée où l'on peut allouer des données dynamiquement:<br>
```c++
int *int_ptr {nullptr};
int_ptr = new int[size];
```
<u>Quelques snippets</u> :
* `array_ptr = new int[n]` : Alloue `n` int dans le pointeur de type int.
**Important** : <br>
Comme déjà dit si l'opération est un succès, alors l'adresse du premier élément dans le tableau est stocké dans le pointeur.
## Liberer l'espace allouer au pointeur :
Le keyword `delete` permet de liberer l'espace allouer au pointeur mais ne supprime pas l'adresse mémoire stocké par dans le pointeur. Pour pouvoir utiliser `delete` il faut que le pointeur ai été initialiser avec `new`.
* `delete [] array_ptr;`
* `delete int_ptr;`
* Dynamique allocation en mémoire d'un array :
```c++
int main(){
    double *temp_ptr {nullptr};
    size_t size {0};

    std::cin >> size;
    temp_ptr = new double[size];

    std::cout << temp_ptr << "\n";
    delete [] temp_ptr;
}
```
**Le Memory Leak** se produit lorsque l'on change la valeur du pointeur sans avoir utiliser `delete`, on perd totalement la référence et on ne pourra jamais la retrouver et on ne pourra jamais écrire dessus car elle n'a pas été libérée et car C++ considère cette mémoire comme utilisée. <br> **Il faut liberer l'espace à chaque fois**. Cela à pour conséquence d'avoir des performance en baisse car réduit l'espace en mémoire disponible voir l'absence d'espace disponible.<br>
Il est important à noter que le pointeur possède toujours l'adresse dans la mémoire lors de l'utilisation de `delete` mais l'emplacement possède des données 'garbage' donc inutilisable.
[article interessant sur new et delete wikipedia](https://en.wikipedia.org/wiki/New_and_delete_(C%2B%2B)).<br>
[Memory Leak Wikipedia](https://en.wikipedia.org/wiki/New_and_delete_(C%2B%2B)).<br>
[Explication Udemy (11:50)](https://www.udemy.com/beginning-c-plus-plus-programming/learn/lecture/9535534#questions).<br>

Donc d'après l'article il faut bien veiller à utiliser `delete` lorsqu'un simple `new` à été appelé et un `delete []` lorsqu'un `new []` à été appelé sinon retourne `undefined`.
## Relation entre Pointers et Arrays :
Un pointeur et un array sont la même chose puisqu'une variable de type **array** est l'adresse du premier élément d'un array. On peut donc les interchanger. Tous ce qui est applicable à un array l'est aussi pour un pointeur d'array :

```c++
int array[] {100,220,58,2200};
int *int_ptr {array};

cout << (array + 1) << "\n";
cout << (int_ptr + 1) << "\n";

cout << array[1] << "\n";
cout << int_ptr[1] << "\n";

cout << *(array + 1) << "\n";
cout << *(int_ptr + 1) << "\n";

// 0x61ff00
// 0x61ff00

// 220
// 220

// 220
// 220

```
## Constant Pointers :
* **Pointer to constant** :<br>
La donnée contenu dans l'adresse du pointeur ne peut pas changer mais le pointeur peux avoir une adresse différente :

```c++
int high_score {80};
int low_score {10};
const int *int_ptr {&high_score};

*int_ptr = 45 //ERROR !
int_ptr = &low_score //OK
```
* **Constant pointers** :<br>
La donnée contenue dans l'adresse du pointeur peut changer mais le pointeur ne peut pas changer d'adresse mémoire :

```c++
int high_score {80};
int low_score {10};
int *const int_ptr {&high_score};

*int_ptr = 45 //OK
int_ptr = &low_score //ERROR !
```
* **Constant pointers to Constant** :<br>
La donnée contenue dans l'adresse du pointeur ne peut pas changer et le pointeur ne peut pas changer d'adresse mémoire :

```c++
int high_score {80};
int low_score {10};
const int *const int_ptr {&high_score};

*int_ptr = 45 //ERROR !
int_ptr = &low_score //ERROR !
```
## Passer un pointer dans l'argument d'une fonction : <br>
```c++
int main(){
    int number {20};
    int *int_ptr {&number};

    double_data(&number);
    std::cout << number << "\n";
}

void double_data(int *int_ptr){
    *int_ptr = *int_ptr * 10;
}
```
On retourne la valeur d'un pointeur depuis une fonction car on ne veux pas perdre l'adresse du pointeur depuis laquelle on a créer notre array, car lorsque la fonction s'arette elle sort du stack et toutes ses variables sont perdues.
## Offset & Subscript Notation :<br>
Deux façon d'écrire pour accéder à un élément contenu dans la valeur(adresse) d'un pointeur :
`*(pointer + i)` : Offset Notation, On avance d'1 l'adresse (selon la taille du type) du pointeur dans un premier temps puis on le déréfence.<br>
`pointeur[i]` : Subscript.<br>
Les deux sont identiques et il n'y a pas d'avantage à utiliser l'une ou l'autres. A part peut etre une question de lisibilité selon nos préférences. C++ Propose beaucoup de solution différentes pour un problème, c'est un avantage mais c'est son défaut également, on est un peu perdus dans la masse que le language propose lorsque l'on débute et cela donne beaucoup de gout différents et de préférences.

## Références 

Une référence est juste un alias, c++ ne stocke pas sa valeur en mémoire, elles n'existe que dans notre code (**syntax sugar**) : `int& ref = variableName;`
Les référence sont utiles pour passer une variable dans une fonction, et ainsi affetcer la valeur de la variable même au lieu de créer une copie.  
On peut faire cela avec un pointeur également mais la syntaxe sera moins lisible car il faudra déférencer le pointeur  
On ne peut pas changer la cible de la référence après l'avoir déclarer (**on doit obligatoirement l'initialiser**, impossible de `int& ref;`)

```javascript
int a = 5;
int b = 1;

int& ref = a;
ref = b;
// ref (ref => (a = 5))
// ref (ref => (a = 1)) et non (ref => b)
```

Quand on comprend les référence cela devient logique, car c'est un alias et puisqu'en faisant cela cela reviendrait à faire `a = b`.  
