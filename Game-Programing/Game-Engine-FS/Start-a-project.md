# Point d'entree sur Windows WINMAIN

[WinMain MSDN DOC]('https://docs.microsoft.com/en-us/windows/win32/learnwin32/winmain--the-application-entry-point')

# Creer un script dans le `startup.bat` windows (pour le trouver `%programdata%\Microsoft\Windows\Start Menu\Programs\Startup`)

```bash
@echo off
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

REM
REM  To run this at startup, use this as your shortcut target:
REM  %windir%\system32\cmd.exe /k w:\handmade\misc\shell.bat
REM

set path=w:\devC++\Handmade_heroes\misc;%path%
w:
cd Handmade_heroes\
atom .
```

# Creer le script `shell.bat` pour la creation du disque dur virtuel dans le projet


```bash
%programdata%\Microsoft\Windows\Start Menu\Programs\Startup
set path=w:\devC++\Handmade_heroes\misc;%path%
```

# Mettre le path `C:\devC++\Handmade_heroes\misc` dans le path de l'utilisateur

Et ainsi avoir just la commande `shell` à taper

# Demarrer visual Studio avec CLI

`devenv build\win32_marsEngine.exe`

# Creer Un script `build` pour demarrer une build en appellant le compilateur avec ses options

```bash
@echo off

mkdir ..\..\build
pushd ..\..\build
cl -Zi ..\Handmade_Heroes\code\win32_handmade.cpp
popd
```

# Creer une windows structure (fenetre windows)

[MSDN sur les class Structure Window]('https://docs.microsoft.com/en-us/windows/win32/winmsg/about-window-classes')  

[MSDN Sur WNDCLASS]('https://docs.microsoft.com/fr-fr/windows/win32/api/winuser/ns-winuser-wndclassexa')

A noter que l'on peut enlever le nom `tagWNDCLASSEXA` de la `struct`.

[MSDN GetModuleHandle]('https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-getmodulehandlea') : pas utile ici mais a retenir, sion a pas écrit WinMain on
peut demander au kernel de nous renvoyer un handle et ainsi ne pas etre obliger
d'utiliser l'instance de WinMain.

## Style de la Fenetre

[MSDN Class Style]('https://docs.microsoft.com/fr-fr/windows/win32/winmsg/window-class-styles')

**[Video 2 - 18:30]** [handmade]('https://www.youtube.com/watch?v=4ROiWonnWGk')

## Recevoir les message windows

[MSDN message Windows]('https://docs.microsoft.com/fr-fr/windows/win32/winmsg/about-messages-and-message-queues')

Etant donner que l'on part from scratch on apas besoin de grand chose.

Il faut ensuite le passer au register de windows pour qu'il la prenne en compte.

[MSDN RegisterClass]('https://docs.microsoft.com/fr-fr/windows/win32/api/winuser/nf-winuser-registerclassexa')

## Creer la windows

[MSDN CreateWindowEx]('https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-createwindowexw')

## Recuperer les messages Windows par une loop

[MSDN GetMessage]('https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getmessage')

## Utiliser windows API pour draw sur la Window

[MSDN BeginPaint]('https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-beginpaint')  

[MSDN EndPaint]('https://docs.microsoft.com/fr-fr/windows/win32/api/winuser/nf-winuser-endpaint')

## MSG Structure

[MSDN MSG Structure]('https://docs.microsoft.com/en-us/dotnet/api/system.windows.interop.msg?view=netframework-4.8')

## POST QUIT MESSAGE
[MSDN PostQuitMessage]('https://docs.microsoft.com/fr-fr/windows/win32/api/winuser/nf-winuser-postquitmessage')
