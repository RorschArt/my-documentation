# __GIT Commandes__

---

## Merge

* Annuler un Merge : `git merge --abort`

---

## Branches

### Locale

* Supprimer l'Alias d'une Branche Remote : `git remote remove git.heroku.com/killphp.git`
### Remote

* Supprimer Une Branche Remote : ` git push <remote_name> --delete <branch_name>`

* Reset une Branche Locale Pour Etre Comme Une Remote Branch : `git reset --hard origin/master`
---
## Pour __Heroku__

* Pour Envoyer sur Une Branche Specifique sur Heroku : `git push heroku localBranch:master`
* Pour Afficher les Logs : `heroku logs --tail`

## Supprimer un dossier dans le repo
[stackoverflow](https://stackoverflow.com/questions/6313126/how-to-remove-a-directory-from-git-repository)

```bash
git rm -r --cached .vscode
```