# MongoDB

## Se connecter 📡

```javascript
const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'medical-manager'

MongoClient.connect(connectionURL, { useNewURLParser: true }, (error, client) => {
    if (error) {
        return console.log('Unable to Connect')
    }
    return console.log('connected')
})
```

## Créer une base de donnée et insérer des documents 🍨

```javascript
const db = client.db(databaseName)
    db.collection('users').insertOne({
        name: 'Axel',
        age: 29
    }, (error, result) => {
        if (error) {
            return  console.log('Unable to insert user');
        }
        console.log(result.ops);

    })
```

## Création d'ObjectID 💥

```javascript
const ObjectID = mongodb.ObjectID()
const id = new ObjectID() //new optionnel car MongoDb handle it
console.log(id.getTimestamp()) //recupérer le Timestamp
```

> 💡 : MongoDB stocke les _id sous forme de buffer, ainsi il divise par deux leur tailles.

---

## Queries 🦋

> 💡 : Les callbacks des queries ne retourne pas d'erreur lorsque mongob n'a pas trouvé le document recherché.

## `.findOne()`

> 💡 :  Retourne le premier document qui match le query.
>
Pour rechercher un document par son `_id\` il faut utiliser la méthode `ObjectID()` fourni par MongoDB :

```javascript
db.collection('users').findOne({ _id: new ObjectID("5d1ca9a6c76d7620fc6181bf") }, (error, result) => {})
```

## `find()`

Il est un peu différent car il ne retourne pas de callback mais retourne un **[Cursor](http://mongodb.github.io/node-mongodb-native/3.1/a)** car un peu à la manière de C++ mongoDB assume qu'a chaque fois que l'on appel  `find()` on souhaite en retour un array.
Ce [Cursor](http://mongodb.github.io/node-mongodb-native/3.1/a) peut être utilisé pour itéré et retrouver les documents contenu dans l'array.
La méthode la plus utilisée est le `toArray` qui retourne l'array des documents qui match les critères.

```javascript
db.collection('users').find({ name: 'Axel' }).toArray((error, users) => {
        console.log(users);
    })
```

Une autre méthode utile est `.count()` qui retourne le nombre de documents qui match le query

## `updateOne()`

> 💡 : update doit etre utilisé avec un update operator.
> [update operators](https://docs.mongodb.com/manual/reference/operator/update/)

```javascript
    const updatePromise = db.collection('users').updateOne({
        _id: new ObjectID("5d1ca9a6c76d7620fc6181bf")
    }, {
            $set: {
                name: "fejjencjndcjhbndc"
            }
        }).then((result) => {
            console.log(result.modifierdCount)
        }).catch((error) => {
            console.log(error);
        })
})
```

Si aucune callback n'est appelé, alors update renvoi une promise.

`$set`: change les champs défini dans l'objet et garde ceux qui n'ont pas été spécifiés.

# Mongoose  ✨

Mongoose facilite les opérations et rend le compte lisible en utilisant une notation Objet.

## Se connecter

```javascript
const mongoose = require('mongoose')

mongoose.connect(mongodb: '//127.0.0.1:27017/medical-manager', {
    useNewUrlParser: true,
    useCreateIndex: true
})
```

## Set un Schemas et enregistrer une instance

```javascript
const mongoose = require('mongoose')

mongoose.connect('mongodb://127.0.0.1:27017/medical-manager', {
    useNewUrlParser: true,
    useCreateIndex: true
})

const User = mongoose.model('User', {
    age: {
        type: Number
    },
    email: {
        type: String
})

const user1 = new User({
    email: 'axel@gmail.com',
    age: 255
})
user1.save() //retourne une promise
```

## Validation & Sanitization 🔎

liste des possibilitées : [liste Mongoose](https://mongoosejs.com/docs/schematypes.html)

### `validate()`

Mongoose lack de methodes pour valider nos données, `validate()` permet de créer nous-meme la fonction que l'on désire pour traiter les valeur entré.

### `trim :`

trim la valeur d'entrée : `trim: true`.

## Permettre l'update que des champs existants ✔️

Dans options : `runValidator: true`.

## Créer un schema et exporter le model

[mongoose.Schema](https://mongoosejs.com/docs/api.html#mongoose_Mongoose-Schema)

```javascript
const userSchema = new mongoose.schema('User', {
        pseudoName: {
            type: String,
            trim: true,
            default: 'inconnu',
            required: true
})
const User = mongoose.model('User', userSchema)
module.exports = User
```

## Mongoose est intelligent 🧠

Lorsque l'on veut update sans utiliser `findByIdAndUpdate` car on veut utiliser un middleware (pas pris en compte avec FBIAU) il suffit de `findById` et une fois que l'on a modifier notre document de le `save()`, mongoose va écraser de lui même le document.

## Créer des méthodes pour un code réutilisable et plus propre 🤡

On peut comme dans une classe créer nos propre méthodes, accessible dans nos instances.

Tout D'abord il faut impérativement dans nos models mettre en place un `Schemas` car sans cela nous n'aurons pas accès à la plupart des fonctionnalités énoncé plus haut ou même celle ci.

```javascript
userSchema.statics.nomDeLaMéthode
User.nomDeLaMéthode()
```

Ceci est appliqué de manière générale à nos instances, comme par exemple rechercher un utilisateur avec son email.
Pour pouvoir créer des méthodes applicable seulement à nos instance on fait

```javascript
userSchema.methods.nomDeLaMéthode
user.nomDeLaMéthode()
```

## Unicité 🦋

Pour qu'un champ de notre document soit unique il suffit de spécifier dans le `schema` : `unique: true`

## Methode Mongoose utiles

`model.toObject()`

## Appliquer un traitement avant chaque renvoi de JSON au client

la méthode de mongoose permet de changer la manière dont le JSON sera réaliser, il suffit de créer une methode comme cela dans le modèle

```javasscript
schema.methods.toJSON = () =>{}
```

[Mongoose Doc](https://mongoosejs.com/docs/api.html#document_Document-toJSON)

## Supprimer l'instance actuelle

```javascript
instanceModelName.remove()
```

## Enlever des propriété dans le résultat  de find()

```javascript
const users = await User.find({}).select('-email -age')
```

## Pagination

Pour choisir un limit lors d'un `find()`

```javascript
 const articles = await Article.find( null, null, queryParams)
 ```
