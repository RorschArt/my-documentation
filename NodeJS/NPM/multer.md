# Multer

## Retrouver le fichier traité dans le router express

`req.file` avec methodes : `req.file.buffer`

On y a accès **seulement** lorsque l'on ne donne de propriété `dest` dans l'instance de `multer`