# Env-cmd

Permet de simplifier la configuration des variables d'environnement qui sont différente d'une machine à l'autre

A installer en dev dependancy

## Pour un script dev nodemon

```json
//avec yarn
"dev": "env-cmd -f ./config/dev.env nodemon src/index.js"
//avec npm
"dev": "env-cmd ./config/dev.env nodemon src/index.js"
```
