#  Express 👍

**Express** est un NPM Module permettant de facilité la mise en place d'un serveur envoyant des données JSON en fonction de l'url et des informations envoyé par le client. Tout cela serait possible juste avec NodeJS mais cela reviendrais a écrire des tas de lignes de codes inutiles, non-maintenable alors que le problèmes à déjà été résolu par cette librairie, comme on dit souvent : **"don't reinvent the wheel"**.

## Démarrer le serveur 💻

```javascript
app.listen(port, () => {
    console.log('écoute du serveur sur port :', port)
})
```

## Créer nos routes 🛵

```javascript
app.get('/users', (req, res) => {
       res.send('testing !')
})
```

## Permettre de recevoir en JSON

```javascript
app.use(express.json())
```

## Utiliser les données de la requete ✏️

```javascript

app.post('/users', (req, res) => {
    const user = new User(req.body)

    user.save().then((user) => {
        console.log("SUCCES : ", user);
    }).catch((error) => {
        console.log(error);

    })
})
```

## Ajouter un Router 🕸

```javascript
const express = require('express')
const router = new express.Router()
```

## Récuperer un header spécifique de la requete 🚁

```javascript
req.header('authorization')
```

On peut utiliser les dot notation si on utilise un string

```javascript
const user = await User.findOne({ _id: jsonDecoded._id, 'tokens.token': jsonDecoded })
```

## Réutiliser les données recueilli par un middleware

Si on a déjà trouver un utilisateur il n'est pas necessaire de le chercher à nouveau dans notre route.

## Customiser la façon dont express handle les erreurs de nos middleware

```javascript
const upload = multer({
    dest: 'images',
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb){
        if (!file.originalname.match(/\.(jpg|png|jpeg)$/)) {
            return cb(new Error('Veuillez Uploader un fichier image SVP !'))
        }
        cb(undefined, true)
    }
})
// UPLOAD Avatar User me
router.post('/users/me/avatar', auth, upload.single('avatar'), async (req, res) => {
    res.send()
}, (error, req, res, next) => res.status(400).send({ error: error.message })
)
```