# Postman 👨‍🚀

Pour créer un héritage des JWT il suffit de mettre dans le `test` le code suivant

```javascript
if(pm.response.code === 200) {
    pm.environment.set('authToken', pm.response.json().token)
}
```

puis de mettre la variable `authToken` dans l'héritage.
