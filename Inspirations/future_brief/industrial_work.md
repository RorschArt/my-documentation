<h1>Machines Working</h1>
<p>
Dans un entrepot qui vend de la technologie simple et bon marché, la main d'oeuvre
humaine a laissé place à la main d'oeuvre robotique. <br>
Dans les entrepots on ne voit plus qu'une seule personne qui gère toute ces machines,
les robots ont tous des fonctions spéciales, la plupart se déplacent via un système de rail
car cela permet un déplacement rapide et ordonnée sur des axes leur permettant de ne
jamais se heurter, le système de gestion des collisions en moins et un prix plus
attractif, car c'est une technologie demandant beaucoup de pièces et d'un logiciel
aux algorithmes évolués même en ces temps modernes. <br>
Ces robots sans jambes ont une carrure légère et ne semble pas peuser lourd mais en
réalité la matière dont ils sont constitués est un alliage très dense et lourd, chaque
robot pèse au moins 500 kg. <br>
Il naviguent parmis les stocks de marchandise montés sur des échaffauts, prennent la
marchandise necessaire à la préparation de leur commande et les stockes dans des boites
en métal déstinées au transport et réutilisatble, lockable à l'aide d'une empreinte
digitale qui est celle du destinataire. Aujourd'hui les empreintes digitales sont
naturellement utilisées pour les signatures, les embarquements spatiaux et autres.<br>
Parmis tous ces robots rollercoster il y en a un de différent : il n'est pas sur rail.
Ce robot est un robot beaucoup plus proche de la silhouette d'un homme que les autres,
il peut recevoir des ordres, apprendre et il peut s'adapter à n'importe quel demande.
L'une des plus grosse difficultés dans l'implémentation des robots dans le monde
industriel à toujours été que l'on devait programmer ce pour quoi ils seraient utilisés,
et bien entendu il arrive toujours dans ce milieu que l'on ai besoin de traiter une tâche
particulière et surtout exceptionnelle. Retirer des autocollants car le fournisseur s'est
trompé lors de l'envoi n'est pas une fonctionnalité que l'on pense à ajouter lorsque
l'on créer un robot industriel par exemple, en revanche il doit pouvoir s'adapter et même
apprendre. Pour cela les concepteur d'intelligence artificielle ont longtemps recherché
comment intégrer cela dans "l'ame" d'un android.<br>
Heureusement un génie est venu résoudre cette quête, c'est un américain de génie qui fit
évolué l'intelligence artificielle d'un pas de géant : Aaron guilmoore c'était son nom.
Il avait participé à la conception du premier android domestique commercialisé alors qu'il
n'avait que 20 ans. C'était un homme dévoué à l'évolution technologique, il voulait changer
le monde et il était visionnaire, tout ce qu"il avait prédit s'est produit après sa mort, en effet
étant devenu génant pour les pays voisins, il avait été éliminer par le gouvernement chinois.
Le monde entier fut choqué par la nouvelle car du haut de ses 35 ans il avait accomplis bon
nombre de révolution dans la vie humaine, et ce n'était pas fini, il avait alors posé les bases
de ce que l'on appelera plus tard la "conscience robotique". Ainsi il avait mis au point toute une
structure algorithmique construite sur le schémas d'un cerveau humain. Il avait pratiquement fini
toute l'élaboration, qui constitue l'écriture d'un "cerveau" robotique et la création des composants
électroiques nouveaux qui n'éxistaient pas encore en ces temps là pour son fonctionnement. Il travaillait
étroitement avec une entreprise "secrète" qui élaborait tous les prototypes des technologies futures.
Son nom était iTeck mais aujourd'hui son nom est devenu Androkit et ne vend dorénavant que des pièces
détachés pour robot évolué ou bionique.<br>
Pour en revenir à notre entrepot de marchandises, le robot autonome fait partie des
robots qui ont poussaient les humains qui travaillaient dans l'industrie vers la sortie.
On pourrait penser que c'est une bonne chose mais en réalité l'homme n'a pas changé
son économie, l'argent est toujours une des choses la plus importante et il y a
aujourdh'ui beaucoup moins de travail pour les tâches facilement remplaçable par
un robot, notamment les tâches répétitives qui ne demandent pas de grande capacités
d'adaptation sont aisément faisable par un robot bas de gamme, il aura quelques
pannes de temps en temps c'est certains mais si on le couple avec un robot mécano
le tandem fait des ravages et la production est facilement multipliable par 100,
un robot peut travailler sans relâche, pas de pause, pas d'enguelade, que du bonus.<br>
Du coup que sont devenus les humains qui travaillaient dans ses usines ? <br>
Soit ils ont trouvé autre chose, un travail dans un fast food si un android n'étais
pas déjà là ou ils ont tout simplement abandonnés, certains ont mal fini et n'ont pas
survécue à ce changement, d'autres ont trouvé une communauté qui les ont accueillis.
Aujourd'hui il existe des communauté qui refuse de subir ce changement et ont décidaient
de se réfugier dans des sortes de bidon ville, où l'argent n'existe pas, l'entraide est là
mais l'endroit peut être dangereux car c'est devenu un endroit pour se cacher des forces de
l'ordre. 
