> 23 September 2019

<h1>Un monde sur le déclin</h1>

<p>
On est 2081 et il arrive ce qu'il nous avait été prédit : la terre arrive à
épuisement de ses ressources naturelles, l'homme a depuis trop longtemps tiré sur
la corde.<br>
La tension entre pays est maximale, les petits pays n'ont d'autres choix que de subir
cette situation en attendant que les "grandes" nations bougent le petit doigt.<br>
Les USA sont en réalité déjà dans une préparation pour attaquer les pays de manière
stratégique. Leur but est d'affaiblir les pays adverses les plus faibles, de faire
un maximum de victime pour diminuer le nombre d'être humains sur terre, d'abord
les pays faibles ainsi que leur colonisation pour récupérer le peu de ressources
qu'il restent. Mais ils veulent également touché un grand pays à l'aide d'alliance avec
d'autres pays.<br>
La technologie actuelle n'a pas permis de se préparer à ce que le monde vie aujourd'hui,
des milliards d'âme vont perdre la vie dans cette survie, et peu à peu les gouvernements
tomberont.<br>

</p>
